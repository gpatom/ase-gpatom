import numpy as np
from ase.build import fcc111
from ase.calculators.emt import EMT
from gpatom.gpfp.gp import GaussianProcess
from gpatom.gpfp.fingerprint import RadialAngularFP
from gpatom.gpfp.prior import ConstantPrior

def new_atoms(rattle_seed=None):     
    atoms = fcc111('Ag', size=(2, 2, 1))
    atoms.center(vacuum=4.0, axis=2)
    atoms.rattle(0.05, seed=rattle_seed)
    atoms.calc = EMT()
    return atoms


def get_training_data():
    atoms_list=[new_atoms(rattle_seed=i) for i in range(3)]    
    return atoms_list


def create_data():
    fp_class = RadialAngularFP

    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1.0}

    frames = get_training_data()


    X = [fp_class(atoms=atoms, **fp_args) for atoms in frames]

    energyforces = [[atoms.get_potential_energy()] +
                    list(atoms.get_forces().flatten())
                    for atoms in frames]
    Y = np.array(energyforces).reshape(-1)
    return  X, Y, fp_class, fp_args



def test_gp_predict():    
    
    X, Y, fp_class, fp_args = create_data()
    
    
    gp_args = dict(prior=ConstantPrior(constant=0), 
                   hp={'scale': 100, 'weight': 10, 
                       'noise': 1e-4, 'ratio': 0.001, 'noisefactor': 1}, 
                   use_forces=True)
    
    gp = GaussianProcess(**gp_args)
    gp.train(X, Y)
    
    test_atoms = new_atoms(rattle_seed=10)
    
    test_fp = fp_class(atoms=test_atoms, **fp_args)
    
    results, variance = gp.predict(test_fp, get_variance=True)
    
    predicted_energy=results[0]
    energy_uncertainty=np.sqrt(variance[0,0])
    
    print('energy', predicted_energy)
    
    test_atoms.calc=EMT()
    calculated_energy=test_atoms.get_potential_energy()


    print('real energy: '+ str(calculated_energy))
    print('real error: '+ str(abs(calculated_energy-predicted_energy)))
    print('estimated energy: ' + str(predicted_energy))
    print('estimated error: ' + str(energy_uncertainty))
        
    # check that the result is the same as when test was made
    # notice that this doesnt test that its correct, but consistent
    assert abs(predicted_energy - 2.274) < 0.001
    assert abs(energy_uncertainty - 0.0058) < 0.0001
    
    # test that predicted and calculated energy are close
    assert abs(predicted_energy - calculated_energy) < 0.05

    