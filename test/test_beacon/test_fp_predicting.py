from ase.build import fcc100
from ase.calculators.emt import EMT
import warnings

from gpatom.gpfp.fingerprint import (RadialFP, RadialAngularFP)

from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.gpfp.calculator import GPCalculator
from gpatom.gpfp.fingerprint import FingerPrint
from gpatom.gpfp.gp import GaussianProcess
import pytest
from gpatom.gpfp.prior import ConstantPrior


@pytest.mark.parametrize('aweight', [0, 1])  #  correspond to RadialFP and  RadialAngularFP
@pytest.mark.parametrize('kernel', ['sqexp', 'matern', 'rq'])

def test_prediction(aweight, kernel):
    def set_structure(atoms, seed=0):
        """
        Create a copy and rattle.
        """
        newatoms = atoms.copy()
        newatoms.rattle(0.05, seed=seed)

        newatoms.calc = EMT()
        return newatoms

    # Create slab
    slab = fcc100('Ag', size=(2, 2, 2))
    slab[-1].symbol = 'Au'
    slab[-2].symbol = 'Au'
    slab.rattle(0.5)
    slab.center(vacuum=4.0)
    slab.pbc = False


    # Create Training Set
    train_images = []
    for i in range(5):
        # Alter structure and get fingerprint
        slab2 = set_structure(slab, seed=i)
        train_images.append(slab2)

    slab.calc = EMT()


    gp_args = dict(prior=ConstantPrior(constant=0), 
                   hp={'scale': 1000, 'weight': 100, 
                       'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1}, 
                   use_forces=True)
    
    gp=GaussianProcess(**gp_args)
    
    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': aweight}
    
    fp=FingerPrint(fp_args)

    model=Model(gp=gp, fp=fp)  
    model.add_data_points(train_images)

    slab.calc = GPCalculator(model)

    with warnings.catch_warnings():
        warnings.filterwarnings('ignore', category=UserWarning)
        e_pred = slab.get_potential_energy()

    slab.calc = EMT()
    e_real = slab.get_potential_energy()
    print()
    print('EMT energy: {:10.04f} eV'.format(e_real))
    print('GP energy: {:11.04f} eV'.format(e_pred))

    assert abs(e_pred - e_real) < 0.015
