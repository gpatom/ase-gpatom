import pytest

import numpy as np

from gpatom.beacon.beacon import StructureList, InitatomsGenerator

from ase.build.surface import fcc100

from gpatom.beacon.str_gen import RandomBranch, Remake

from ase.optimize import BFGS

from ase.calculators.emt import EMT

from ase.constraints import FixAtoms

def set_atoms():
    atoms1 = fcc100('Au', (2, 2, 1))
    atoms1.pbc = False
    atoms1.info = {}
    atoms1.center(vacuum=6.0)
    atoms1.rattle(0.1)

    sgen = RandomBranch(atoms1, rng=np.random.RandomState(451)) 
    atoms2 = sgen.get()
    atoms3 = sgen.get()

    atoms1.calc = EMT()
    atoms2.calc = EMT()
    atoms3.calc = EMT()

    return atoms1, atoms2, atoms3, sgen

def test_structurelist():

    atoms1, atoms2, atoms3, sgen = set_atoms()

    # Make sure our test makes sense:
    assert atoms1.get_potential_energy() < atoms2.get_potential_energy()
    assert atoms1.get_potential_energy() < atoms3.get_potential_energy()
    assert atoms3.get_potential_energy() < atoms2.get_potential_energy()

    # Initialize object:
    obj = StructureList(images=[atoms1, atoms2], n=2, flim=0.1)
    assert len(obj) == 2

    # Update list:
    obj.update(new_atoms=atoms3, candidate_index=2)

    # atoms3 has lower energy than atoms2, and therefore
    # it should have been moved to the second place of
    # structure list:
    assert len(obj) == 2
    assert obj[1] == atoms3
    assert obj.energies[1] == atoms3.get_potential_energy()



    # Relax first structure and move it to the list:
    new = atoms1.copy()
    new.calc = EMT()
    opt = BFGS(new)
    opt.run(fmax=obj.flim)

    # Update list:
    obj.update(new_atoms=new, candidate_index=0)

    # Now the first element should have been removed from the
    # list because we relaxed it to small force residuals.
    # The list should only contain atoms3 now:
    assert len(obj) == 1
    assert obj[0] == atoms3
    assert obj.energies[0] == atoms3.get_potential_energy()


def test_empty_structurelist():

    atoms1, atoms2, atoms3, sgen = set_atoms()

    # Initialize object:
    obj = StructureList(images=[], n=2, flim=0.1)

    # Update list:
    obj.update(new_atoms=atoms1, candidate_index=2)

    assert len(obj) == 1
    assert obj[0] == atoms1
    assert obj.energies[0] == atoms1.get_potential_energy()


def test_testatomsgenerator():
    
    atoms1, atoms2, atoms3, sgen = set_atoms()

    # Make sure our test makes sense:
    assert atoms1.get_potential_energy() < atoms2.get_potential_energy()
    assert atoms1.get_potential_energy() < atoms3.get_potential_energy()
    assert atoms3.get_potential_energy() < atoms2.get_potential_energy()

    # Initialize object:
    obj = StructureList(images=[atoms1, atoms2], n=2, flim=0.1)


    radint=100000
    gen = InitatomsGenerator(nbest=2, nrattle=1, rattlestrength=0.5, 
                             sgen=sgen, lob=obj,
                             rng=np.random.RandomState(600),
                             rattle_seed_int=radint)

  


    # best
    testatoms = gen.get(0)
    testatoms.center()
    atoms1.center()
     
    
    assert np.allclose(testatoms.positions, atoms1.positions)
    

    # second best
    testatoms = gen.get(1)
    testatoms.center()
    atoms2.center()
    assert np.allclose(testatoms.positions, atoms2.positions)
    
  
    # rattled one
    testatoms = gen.get(2)
    
    testatoms.center()
    atoms3 = atoms1.copy()
    atoms3.rattle(0.5, seed=np.random.RandomState(600).randint(radint))
    atoms3.center()
    
  
    assert np.allclose(testatoms.positions, atoms3.positions)
    
    
    # something else:
    testatoms = gen.get(3)
    testatoms.center()
    sgen = RandomBranch(atoms1, rng=np.random.RandomState(451))

    assert not np.allclose(testatoms.positions, atoms1.positions)
    assert not np.allclose(testatoms.positions, atoms2.positions)
    assert not np.allclose(testatoms.positions, atoms3.positions)

    atoms4 = atoms2.copy()
    rng = np.random.RandomState(601)
    rng.randint(100000)
    atoms4.rattle(0.5, seed=rng.randint(100000))
    atoms4.center()
    assert not np.allclose(testatoms.positions, atoms4.positions)

    
    
def test_get_random():
    # test the functionality of InitatomsGenerator.get_random()
    atoms1, atoms2, atoms3, sgen = set_atoms()
    
    rgen=Remake(atoms2)
    
    gen = InitatomsGenerator(nbest=0, nrattle=0, 
                             sgen=sgen, rgen=rgen,
                             rng=np.random.RandomState(600))
    
    assert np.allclose(atoms2.positions, gen.get_random().positions )
    
    
    