#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 00:11:38 2024

@author: casper
"""

import numpy as np
from ase.build import fcc111
from ase.calculators.emt import EMT
from gpatom.gpfp.fingerprint import FingerPrint, FPUpdater


def new_atoms(rattle_seed=None):     
    atoms = fcc111('Ag', size=(2, 2, 2))
    atoms.center(vacuum=4.0, axis=2)
    atoms.rattle(0.1, seed=rattle_seed)
    atoms.calc = EMT()
    return atoms


def get_rel_diff(fp1, fp2):
        
    dif_fp_rad=np.abs(fp1.rho_R.flatten()-fp2.rho_R.flatten())
    dif_fp_ang=np.abs(fp1.rho_a.flatten()-fp2.rho_a.flatten())
    
    max_rad=np.max(dif_fp_rad)
    max_ang=np.max(dif_fp_ang)
    
    rel_max=max_ang/max_rad
    
    return rel_max


def test_update():

    fp_args = {'r_cutoff': 8, 'a_cutoff': 4, 'aweight': 1}

    factor=0.45

    fp_updater=FPUpdater(factor=factor)

    fp=FingerPrint(fp_args=fp_args, calc_strain=False)

    atoms_1=new_atoms(rattle_seed=10)
    atoms_2=new_atoms(rattle_seed=20)

    fp_1=fp.get(atoms_1)
    fp_2=fp.get(atoms_2)
    
    rel_dif=get_rel_diff(fp_1, fp_2)
    
    fp_list=[fp_1, fp_2]

    fp_updater.update(fp_list, fp)
        
    fp_1_recalc=fp.get(atoms_1)
    fp_2_recalc=fp.get(atoms_2)
    
    rel_dif_recalc=get_rel_diff(fp_1_recalc, fp_2_recalc)
    
    
    aweight_recalc=fp.fp_args['aweight']
    
    assert aweight_recalc > 1
    assert rel_dif_recalc > rel_dif
    assert np.isclose(rel_dif_recalc, factor)
    
    assert np.isclose(np.max(fp_1_recalc.rho_R.flatten()) , 
                      np.max(fp_1.rho_R.flatten()) )
    
    assert not np.isclose(np.max(fp_1_recalc.rho_a.flatten()) ,
                          np.max(fp_1.rho_a.flatten()) )
     
    
    
    # finally check that recalculating awaigt on new set of 
    # fingerprints doesnt change aweight
    
    fp_list_recalc=[fp_1_recalc, fp_2_recalc]  
    fp_updater.update(fp_list_recalc, fp)
     
    aweight_recalc_again=fp.fp_args['aweight']
    
    fp_1_recalc_again=fp.get(atoms_1)
    fp_2_recalc_again=fp.get(atoms_2)
    
    rel_dif_recalc_again=get_rel_diff(fp_1_recalc_again, fp_2_recalc_again)
     
    assert np.isclose(aweight_recalc, aweight_recalc_again)
    
    
    