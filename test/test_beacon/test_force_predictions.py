
import pytest
import numpy as np
from ase.build import fcc111
from ase.calculators.emt import EMT

from gpatom.gpfp.gp import GaussianProcess
from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.gpfp.fingerprint import FingerPrint

from gpatom.gpfp.prior import ConstantPrior, RepulsivePotential, CalculatorPrior
from gpatom.gpfp.calculator import GPCalculator, PriorCalculator


def new_atoms(rattle_seed=None):     
    atoms = fcc111('Ag', size=(2, 2, 2))
    atoms.center(vacuum=4.0, axis=2)
    atoms.rattle(0.1, seed=rattle_seed)
    atoms.calc = EMT()
    return atoms


def get_training_data():
    atoms_list=[new_atoms(rattle_seed=i) for i in range(5)]
    return atoms_list


@pytest.mark.parametrize('prior', [CalculatorPrior(RepulsivePotential(potential_type='LJ', prefactor=10), constant=0),
                                   CalculatorPrior(RepulsivePotential(potential_type='parabola', prefactor=10), constant=0)]  )
def test_repulsive_prior_force_prediction(prior):

    calc=PriorCalculator(prior.calculator, calculate_stress=False)
    
    atoms=new_atoms(rattle_seed=10)

    atoms.calc=calc 
   
    atoms.get_potential_energy() 
    
    analytical_forces=calc.results['forces']
    
    numerical_forces = atoms.calc.calculate_numerical_forces(atoms)    

    print("analytical forces:", analytical_forces)
    print("numerical forces:", numerical_forces)     

    assert np.allclose(analytical_forces, numerical_forces, atol=5e-3, rtol=1e-3)



@pytest.mark.parametrize('use_forces', [True, False])
@pytest.mark.parametrize('prior', [ConstantPrior(constant=0), 
                                   CalculatorPrior(RepulsivePotential(potential_type='LJ', prefactor=10), constant=0)]  )    
def test_force_prediction_with_prior(prior, use_forces):
    
    gp_args = dict(prior=prior,
                   hp={'scale': 100, 'weight': 10, 
                       'noise': 1e-4, 'ratio': 0.001, 'noisefactor': 1}, 
                   use_forces=use_forces)
    
    gp=GaussianProcess(**gp_args)
    
    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1.0}
    
    fp=FingerPrint(fp_args, calc_strain=False)
    
    model=Model(gp=gp, fp=fp)
    
    training_data=get_training_data()
        
    model.add_data_points(training_data)
    
    atoms=new_atoms(rattle_seed=10)

    calc=GPCalculator(model, calculate_stress=False)
    
    atoms.calc=calc 
   
    atoms.get_potential_energy() 
    
    analytical_forces=calc.results['forces']
    
    numerical_forces = atoms.calc.calculate_numerical_forces(atoms)    

    print("analytical forces:", analytical_forces)
    print("numerical forces:", numerical_forces)     
    assert np.allclose(analytical_forces, numerical_forces, atol=5e-3, rtol=1e-3)
    