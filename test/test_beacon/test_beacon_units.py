""" Tests for pytest

Usage
-----
>>> python3 -m pytest
"""


from gpatom.beacon.beacon import (SurrogateOptimizer,
                                  Checker, BEACONAtomsIO,
                                  IterationResult,
                                  CalculatorInterface,
                                  LowerBound)
from gpatom.gpfp.calculator import GPCalculator
from gpatom.beacon.str_gen import RandomBranch
from ase.build.surface import fcc100
from ase.calculators.emt import EMT
from ase.io import read
from os.path import exists

import numpy as np

from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.gpfp.gp import GaussianProcess
from gpatom.gpfp.fingerprint import FingerPrint
from gpatom.gpfp.prior import ConstantPrior
from ase import Atoms


def get_atoms():
    atoms1 = fcc100('Au', (2, 2, 1))
    atoms1.pbc = False
    atoms1.info = {}
    atoms1.center(vacuum=6.0)
    atoms1.rattle(0.1)

    atoms1.calc = EMT()

    return atoms1 


def test_surrogateoptimizer():
    
    atoms1 = get_atoms()
    
    sgen = RandomBranch(atoms1, rng=np.random.RandomState(451))

    initatoms = [sgen.get(), sgen.get()]

    for atoms in initatoms:
        atoms.calc = EMT()

    gp_args = dict(prior=ConstantPrior(constant=0), 
               hp={'scale': 500, 'weight': 50, 
                       'noise': 0.05, 'ratio': 0.001, 'noisefactor': 1},
               use_forces=True) 
    
    
    fp_args = {'r_cutoff': 8.0, 'r_delta': 0.4, 'r_nbins': 200,
               'a_cutoff': 4.0, 'a_delta': 0.4, 'a_nbins': 100,
               'gamma': 0.5, 'aweight': 1.0}    
    
    
    gp=GaussianProcess(**gp_args)
    fp=FingerPrint(fp_args)
    model=Model(gp=gp, fp=fp)
    model.add_data_points(initatoms)


    opt = SurrogateOptimizer(fmax=0.005, relax_steps=10,
                             write_surropt_trajs=True)

    
    eng_start, forces_start, unc_start= model.get_energy_and_forces(atoms1)
    
    opt_atoms, success = opt.relax(atoms1, model, file_identifier='test')
   
    # assert no success because so few evaluations:
    assert not success

    # assert file exists:
    assert exists('opt_test.traj')

    # assert that energy is falling during relaxation
    eng_end, forces_end, unc_end= model.get_energy_and_forces(atoms1)

    assert eng_end < eng_start
    
    

def test_checker():

    atoms1 = get_atoms()

    atoms1.positions = [[0., 0., 0.],
                        [1., 0., 0.],
                        [0., 3., 0.],
                        [0., 3., 10.]]

    checker = Checker(dist_limit=50, rlimit=0.5, disconnect_limit=1.25) 

    # bad bond lengths:
    assert not checker.check_atomic_distances(atoms1)   

    atoms1.positions = [[0., 0., 0.],
                        [2., 0., 0.],
                        [0., 4., 0.],
                        [0., 3., 10.]]

    # good bond lengths:
    assert checker.check_atomic_distances(atoms1)
    
    
    atoms1.positions = [[0., 0., 0.],
                        [1., 3., 3.],
                        [3., 3., 3.],
                        [5., 5., 3.]]
    
    # disconnection
    assert not checker.check_disconnections(atoms1) 
    
    atoms1.positions = [[0., 5., 5.],
                        [1., 3., 3.],
                        [3., 3., 3.],
                        [5., 3., 3.]]
    
    # no disconnection
    assert checker.check_disconnections(atoms1)

    # bad fingerprint distances:
    assert not checker.check_fingerprint_distances(distances=[25])   

    # good fingerprint distances:
    assert checker.check_fingerprint_distances(distances=[100])
    
    
    atoms1.positions = [[0., 0., 0.],
                        [2., 0., 0.],
                        [0., 4., 0.],
                        [0., 3., 10.]]
    
    

def test_checker_cell():
    atoms1 = get_atoms()
    
    volume=atoms1.get_volume()
    
    volume_limits=[volume*0.75, volume*1.25]
    
    checker = Checker(angle_limit=45, volume_limits=volume_limits )
    
    
    # check the cell volume
    small_cell = atoms1.cell.copy()* 0.8
    
    large_cell = atoms1.cell.copy()* 1.2
    
    
    cell_too_small, cell_too_large = checker.check_cell_volume(atoms1)
    
    assert (not cell_too_small) and (not cell_too_large)
    
    atoms1_small_cell=atoms1.copy()
    
    atoms1_small_cell.cell=small_cell
        
    cell_too_small, cell_too_large = checker.check_cell_volume(atoms1_small_cell)
    
    assert (cell_too_small) and (not cell_too_large)
    
    atoms1_large_cell=atoms1.copy()
    
    atoms1_large_cell.cell=large_cell
        
    cell_too_small, cell_too_large = checker.check_cell_volume(atoms1_large_cell)
    
    assert (not cell_too_small) and (cell_too_large)
    
    # check the angles    
    assert checker.check_cell_angles(atoms1)
    
    new_angle = 35  # in degrees
    angle=new_angle*np.pi/180
    X=np.array([1.0, 0.0, 0.0])*5
    Y=np.array([np.cos(angle),np.sin(angle),0])*5
    Z=np.array([0,0,1])*5
    new_cell=np.array([X,Y,Z])
    atoms_angle_35=atoms1.copy()
    atoms_angle_35.cell=new_cell
    atoms_angle_35.center()
    
    assert not checker.check_cell_angles(atoms_angle_35)
 
    
    new_angle = 145  # in degrees
    angle=new_angle*np.pi/180
    X=np.array([1.0, 0.0, 0.0])*5
    Y=np.array([np.cos(angle),np.sin(angle),0])*5
    Z=np.array([0,0,1])*5
    new_cell=np.array([X,Y,Z])
    atoms_angle_145=atoms1.copy()
    atoms_angle_145.cell=new_cell
    atoms_angle_145.center()
    
    assert not checker.check_cell_angles(atoms_angle_145)
    
    

def test_beaconatomsio():

    # default params:
    atomsio = BEACONAtomsIO()
    assert exists('structures_dft.xyz')

    # test write_xyz:
    atoms1 = fcc100('Au', (2, 2, 1))
    atoms1.info.clear()
    atoms1.pbc = False
    atoms1.calc = EMT()
    atoms1.get_potential_energy()

    atomsio.write_xyz(atoms1, strtype='init')
    testatoms = read('init_structures.xyz@:')
    assert len(testatoms) == 1



def test_iterationresult():

    atoms1 = get_atoms()
    
    it_result = IterationResult(len(atoms1))
    
    assert it_result.is_better(1.0)

    it_result.update({'acq': 0.5})
    assert not it_result.is_better(1.0)

    assert it_result.get_min_rank() == 0










def test_calculator_interface():    
    
    atoms1 = get_atoms()
    
    e=atoms1.get_potential_energy()
        
    atoms1.calc=None

    calc=CalculatorInterface(calc=EMT)
    
    new_atoms=atoms1.copy()
    
    calc.calculate_properties(new_atoms)
    
    energy_from_interface, forces_from_iterface=calc.get_output(new_atoms)
    
    assert abs(e-energy_from_interface)/abs(e)<1e-10





def initialize_magmoms(atoms):
    '''
    Set magnetic moments to atoms according to the user-given
    information in magmoms dictionary. Return updated atoms.
    '''
    atoms = atoms.copy()
    magmominfo=dict(elements=['Fe', 'Ni'], magmoms=[2.0, 1.0])

    magmoms_array = np.zeros(len(atoms))

    for i in range(len(atoms)):
        for j in range(len(magmominfo['elements'])):
            if atoms[i].symbol == magmominfo['elements'][j]:
                magmoms_array[i] = magmominfo['magmoms'][j]
    atoms.set_initial_magnetic_moments(magmoms_array)
    return atoms

def test_import_of_DFT_preparation_method_to_calculator_interface():


    atoms1=get_atoms() 
    atoms1[1].symbol='Ni'
    atoms1[2].symbol='Fe'    
 
    calculator=CalculatorInterface(calc=EMT, 
                                   prepare_atoms_for_dft=initialize_magmoms)

    prepared_atoms=calculator.prepare_atoms_for_dft(atoms1)

    assert all(prepared_atoms.get_initial_magnetic_moments() == [0., 1., 2., 0.])
    
    
    
    
    
def test_UCB_acquisition_function():
    
    energy=10
    uncertainty=3
    
    acq=LowerBound(kappa=2)
    
    assert acq.get(energy, uncertainty) == 4

