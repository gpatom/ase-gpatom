#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  9 10:23:15 2023

@author: casper
"""

import numpy as np
from ase.build import fcc111
from ase.calculators.emt import EMT

from gpatom.gpfp.gp import GaussianProcess

from gpatom.gpfp.fingerprint import RadialAngularFP
from gpatom.gpfp.hpfitter import HyperparameterFitter, GPPrefactorFitter, PriorFitter, HpFitInterface
from hpfitter.pdistributions import Normal_prior
from gpatom.gpfp.prior import ConstantPrior
from gpatom.gpfp.hpfitter import PriorDistributionLogNormal

from gpatom.gpfp.avishart_hpfitter import HpFitterVariableRatio, HpFitterConstantRatio
from gpatom.gpfp.avishart_hpfitter import HpFitterVariableRatioParallel, HpFitterConstantRatioParallel

import pytest


def setup_gaussian_process():
    fp_class = RadialAngularFP

    gp_args = dict(prior=ConstantPrior(constant=0), 
                   hp={'scale': 2, 'weight': 1, 
                       'noise': 1e-5, 'ratio': 0.001, 'noisefactor': 1},
                   use_forces=True, use_noise_correction=False) 

    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1.0}

    # Create slab:
    slab = fcc111('Ag', size=(2, 2, 2))
    slab.center(vacuum=4.0, axis=2)
    slab.rattle(0.05)
    slab.calc = EMT()

    def new_atoms(rattle_seed=None):
        atoms = slab.copy()
        atoms.rattle(0.05, seed=rattle_seed)
        atoms.calc = EMT()
        return atoms

    frames = [new_atoms(rattle_seed=i) for i in range(3)]

    X = [fp_class(atoms=atoms, **fp_args) for atoms in frames]

    energyforces = [[atoms.get_potential_energy()] +
                    list(atoms.get_forces().flatten())
                    for atoms in frames]
    Y = np.array(energyforces).reshape(-1)
    
    gp = GaussianProcess(**gp_args)
    gp.train(X, Y)
    
    return gp, X, Y


def check_vicinity_prior(gp, X, Y):
    
    optimum_prior_const=gp.prior.constant
    
    logP_afterfit = HyperparameterFitter.logP(gp) 
    
    gp.set_hyperparams({'prior': optimum_prior_const * 0.9})
    gp.train(X,Y)    
    logP_toosmallprior = HyperparameterFitter.logP(gp)  
    
    gp.set_hyperparams({'prior': optimum_prior_const * 1.1})
    gp.train(X,Y)    
    logP_toobigprior = HyperparameterFitter.logP(gp)  

    # set back to optimum
    gp.prior.constant=optimum_prior_const
    gp.train(X,Y)

    return (logP_afterfit > logP_toobigprior) and  (logP_afterfit > logP_toosmallprior)


def check_vicinity_weight(gp, X, Y):
    
    optimum_weight=gp.hp['weight']
    
    logP_afterfit= HyperparameterFitter.logP(gp) 
    
    gp.set_hyperparams({'weight': optimum_weight*0.9})
    gp.set_hyperparams({'noise': gp.hp['ratio']*optimum_weight*0.9})

    gp.train(X,Y)    
    logP_toosmallweight = HyperparameterFitter.logP(gp) 

    gp.set_hyperparams({'weight': optimum_weight*1.1})
    gp.set_hyperparams({'noise': gp.hp['ratio']*optimum_weight*1.1})
    
    gp.train(X,Y) 
    logP_toobigweight = HyperparameterFitter.logP(gp) 
    
    # set back to optimal
    gp.set_hyperparams({'weight': optimum_weight})
    gp.set_hyperparams({'noise': gp.hp['ratio']*optimum_weight})
    gp.train(X,Y)
    
    return (logP_afterfit > logP_toobigweight) and  (logP_afterfit > logP_toosmallweight)


def check_vicinity_scale(gp, X, Y):
    
    optimum_scale=gp.hp['scale']
    
    logP_afterfit = HyperparameterFitter.logP(gp) 

    gp.set_hyperparams({'scale': optimum_scale * 0.9})
    gp.train(X,Y)    
    logP_toosmallscale = HyperparameterFitter.logP(gp)  

    gp.set_hyperparams({'scale': optimum_scale * 1.1})
    gp.train(X,Y)   
    logP_toobigscale = HyperparameterFitter.logP(gp)  
    
    # set back to optimum
    gp.set_hyperparams({'scale': optimum_scale})
    gp.train(X,Y)
    
    return (logP_afterfit > logP_toobigscale) and  (logP_afterfit > logP_toosmallscale)


def test_fit_prior():
    # test that the optimized prior consatnt is a maximum on log-likelihood space       
    gp, X, Y = setup_gaussian_process()
    
    gp=PriorFitter.fit(gp)
    
    is_optimum_prior = check_vicinity_prior(gp, X, Y)
    
    assert is_optimum_prior


def test_fit_weight():
    # test that the optimized prefactor is a maximum on log-likelihood space
    
    gp, X, Y = setup_gaussian_process()

    gp=GPPrefactorFitter.fit(gp)
    
    is_optimum_weight = check_vicinity_weight(gp, X, Y)
    
    assert is_optimum_weight


def test_fit_scale():
    # test that the optimized length scale is a maximum on log-likelihood space    

    gp, X, Y = setup_gaussian_process()


    gp, results=HyperparameterFitter.fit(gp, params_to_fit=['scale'], 
                                         fit_weight=False, fit_prior=False, 
                                         pd=None)
    
    is_optimum_scale = check_vicinity_scale(gp, X, Y)
    
    assert is_optimum_scale


def test_HpFitInterFace():
    
    gp, X, Y = setup_gaussian_process()
    
    hp_optimizer=HpFitInterface(scale_prior=None, fit_scale_interval=1, 
                                fit_weight_interval=1, fit_prior_interval=1)

    hp_optimizer.fit(gp) 




    is_optimum_scale = check_vicinity_scale(gp, X, Y)    
    assert is_optimum_scale

    is_optimum_prior = check_vicinity_prior(gp, X, Y)
    assert is_optimum_prior
    

    is_optimum_weight=check_vicinity_weight(gp, X, Y)   
    assert is_optimum_weight

   
def test_pd_log_normal():
    # test that the lognormal prior has its optimum at exp(3)
    
    pd_log_normal = PriorDistributionLogNormal(loc=3, width=0.1)
    
    log_scales=[0. , 0.5, 1. , 1.5, 2. , 2.5, 3. , 3.5, 4. ]
    
    results=[pd_log_normal.get( np.exp(scale) ) for scale in log_scales]
 
    max_idx=np.argmax(results)
    
    assert log_scales[max_idx]==3
    

def test_scale_fit_with_prior():    
    # check that the optimized length scale is the same as that of the
    # lognormal prior for small lognormal widths
    
    gp, X, Y = setup_gaussian_process()

    pd_log_normal = PriorDistributionLogNormal(loc=3, width=0.0001)
    

    gp, results=HyperparameterFitter.fit(gp, params_to_fit=['scale'], 
                                         fit_weight=False, fit_prior=False, 
                                         pd=pd_log_normal)
 
    assert np.isclose(gp.hp['scale'] , np.exp(3)) 
    

def test_scale_fit_with_prior_in_HpFitInterface():    
    # check that the optimized length scale is the same as that of the
    # lognormal prior for small lognormal widths
    
    gp1, X, Y = setup_gaussian_process()
    hp_optimizer1=HpFitInterface(scale_prior=None,fit_scale_interval=1, 
                                fit_weight_interval=np.inf, fit_prior_interval=np.inf)
    
    hp_optimizer1.fit(gp1) 

    scale1=gp1.hp['scale']
    
    pd_log_normal = PriorDistributionLogNormal(loc=np.log(scale1*3), width=0.0001)

    gp2, X, Y = setup_gaussian_process()
    hp_optimizer2=HpFitInterface(scale_prior=pd_log_normal,fit_scale_interval=1, 
                                fit_weight_interval=np.inf, fit_prior_interval=np.inf)

    hp_optimizer2.fit(gp2 )
    
    scale2=gp2.hp['scale']
    
    assert scale2 > scale1
    

@pytest.mark.parametrize('hp_optimizer', [HpFitterConstantRatio(fit_scale_interval=1, 
                                                                          fit_weight_interval=1, 
                                                                          fit_prior_interval=1),
                                          HpFitterConstantRatioParallel(fit_scale_interval=1, 
                                                                        fit_weight_interval=1, 
                                                                        fit_prior_interval=1)])
def test_HpFitterConstantRatio_is_optimum(hp_optimizer):
    
    gp, X, Y = setup_gaussian_process()
    
    gp.use_noise_correction=True
    
    hp_optimizer.fit(gp) 
    
    is_optimum_scale = check_vicinity_scale_avishart(gp, X, Y, hp_optimizer)    
    assert is_optimum_scale
    
    is_optimum_weight=check_vicinity_weight_avishart(gp, X, Y, hp_optimizer)   
    assert is_optimum_weight


@pytest.mark.parametrize('hp_optimizer', [HpFitterVariableRatio(fit_scale_and_ratio_interval=1,
                                                                          fit_prior_interval=1), 
                                          HpFitterVariableRatioParallel(fit_scale_and_ratio_interval=1,
                                                                        fit_prior_interval=1)])
def test_HpFitterVariableRatio_is_optimum(hp_optimizer):
    
    gp, X, Y = setup_gaussian_process()
    
    gp.use_noise_correction=True
    
    hp_optimizer.fit(gp) 
 
    is_optimum_scale = check_vicinity_scale_avishart(gp, X, Y, hp_optimizer)    
    assert is_optimum_scale

    is_optimum_ratio = check_vicinity_ratio_avishart(gp, X, Y, hp_optimizer)
    assert is_optimum_ratio
        
    is_optimum_weight=check_vicinity_weight_avishart(gp, X, Y, hp_optimizer)   
    assert is_optimum_weight


def test_HpFitterConstantRatio_agreement():
    
    single_core=HpFitterConstantRatio(fit_scale_interval=1, 
                                                fit_weight_interval=1, 
                                                fit_prior_interval=1)
    
    parallel=HpFitterConstantRatioParallel(fit_scale_interval=1, 
                                           fit_weight_interval=1, 
                                           fit_prior_interval=1)
    
    gp1, X, Y = setup_gaussian_process()
    gp1.use_noise_correction=True
    single_core.fit(gp1) 
    
    gp2, X, Y = setup_gaussian_process()
    gp2.use_noise_correction=True
    parallel.fit(gp2) 
    
    hp_update_1=get_hp_list(gp1)
    hp_update_2=get_hp_list(gp2)
    
    assert np.allclose(hp_update_1,hp_update_2, atol=0, rtol=0.01)


def test_HpFitterVariableRatio_agreement():
    
    single_core=HpFitterVariableRatio(fit_scale_and_ratio_interval=1,
                                                fit_prior_interval=1)
    
    parallel=HpFitterVariableRatioParallel(fit_scale_and_ratio_interval=1,
                                           fit_prior_interval=1)
    
    gp1, X, Y = setup_gaussian_process()
    gp1.use_noise_correction=True
    single_core.fit(gp1)
    
    gp2, X, Y = setup_gaussian_process()
    gp2.use_noise_correction=True
    parallel.fit(gp2) 
    
    hp_update_1=get_hp_list(gp1)
    hp_update_2=get_hp_list(gp2)
    
    assert np.allclose(hp_update_1,hp_update_2, atol=0, rtol=0.01)
    
    
def get_hp_list(gp):
    
    hp_list=[gp.hp['scale'], gp.hp['weight'], gp.hp['ratio'],  
             gp.hp['corr'], gp.hp['noise'], gp.prior.constant] 
    
    return hp_list
    

def check_vicinity_scale_avishart(gp, X, Y, hp_optimizer):
    
    optimum_scale=gp.hp['scale']

    logP_afterfit = hp_optimizer.get_function_value(gp) 

    gp.set_hyperparams({'scale': optimum_scale * 0.9})
    gp.train(X,Y)    

    logP_toosmallscale = hp_optimizer.get_function_value(gp) 
    
    gp.set_hyperparams({'scale': optimum_scale * 1.1})
    gp.train(X,Y)   

    logP_toobigscale = hp_optimizer.get_function_value(gp)  
    
    # set back to optimal 
    gp.set_hyperparams({'scale': optimum_scale})
    gp.train(X,Y)
    
    return (logP_afterfit < logP_toobigscale) and  (logP_afterfit < logP_toosmallscale) 
    


def check_vicinity_weight_avishart(gp, X, Y, hp_optimizer):
    
    optimum_weight=gp.hp['weight']
    
    logP_afterfit = hp_optimizer.get_function_value(gp) 
    
    gp.set_hyperparams({'weight': optimum_weight*0.9})

    gp.train(X,Y)    
    logP_toosmallweight = hp_optimizer.get_function_value(gp) 

    gp.set_hyperparams({'weight': optimum_weight*1.1})
    
    gp.train(X,Y) 
    logP_toobigweight = hp_optimizer.get_function_value(gp) 
    
    #set back to optimal
    gp.set_hyperparams({'weight': optimum_weight})
    gp.train(X,Y)
    
    return (logP_afterfit < logP_toobigweight) and  (logP_afterfit < logP_toosmallweight)


def check_vicinity_ratio_avishart(gp, X, Y, hp_optimizer):
    
    optimum_ratio=gp.hp['ratio']
    
    logP_afterfit = hp_optimizer.get_function_value(gp) 
    
    gp.set_hyperparams({'ratio': optimum_ratio*0.9})
    gp.train(X,Y)    
    logP_toosmallratio = hp_optimizer.get_function_value(gp) 

    gp.set_hyperparams({'ratio': optimum_ratio*1.1})
    gp.train(X,Y) 
    logP_toobigratio = hp_optimizer.get_function_value(gp) 
    
    # set back to optimal
    gp.set_hyperparams({'ratio': optimum_ratio})
    gp.train(X,Y)

    return (logP_afterfit < logP_toobigratio) and  (logP_afterfit < logP_toosmallratio)




@pytest.mark.parametrize('hp_optimizer_class', [HpFitterConstantRatio,
                                                HpFitterConstantRatioParallel])
def test_scale_bounds(hp_optimizer_class):

    # define a really narrow interval  
    def scale_bounds_method(fingerprints):
        return [1,1+1e-10]
        
    hpfitter=hp_optimizer_class(fit_scale_interval=1, 
                                fit_weight_interval=1, 
                                fit_prior_interval=1,
                                scale_bounds_method=scale_bounds_method)

    gp, X, Y = setup_gaussian_process()
    gp.use_noise_correction=True
    
    hpfitter.fit(gp)     
    hp_list=get_hp_list(gp)
    scale=hp_list[0]
    
    assert np.isclose(scale, 1, atol=1e-8, rtol=0)

    

@pytest.mark.parametrize('hp_optimizer_class', [HpFitterVariableRatio,
                                                HpFitterVariableRatioParallel])
def test_scale_and_ratio_bounds(hp_optimizer_class):
    
    # define a really narrow interval 
    def scale_bounds_method(fingerprints):
        return [1,1+1e-10]
    
    # define a really narrow interval 
    def ratio_bounds_method(targets):
        return [1,1+1e-10]
            
    hpfitter=HpFitterVariableRatio(fit_scale_and_ratio_interval=1,  
                                             fit_prior_interval=1,
                                             scale_bounds_method=scale_bounds_method,
                                             ratio_bounds_method=ratio_bounds_method)
    
    gp, X, Y = setup_gaussian_process()
    gp.use_noise_correction=True
    hpfitter.fit(gp)   
    hp_list=get_hp_list(gp)
    scale=hp_list[0]
    ratio=hp_list[2]
     
    assert np.isclose(scale, 1, atol=1e-8, rtol=0)
    assert np.isclose(ratio, 1, atol=1e-8, rtol=0)
    


@pytest.mark.parametrize('hp_optimizer_class', [HpFitterConstantRatio,
                                                HpFitterConstantRatioParallel])
def test_scale_prior(hp_optimizer_class):
    
    def scale_bounds_method(fingerprints):
        return [100, 1000]
    
    def scale_prior_method(fingerprints):
        return [500, 1.01]
        
    hpfitter1=hp_optimizer_class(fit_scale_interval=1, 
                                 fit_weight_interval=1, 
                                 fit_prior_interval=1, 
                                 scale_prior=None,
                                 scale_prior_method=None,
                                 scale_bounds_method=scale_bounds_method)

    hpfitter2=hp_optimizer_class(fit_scale_interval=1, 
                                 fit_weight_interval=1, 
                                 fit_prior_interval=1,
                                 scale_prior=Normal_prior(mu=0, std=10.0),
                                 scale_prior_method=scale_prior_method,
                                 scale_bounds_method=scale_bounds_method)
        
    
    gp, X, Y = setup_gaussian_process()
    gp.use_noise_correction=True
    
    hpfitter1.fit(gp)       
    hp_list=get_hp_list(gp)
    scale1=hp_list[0]

    hpfitter2.fit(gp)     
    hp_list=get_hp_list(gp)
    scale2=hp_list[0]
        
    scale1_is_close=np.isclose(scale1, 500, atol=10)
    scale2_is_close=np.isclose(scale2, 500, atol=10)
    
    assert not scale1_is_close
    assert scale2_is_close

    
    
@pytest.mark.parametrize('hp_optimizer_class', [HpFitterVariableRatio,   
                                                HpFitterVariableRatioParallel])
def test_scale_and_ratio_prior(hp_optimizer_class):
    
    def scale_bounds_method(fingerprints):
        return [100, 1000]
    
    def scale_prior_method(fingerprints):
        return [500, 1.01]

    def ratio_bounds_method(targets):
        return [0.01, 1]
    
    def ratio_prior_method():
        return [0.5, 1.001]

    
    hpfitter1=hp_optimizer_class(fit_scale_and_ratio_interval=1, 
                                 fit_prior_interval=1,
                                 scale_prior=None,
                                 ratio_prior=None,
                                 scale_prior_method=None,
                                 ratio_prior_method=None,
                                 scale_bounds_method=scale_bounds_method,
                                 ratio_bounds_method=ratio_bounds_method)

    hpfitter2=hp_optimizer_class(fit_scale_and_ratio_interval=1, 
                                 fit_prior_interval=1,
                                 scale_prior=Normal_prior(mu=0, std=10.0),
                                 ratio_prior=Normal_prior(mu=0, std=10.0),
                                 scale_prior_method=scale_prior_method,
                                 ratio_prior_method=ratio_prior_method,
                                 scale_bounds_method=scale_bounds_method,
                                 ratio_bounds_method=ratio_bounds_method)
        
    gp, X, Y = setup_gaussian_process()
    gp.use_noise_correction=True
    
    hpfitter1.fit(gp)     
    hp_list=get_hp_list(gp)
    scale1=hp_list[0]
    ratio1=hp_list[2]

    hpfitter2.fit(gp)     
    hp_list=get_hp_list(gp)
    scale2=hp_list[0]
    ratio2=hp_list[2]
        
    scale1_is_close=np.isclose(scale1, 500, atol=10)
    scale2_is_close=np.isclose(scale2, 500, atol=10)

    ratio1_is_close=np.isclose(ratio1, 0.5, atol=1e-2)
    ratio2_is_close=np.isclose(ratio2, 0.5, atol=1e-2)
        
    assert not scale1_is_close
    assert scale2_is_close

    assert not ratio1_is_close
    assert ratio2_is_close    
    
    
    
    