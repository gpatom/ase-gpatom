#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  2 15:52:47 2023

@author: casper
"""

import numpy as np
import pytest
from ase.build import bulk
from ase import Atoms
from gpatom.fractional_beacon.unitcell_handler import UnitCellHandler
from ase.calculators.emt import EMT
from ase.stress import voigt_6_to_full_3x3_stress

from gpatom.fractional_beacon.icebeacon import CoordinateTransformer
from gpatom.fractional_beacon.hsicebeacon import HSCoordinateTransformer

def test_UnitCellHandler_cyclic_transformations():
    atoms = bulk('CuAu', 'rocksalt', a=4.8)
    atoms = atoms.repeat((2, 2, 3))
    atoms.rattle(0.001, seed=13)
    atoms.pbc = True

    atoms.calc=EMT()
    forces=atoms.get_forces()
    stress=atoms.get_stress()

    original_cell=atoms.get_cell()    
    
    # change the system slightly so we check that it doesnt work just for starting point
    # do this py expanding the cell and all atoms coorespondingly
    d=1.1
    x = np.eye(3)
    x[0, 0] = +d
    x[1, 1] = +d
    x[2, 2] = +d
    
    cell=original_cell.copy()   
    atoms_d=atoms.copy()
    atoms_d.set_cell(np.dot(cell, x), scale_atoms=True)

    # check that changing system actually happened
    assert not np.allclose( atoms.positions.flatten(),  atoms_d.positions.flatten())
    assert not np.allclose( atoms.cell.flatten(),  atoms_d.cell.flatten())   

    cell_factor=float(len(atoms))
    
    # go to deformed coordinates
    deformation_tensor, deformed_positions = UnitCellHandler.atoms_real_to_deformed(atoms_d, original_cell, cell_factor) 
     
    deformed_forces, deformed_virial = UnitCellHandler.forces_real_to_deformed(atoms_d, forces, stress, original_cell, cell_factor)
    
    # check that deformed coordinates are actually different from real coordinates 
    assert not np.allclose( atoms_d.positions.flatten(),  deformed_positions.flatten())
    assert not np.allclose( atoms_d.cell.flatten(),  deformation_tensor.flatten())    
    assert not np.allclose( forces.flatten(),  deformed_forces.flatten())
    assert not np.allclose( voigt_6_to_full_3x3_stress(stress).flatten(),  deformed_virial.flatten() ) 
    
    # go back to real coordinates
    atoms_d_remade=UnitCellHandler.atoms_deformed_to_real(atoms_d, deformation_tensor, deformed_positions, original_cell, cell_factor)    
    
    forces_remade, stress_remade = UnitCellHandler.forces_deformed_to_real(deformation_tensor, deformed_forces, deformed_virial, atoms_d, cell_factor)  
  
    # check that transformations are cyclic
    assert np.allclose( atoms_d.positions.flatten(),  atoms_d_remade.positions.flatten())
    assert np.allclose( atoms_d.cell.flatten(),  atoms_d_remade.cell.flatten())
    assert np.allclose( forces.flatten(),  forces_remade.flatten())
    assert np.allclose( stress.flatten(),  stress_remade.flatten())
    
    
    
def test_CoordinateTransformer_cyclic_transformations():
    atoms = bulk('CuAu', 'rocksalt', a=4.8)
    atoms = atoms.repeat((2, 2, 3))
    atoms.rattle(0.001, seed=13)
    atoms.pbc = True
    
    rng=np.random.RandomState(757)
    
    positions=atoms.positions.copy()
    
    fractions=rng.rand( len(atoms),2 )
    
    # making some fictive graidents
    gradients=rng.rand(len(atoms), 3)
    
    fraction_gradients=rng.rand(len(atoms), 2)
    
    coord_rescale=3
    
    fraction_rescale=2
    
    # make transformed coordinates:
    t_positions = CoordinateTransformer.positions_real_to_transformed(positions, coord_rescale)

    t_gradients = CoordinateTransformer.coord_gradients_real_to_transformed(gradients, coord_rescale)
    
    t_fractions = CoordinateTransformer.fractions_real_to_transformed(fractions, fraction_rescale)
    
    t_fraction_gradients = CoordinateTransformer.fraction_gradients_real_to_transformed(fraction_gradients, fraction_rescale)
    
    # asser that all transformations had an effect
    assert not np.allclose( t_positions.flatten(),  positions.flatten())
    assert not np.allclose( t_gradients.flatten(),  gradients.flatten())
    assert not np.allclose( t_fractions.flatten(), fractions.flatten())
    assert not np.allclose( t_fraction_gradients.flatten(),  fraction_gradients.flatten())    
    
    
    positions_remade = CoordinateTransformer.positions_transformed_to_real(t_positions, coord_rescale)

    gradients_remade = CoordinateTransformer.coord_gradients_transformed_to_real(t_gradients, coord_rescale)
    
    fractions_remade = CoordinateTransformer.fractions_transformed_to_real(t_fractions, fraction_rescale)
    
    fraction_gradients_remade = CoordinateTransformer.fraction_gradients_transformed_to_real(t_fraction_gradients, fraction_rescale)
    
    
    assert np.allclose( positions_remade.flatten(),  positions.flatten())
    assert np.allclose( gradients_remade.flatten(),  gradients.flatten())
    assert np.allclose( fractions_remade.flatten(), fractions.flatten())
    assert np.allclose( fraction_gradients_remade.flatten(),  fraction_gradients.flatten())  
    
    
    
def test_HSCoordinateTransformer_cyclic_transformations():
    atoms = bulk('CuAu', 'rocksalt', a=4.8)
    atoms = atoms.repeat((2, 2, 3))
    atoms.rattle(0.001, seed=13)
    atoms.pbc = True
    
    rng=np.random.RandomState(757)
    
    positions=atoms.positions.copy()
    
    extra_coords=2*rng.rand( len(atoms),2 )-1
    
    fractions=rng.rand( len(atoms),2 )
    
    # making some fictive graidents
    gradients=rng.rand(len(atoms), 3)
    
    extra_gradients=rng.rand(len(atoms), 2)
    
    fraction_gradients=rng.rand(len(atoms), 2)
    
    coord_rescale=3
    
    fraction_rescale=2
    
    # make transformed coordinates:
    t_positions, t_extra_coords = HSCoordinateTransformer.positions_real_to_transformed(positions, extra_coords, coord_rescale)

    t_gradients, t_extra_gradients = HSCoordinateTransformer.coord_gradients_real_to_transformed(gradients, extra_gradients, coord_rescale)
    
    t_fractions = HSCoordinateTransformer.fractions_real_to_transformed(fractions, fraction_rescale)
    
    t_fraction_gradients = HSCoordinateTransformer.fraction_gradients_real_to_transformed(fraction_gradients, fraction_rescale)
    
    # asser that all transformations had an effect
    assert not np.allclose( t_positions.flatten(),  positions.flatten())
    assert not np.allclose( t_gradients.flatten(),  gradients.flatten())
    assert not np.allclose( t_extra_gradients.flatten(), extra_gradients.flatten())
    assert not np.allclose( t_fractions.flatten(), fractions.flatten())
    assert not np.allclose( t_fraction_gradients.flatten(),  fraction_gradients.flatten())    
    
    
    positions_remade, extra_coords_remade = HSCoordinateTransformer.positions_transformed_to_real(t_positions, t_extra_coords, coord_rescale)

    gradients_remade, extra_gradients_remade = HSCoordinateTransformer.coord_gradients_transformed_to_real(t_gradients, t_extra_gradients, coord_rescale)
    
    fractions_remade = HSCoordinateTransformer.fractions_transformed_to_real(t_fractions, fraction_rescale)
    
    fraction_gradients_remade = HSCoordinateTransformer.fraction_gradients_transformed_to_real(t_fraction_gradients, fraction_rescale)
    
    
    assert np.allclose( positions_remade.flatten(),  positions.flatten())
    assert np.allclose( gradients_remade.flatten(),  gradients.flatten())
    assert np.allclose( extra_gradients_remade.flatten(), extra_gradients.flatten())
    assert np.allclose( fractions_remade.flatten(), fractions.flatten())
    assert np.allclose( fraction_gradients_remade.flatten(),  fraction_gradients.flatten())  
    