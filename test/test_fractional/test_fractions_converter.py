#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 12 01:12:22 2023

@author: casper
"""
import numpy as np
from ase import Atoms
from gpatom.fractional_beacon.icebeacon import AtomsConverter, GhostHandler, ICEFractionConverter
from gpatom.beacon.str_gen import RandomBranch


def test_FractionConverter():
    
    atoms = Atoms(['Au'] * 3 + ['Cu'] * 3 + ['Ni'] * 2 + ['Ag'] *2, positions=[[0., 0., 0.]] * 10)
    atoms.pbc = False
    atoms.center(vacuum=6.0)
    
    constrained_fractions=[0,5]
    
    fractional_elements=['Au', 'Cu', 'Ag']
    
    # assert that fmask, and n_0 production is correct
    fmask, n_0 = ICEFractionConverter.set_fractional_atoms(atoms, fractional_elements)
    
    assert  all(fmask==np.array([1,1,1,1,1,1,0,0,1,1],dtype=bool))
    assert  all(n_0==np.array([3,3,2]))
    
    
    # assert that atoms2fraction conversion is correct
    fractions = ICEFractionConverter.atoms2fractions(atoms, fractional_elements)
    
    expected_fractions=np.array([[1,0,0], 
                                 [1,0,0], 
                                 [1,0,0], 
                                 [0,1,0], 
                                 [0,1,0],
                                 [0,1,0],
                                 [0,0,0],
                                 [0,0,0],
                                 [0,0,1],
                                 [0,0,1]])     
    
    assert np.array_equal(fractions, expected_fractions)
    
    
    # assert that fractions2atoms conversion is correct
    fractions=np.array([[1.0, 0.0, 0.0],
                        [0.3, 0.4, 0.3],
                        [0.1, 0.9, 0.0],
                        [0.2, 0.1, 0.7],
                        [0.4, 0.1, 0.5],
                        [0.0, 1.0, 0.0], 
                        [0.0, 0.0, 0.0],
                        [0.0, 0.0, 0.0],
                        [0.4, 0.1, 0.5],
                        [0.6, 0.4, 0.0]])
    
    assert all(np.sum(fractions,axis=0)==np.array([3,3,2]))

    atoms = ICEFractionConverter.fractions2atoms(fractions, atoms, fractional_elements, constrained_fractions)
    
    # notice in case of a draw, the symbol is picked from the bottom most atom
    expected_symbols=['Au', 'Cu', 'Cu', 'Ag', 'Au', 'Cu', 'Ni', 'Ni', 'Ag', 'Au']
    
    found_symbols=[]
    for atom in atoms:
        found_symbols.append(atom.symbol)
        
    print('found' ,found_symbols)
    print('expected', expected_symbols)
    assert (found_symbols==expected_symbols)
    

def test_GhostHandler():
    
    atoms = Atoms(['Au'] * 5 + ['Cu'] * 3+ ['Ni']*2, positions=[[0., 0., 0.]] * 10)
    atoms.pbc = False
    atoms.center(vacuum=6.0)
    
    fractional_elements=['Au', 'Cu']
    
    sgen=RandomBranch(atoms, rng=np.random.RandomState(7680))
    
    atoms0=sgen.get()
       
    constrained_atoms=[0,1]
    
    n_ghost=np.array([2,1])
    
    fractions=np.array([1, 1, 0.1, 0.5, 0.4, 1, 0.4, 0.6, 1, 1])
    
    selected_atoms=GhostHandler.generate_ghosts_constrained(atoms0, fractions, n_ghost, constrained_atoms, fractional_elements)    

    assert all( selected_atoms == np.array( [1, 1, 0, 1, 0, 1, 0, 1, 1, 1], dtype=bool) )

    processed_atoms=GhostHandler.construct_processed_atoms_object(atoms0, selected_atoms)
    
    pos0=atoms0.get_positions()
    
    pos_pros=processed_atoms.get_positions()
    
    assert len(processed_atoms)==7
    assert np.allclose(pos_pros[0,:], pos0[0,:])
    assert np.allclose(pos_pros[1,:], pos0[1,:])  
    assert np.allclose(pos_pros[2,:], pos0[3,:])
    assert np.allclose(pos_pros[3,:], pos0[5,:])  
    assert np.allclose(pos_pros[4,:], pos0[7,:])
    assert np.allclose(pos_pros[5,:], pos0[8,:])  
    assert np.allclose(pos_pros[6,:], pos0[9,:])
    
    symbols=processed_atoms.get_chemical_symbols()
    assert symbols==['Au','Au','Au','Cu','Cu','Ni','Ni']




def test_fractions_to_atoms_conversion():
    atoms = Atoms(['Au'] * 3 + ['Mn']*2 +  ['Cu'] * 2 + ['Ni'] *1 + ['Ag']*1 +['Al']*2+['Co']*1, positions=[[0., 0., 0.]] * 12) 

    elements=['Au', 'Mn', 'Cu', 'Ni', 'Ag', 'Al', 'Co']      
    ice_elements=[[True,True, True, False,False,False,False], 
                  [False, False, False,True,True,False,False]] 

    n_real=[3,2,1,1,1,1,1]
    
    constrained_fractions=[0]
        
    fractions=[[1.,   0.,   0.,   0.,   0.,   0.,   0.  ],
               [0.29, 0.24, 0.28, 0.,   0.,   0.,   0.  ],
               [0.25, 0.29, 0.2,  0.,   0.,   0.,   0.  ],
               [0.04, 0.21, 0.22, 0.,   0.,   0.,   0.  ],
               [0.86, 0.09, 0.04, 0.,   0.,   0.,   0.  ],
               [0.45, 0.3,  0.24, 0.,   0.,   0.,   0.  ],
               [0.1,  0.87, 0.01, 0.,   0.,   0.,   0.  ],
               [0.,   0.,   0.,   0.81, 0.19, 0.,   0.  ],
               [0.,   0.,   0.,   0.19, 0.81, 0.,   0.  ],
               [0.,   0.,   0.,   0.,   0.,   0.12, 0.  ],
               [0.,   0.,   0.,   0.,   0.,   0.88, 0.  ],
               [0.,   0.,   0.,   0.,   0.,   0.,   1.  ]]
    
    fractions=np.array(fractions)

    existence_fractions=[1.,  0.81, 0.74, 0.47, 0.99, 0.99, 0.98, 1.,   1.,   0.12, 0.88, 1.  ]


    # test that conversion of fractions to atomic types work as intended
    whole_atoms=AtomsConverter.ice_convert(atoms, fractions, 
                                           constrained_fractions, 
                                           elements, ice_elements)
    
    expected_whole_atoms_symbols=['Au', 'Cu', 'Mn', 'Cu', 'Au', 'Au', 'Mn', 'Ni', 'Ag', 'Al', 'Al', 'Co']

    found_whole_atoms_symbols=[]
    for atom in whole_atoms:
        found_whole_atoms_symbols.append(atom.symbol)
        
    assert expected_whole_atoms_symbols==found_whole_atoms_symbols
        
    
    # test that conversion of fractions to ghost atoms work as intended
    deghosted_whole_atoms=AtomsConverter.ghost_convert(whole_atoms, fractions, 
                                                       constrained_fractions, 
                                                       elements, n_real)
        
    expected_deghosted_symbols=['Au', 'Cu', 'Mn', 'Au', 'Au', 'Mn', 'Ni', 'Ag', 'Al', 'Co']
    
    found_deghosted_symbols=[]
    for atom in deghosted_whole_atoms:
        found_deghosted_symbols.append(atom.symbol)
    
    assert expected_deghosted_symbols==found_deghosted_symbols
    
    
    # test that the two work as intended together
    converted_atoms=AtomsConverter.fractions2atoms(atoms, fractions, constrained_fractions, elements, n_real, ice_elements)    
    
    found_converted_atoms_symbols=[]
    for atom in converted_atoms:
        found_converted_atoms_symbols.append(atom.symbol)
    
    assert expected_deghosted_symbols==found_converted_atoms_symbols
    
    
def test_release_of_extra_existence():
    
    atoms = Atoms(['Au']*4 + ['Cu']*2 + ['Al']*2 + ['Ni']*1, positions=[[0., 0., 0.]] * 9) 

    elements=['Au', 'Cu', 'Al', 'Ni']      
    ice_elements=[[True, True, False, False]] 

    n_real=[2,2,1,1]
    
    constrained_fractions=[]
        
    fractions = [[0.36, 0.29, 0.,   0.  ],
                 [0.68, 0.25, 0.,   0.  ],
                 [0.59, 0.04, 0.,   0.  ],
                 [0.03, 0.86, 0.,   0.  ],
                 [0.04, 0.45, 0.,   0.  ],
                 [0.70, 0.11, 0.,   0.  ],
                 [0.,   0.,   0.85, 0.  ],
                 [0.,   0.,   0.35, 0.  ],
                 [0.,   0.,   0.,   1.  ]]
    
    fractions_before=np.array(fractions)

    # just check that 
    assert np.allclose( np.sum(fractions,axis=0), [ 2.4, 2.0, 1.2, 1.0 ] ) 
    
    fractions_after=AtomsConverter.release_extra_existence(atoms, fractions_before, 
                                                           constrained_fractions, 
                                                           elements, n_real)
    
    expected_fractions_after = [[0.03, 0.29, 0.,   0.  ],
                                [0.68, 0.25, 0.,   0.  ],
                                [0.59, 0.04, 0.,   0.  ],
                                [0.00, 0.86, 0.,   0.  ],
                                [0.00, 0.45, 0.,   0.  ],
                                [0.70, 0.11, 0.,   0.  ],
                                [0.,   0.,   0.85, 0.  ],
                                [0.,   0.,   0.15, 0.  ],
                                [0.,   0.,   0.,   1.  ]]
    
    assert np.allclose (fractions_after, expected_fractions_after)
    
    assert np.allclose( np.sum(fractions_after,axis=0), [ 2.0, 2.0, 1.0, 1.0 ] ) 
    
    
def test_get_fractional_mask():
    
    atoms = Atoms(['Au']*4 + ['Cu']*2 + ['Al']*2 + ['Ni']*1, positions=[[0., 0., 0.]] * 9) 

    elements=['Au', 'Cu', 'Al', 'Ni']      
    ice_elements=[True, True, False, True]
     
    constrained_fractions=[0, 2]
    
    ice_mask=AtomsConverter.get_fractional_mask(atoms, elements, 
                                                ice_elements, 
                                                constrained_fractions=constrained_fractions)
    
    expected_ice_mask= [False, True, False, True, True, True, False, False, True]
    
    assert np.all(ice_mask==expected_ice_mask)
    
    
def test_get_full_ice_elements():

    atoms = Atoms(['Au']*4 + ['Cu']*2 + ['Al']*2 + ['Ni']*1 +['Mn']*1, positions=[[0., 0., 0.]] * 10) 

    elements=['Au', 'Cu', 'Al', 'Ni', 'Mn']

    ice_elements=[[True, True,  False, False, False] ,   
                  [False, False, False, True, False] ]

    full_ice_elements = AtomsConverter.get_full_ice_elements(atoms, elements, ice_elements)    
    
    expected_full_ice_elements=[True, True, False, True, False]
    
    assert np.all(full_ice_elements==expected_full_ice_elements)
    
    
def test_get_n_ghost():
    
    atoms = Atoms(['Au']*4 + ['Cu']*2 + ['Al']*2 + ['Ni']*1, positions=[[0., 0., 0.]] * 9) 

    n_real=[2,1,1,1]
    
    elements=['Au', 'Cu', 'Al', 'Ni']
    
    n_ghost, ghost_elements = AtomsConverter.get_n_ghost(atoms, n_real, elements)
    
    expected_n_ghost=[2,1,1,0]
    
    expected_ghost_elements = [True, True, True, False]
    
    assert np.all( expected_n_ghost == n_ghost)
    assert np.all( ghost_elements == expected_ghost_elements)
    
    
    