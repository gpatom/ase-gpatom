from gpatom.fractional_beacon.gpfp.fingerprint import PartialFingerPrint, HighDimPartialFingerPrint
from gpatom.gpfp.fingerprint import FingerPrint
import numpy as np

from ase import Atoms
from gpatom.beacon.str_gen import RandomBranch
from ase.calculators.emt import EMT
from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.gpfp.gp import GaussianProcess
from gpatom.fractional_beacon.gpfp.gp import ICEGaussianProcess
from gpatom.fractional_beacon.gpfp.prior import WeightedConstantPrior


def test_ice_gp_and_beacon_gp_predictions_agree():

    atoms = Atoms(['Au'] * 2 + ['Cu'] * 2, positions=[[0., 0., 0.]] * 4)
    atoms.pbc = True
    atoms.center(vacuum=6.0)
    
    rng = np.random.RandomState(9)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng) 

    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1}  

    ice_fp=PartialFingerPrint(fp_args=fp_args, calc_strain=True)

    fp=FingerPrint(fp_args=fp_args, calc_strain=True)    
    
    gp_args=dict(prior=WeightedConstantPrior(constant=0), 
                 hp={'scale': 1000}, use_forces=True)
    
    ice_gp=ICEGaussianProcess(**gp_args)
    
    gp=GaussianProcess(**gp_args)      
    
    ice_model=Model(gp=ice_gp, fp=ice_fp)
    
    model=Model(gp=gp, fp=fp)

    training_atoms=[]
    for i in range(2):
        atoms=sgen.get()
        atoms.calc=EMT()
        training_atoms.append(atoms)
            
    model.add_data_points(training_atoms)    
    
    ice_model.add_data_points(training_atoms)

    test_atoms=sgen.get()
    
    ice_energy, ice_forces, ice_unc = ice_model.get_energy_and_forces(test_atoms)
    
    energy, forces, unc = model.get_energy_and_forces(test_atoms)

    x_ice=ice_model.new_fingerprint(test_atoms)
    ice_stress = ice_model.gp.predict_stress(x_ice)

    x=fp.get(test_atoms)
    stress=model.gp.predict_stress(x)

    assert(np.allclose(ice_energy, energy, atol=0.0, rtol=0.01))
    assert(np.allclose(ice_forces.flatten(), forces.flatten(), atol=0.0, rtol=0.01))
    assert(np.allclose(ice_unc.flatten(), unc.flatten(), atol=0.0, rtol=0.01))    
    assert(np.allclose(ice_stress, stress, atol=0.0, rtol=0.01))
    
    
    


def test_highdimice_gp_and_beacon_gp_predictions_agree():

    atoms = Atoms(['Au'] * 2 + ['Cu'] * 2, positions=[[0., 0., 0.]] * 4)
    atoms.pbc = True
    atoms.center(vacuum=6.0)
    
    rng = np.random.RandomState(9)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng) 

    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1}  

    hsice_fp=HighDimPartialFingerPrint(fp_args=fp_args, calc_strain=True)

    fp=FingerPrint(fp_args=fp_args, calc_strain=True)    
    
    gp_args=dict(prior=WeightedConstantPrior(constant=0), 
                 hp={'scale': 1000}, use_forces=True)
    
    
    hsice_gp=ICEGaussianProcess(**gp_args)
    
    gp=GaussianProcess(**gp_args)      
    
    hsice_model=Model(gp=hsice_gp, fp=hsice_fp)
    
    model=Model(gp=gp, fp=fp)

    training_atoms=[]
    for i in range(2):
        atoms=sgen.get()
        atoms.calc=EMT()
        training_atoms.append(atoms)
            
    model.add_data_points(training_atoms)    
    
    hsice_model.add_data_points(training_atoms)

    test_atoms=sgen.get()
    
    hsice_energy, hsice_forces, hsice_unc = hsice_model.get_energy_and_forces(test_atoms)
    
    energy, forces, unc = model.get_energy_and_forces(test_atoms)

    x_hsice=hsice_model.new_fingerprint(test_atoms)
    hsice_stress = hsice_model.gp.predict_stress(x_hsice)

    x=fp.get(test_atoms)
    stress=model.gp.predict_stress(x)

    assert(np.allclose(hsice_energy, energy, atol=0.0, rtol=0.01))
    assert(np.allclose(hsice_forces.flatten(), forces.flatten(), atol=0.0, rtol=0.01))
    assert(np.allclose(hsice_unc.flatten(), unc.flatten(), atol=0.0, rtol=0.01))    
    assert(np.allclose(hsice_stress, stress, atol=0.0, rtol=0.01))

