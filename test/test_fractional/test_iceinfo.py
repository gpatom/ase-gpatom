#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 13 00:42:34 2024

@author: casper
"""
import numpy as np
from ase import Atoms
from gpatom.fractional_beacon.icebeacon import ICEInfo

def test_get_element_info():
    
    atoms = Atoms(['Au'] * 3 + ['Mn']*2 +  ['Cu'] * 2 + ['Ni'] *1 + ['Ag']*1 +['Al']*2+['Co']*1, positions=[[0., 0., 0.]] * 12) 

    ice_elements=[ [ 'Au', 'Ag', 'Al' ] , [ 'Mn', 'Ni' ] ]        

    elements, n_elements, ice_element_bools = ICEInfo.get_element_info(atoms, ice_elements)
        
    # get_element_info orders the elements alphabetically
    expected_elements=['Ag', 'Al', 'Au', 'Co', 'Cu', 'Mn', 'Ni' ]

    expected_n_elements=[1, 2, 3, 1, 2, 2, 1]

    expected_ice_elements_bools=[ [True, True, True, False, False, False, False] , 
                                  [False, False, False, False, False, True, True] ]
    
    assert np.all( elements == expected_elements)
    assert np.all( expected_n_elements == n_elements)
    assert np.all( expected_ice_elements_bools == ice_element_bools)
    
    
def test_ICEInfo():
    
    atoms = Atoms(['Au'] * 3 + ['Mn']*3 +  ['Cu'] * 2 + ['Ni'] *2 , positions=[[0., 0., 0.]] * 10) 

    atoms_real = Atoms(['Au'] * 2 + ['Mn']*1 +  ['Cu'] * 2 + ['Ni'] *1 , positions=[[0., 0., 0.]] * 6) 

    ice_elements=[ [ 'Au', 'Cu'] , [ 'Mn', 'Ni' ] ]        

    ice_info=ICEInfo(atoms, 
                     atoms_real=atoms_real, 
                     ice_elements=ice_elements,
                     lower_lim=0.05,
                     frac_cindex=[1,3])
        
    # get_element_info orders the elements alphabetically
    expected_elements=['Au', 'Cu', 'Mn', 'Ni']

    expected_n_elements=[2, 2, 1, 1]

    expected_ice_elements_bools=[ [True, True, False, False] , 
                                  [False, False, True, True] ]

    (elements,
    ice_bools,
    n_real,
    frac_lims,
    frac_cindex) = ice_info.get_info()
    
    assert np.all( elements == expected_elements)
    assert np.all( n_real == expected_n_elements)
    assert np.all( ice_bools == expected_ice_elements_bools)
    assert np.all( frac_lims == [0.05, 1])
    assert np.all( frac_cindex == [1,3])