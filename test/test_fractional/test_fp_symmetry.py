from gpatom.fractional_beacon.gpfp.fingerprint import PartialRadAngFP, HighDimPartialRadAngFP
from gpatom.fractional_beacon.gpfp.kerneltypes import ICEDistance
from ase.cluster import Icosahedron
import numpy as np
import pytest

def check_fingerprints_equality(fp1, fp2):

    
    vec1 = fp1.vector
    vec2 = fp2.vector

    d = ICEDistance.distance(fp1, fp2)
    
    # check atoms are different
    assert  not (fp1.atoms.positions == fp2.atoms.positions).all()
    
    distance_is_small = (d < 1e-4)
    vectors_are_equal = np.allclose(vec1, vec2, atol=1e-8)

    #if is_equal:
    assert distance_is_small
    assert vectors_are_equal

            

def test_partial_fingerprint_symmetries():
    
    rng = np.random.RandomState(53453)
                                
    slab = Icosahedron('Cu', noshells=2, latticeconstant=3.6)
    slab[10].symbol = 'Au'
    slab[11].symbol = 'Au'
    slab.center(vacuum=4.0)

    n_elements=len(set(slab.symbols))

    fractions = rng.random( (len(slab), n_elements) )
    
    fp = PartialRadAngFP

    fp0 = fp(atoms=slab.copy(), fractions=fractions)

    # TRANSLATION
    slab_trans=slab.copy()
    slab_trans.positions += rng.random(3)
    fp_trans = fp(atoms=slab_trans, fractions=fractions)
    
    check_fingerprints_equality(fp0, fp_trans)

    # ROTATION
    slab_rot=slab.copy()
    slab_rot.rotate(a=180. * rng.random(), v=rng.random(3), center='COM')
    fp_rot = fp(atoms=slab_rot, fractions=fractions)
    
    check_fingerprints_equality(fp0, fp_rot)

    # PERMUTATION (swap both positions and fractions)
    slab_perm=slab.copy()
    fractions_perm=fractions.copy()

    tmp = slab_perm[3].position.copy()
    slab_perm[3].position = slab_perm[4].position
    slab_perm[4].position = tmp
    
    tmp = fractions_perm[3].copy()
    fractions_perm[3] = fractions_perm[4]
    fractions_perm[4] = tmp

    fp_perm = fp(atoms=slab_perm, fractions=fractions_perm)
    
    check_fingerprints_equality(fp0, fp_perm)
    


@pytest.mark.parametrize('dimensions', [4,5])
def test_highdim_partial_fingerprint_symmetries(dimensions):

    slab = Icosahedron('Cu', noshells=2, latticeconstant=3.6)
    slab[10].symbol = 'Au'
    slab[11].symbol = 'Au'
    slab.center(vacuum=4.0)    

    rng = np.random.RandomState(13454)

    n_elements=len(set(slab.symbols))

    fractions = rng.random( (len(slab), n_elements) )

    extra_coords = rng.rand(len(slab),dimensions-3)-0.5
    
    fp = HighDimPartialRadAngFP

    fp0 = fp(atoms=slab.copy(), extra_coords=extra_coords, 
             fractions=fractions)

    # TRANSLATION
    slab_trans=slab.copy()
    slab_trans.positions += rng.random(3)
    extra_coords_trans = extra_coords.copy() + rng.random(dimensions-3)  
    fp_trans = fp(atoms=slab_trans, extra_coords=extra_coords_trans, 
                  fractions=fractions)

    check_fingerprints_equality(fp0, fp_trans)

    # ROTATION (just 3D.  4D rotation was too difficult to set up)   
    slab_rot=slab.copy()     
    slab_rot.rotate(a=180. * rng.random(), v=rng.random(3), center='COM')
    fp_rot = fp(atoms=slab_rot, extra_coords=extra_coords, 
                fractions=fractions)

    check_fingerprints_equality(fp0, fp_rot)    

    # PERMUTATION (swap both positions and extra_coords and fractions)
    slab_perm=slab.copy()    
    tmp = slab_perm[3].position.copy()
    slab_perm[3].position = slab_perm[4].position
    slab_perm[4].position = tmp
    
    extra_coords_perm=extra_coords.copy()
    tmp = extra_coords_perm[3,:].copy()
    extra_coords_perm[3,:]=extra_coords_perm[4,:]
    extra_coords_perm[4,:]=tmp
    
    fractions_perm=fractions.copy()
    tmp = fractions_perm[3].copy()
    fractions_perm[3] = fractions_perm[4]
    fractions_perm[4] = tmp
    
    fp_perm = fp(atoms=slab_perm, extra_coords=extra_coords_perm, 
                 fractions=fractions_perm)

    check_fingerprints_equality(fp0, fp_perm)
        
        
