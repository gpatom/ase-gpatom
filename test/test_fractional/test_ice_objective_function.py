import numpy as np
from gpatom.fractional_beacon.gpfp.fingerprint import PartialFingerPrint

from ase import Atoms

from ase.calculators.emt import EMT

from ase.data import covalent_radii, atomic_numbers

from gpatom.fractional_beacon.gpfp.gp import ICEGaussianProcess

from gpatom.fractional_beacon.icebeacon import (ICESurrogateOptimizer,  CoordinateTransformer, 
                                                ICEParamsHandler, ICEInfo, RandomFractionGenerator)

from gpatom.fractional_beacon.gpfp.prior import (WeightedCalculatorPrior, 
                                                 WeightedRepulsivePotential)

from gpatom.beacon.str_gen import RandomBox, AtomsRelaxer

from gpatom.gpfp.prior import RepulsivePotential

from gpatom.fractional_beacon.gpfp.atoms_gp_interface import PartialModel


from gpatom.fractional_beacon.icebeacon import UnitCellHandler



def numeric_gradients(natoms, positions, fractions, surropt, args, a, i, d=1e-6):
    """Compute numeric force on atom with index a, Cartesian component i,
    with finite step of size d
    """
    positions=positions.copy()

    positions[a, i] += d
    
    params=ICEParamsHandler.pack_atomic_params(natoms,
                                               len(surropt.elements), 
                                               positions, 
                                               fractions)
    
    e_plus, derivs_plus=surropt._calculate_properties(params, *args)

    positions[a, i] -= 2 * d
    
    params=ICEParamsHandler.pack_atomic_params(natoms,
                                               len(surropt.elements), 
                                               positions, 
                                               fractions)
    
    e_minus, derivs_minus=surropt._calculate_properties(params, *args)

    return (e_plus-e_minus) / (2 * d)



def numeric_fraction_gradients(natoms, positions, fractions, surropt, args, a, i, d=1e-6):
    
    fractions=fractions.copy()   
    
    fractions[a, i] += d
    
    params=ICEParamsHandler.pack_atomic_params(natoms,
                                               len(surropt.elements), 
                                               positions, 
                                               fractions)
    
    e_plus, derivs_plus=surropt._calculate_properties(params, *args)

    fractions[a, i] -= 2 * d
    
    params=ICEParamsHandler.pack_atomic_params(natoms,
                                               len(surropt.elements), 
                                               positions, 
                                               fractions)
    
    e_minus, derivs_minus=surropt._calculate_properties(params, *args)
    
    return  (e_plus-e_minus) / (2 * d)



def test_objective_function():
    
    #RUNNING THE SIMULATION

    atoms= Atoms(['Al'] * 3 + ['Cu']*2  , positions=[[0., 0., 0.]] * 5)
    atoms.pbc = False
    atoms.cell=[10,10,10]
    
    ice_info=ICEInfo(atoms, ice_elements=['Al', 'Cu'])
    
    rng = np.random.RandomState(88)

    r_atom = (covalent_radii[atomic_numbers['Al']] )  

    potential=RepulsivePotential(potential_type='parabola', 
                                 prefactor=10, rc=0.9)

    relaxer=AtomsRelaxer(potential, with_unit_cell=False)

    sgen = RandomBox(atoms, box=[(3,7)]*3, relaxer=relaxer, rng=rng)

    fp_args = {'r_cutoff': r_atom*4.25, 'a_cutoff': r_atom*2.25, 'aweight': 1}
    fp=PartialFingerPrint(fp_args=fp_args, 
                          angular=True, calc_strain=False)

    relax_potential=WeightedRepulsivePotential(potential_type='parabola', 
                                               prefactor=10, rc=0.9)

    prior=WeightedCalculatorPrior(calculator=relax_potential, constant=0)

    gp_args=dict(prior=prior,    
                 hp={'scale': 1000, 'weight': 100, 
                     'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1}, 
                 use_forces=True)
    
    gp=ICEGaussianProcess(**gp_args)

    model=PartialModel(gp=gp, fp=fp)

    training_set=[]
    for i in range(2):
        atoms_i=sgen.get()
        atoms_i.calc=EMT()
        training_set.append(atoms_i)

    model.add_data_points(training_set)

    rfg=RandomFractionGenerator(ice_info, randomtype='drs', rng=rng)

    # the most parameters are just here for placeholding as they a re not directly used in the objective method
    surropt=ICESurrogateOptimizer(ice_info,
                                  fmax=0.001, 
                                  random_fraction_generator=rfg,
                                  weak_existing_steps=5,
                                  after_steps=5, 
                                  post_rounding_steps=5,
                                  derivative_modulation=0.1,
                                  fraction_rescale=20,
                                  coord_rescale=100,
                                  write_surropt_trajs=True,
                                  with_unit_cell=False)
    surropt.steps_taken=0

    # get test_values
    original_atoms=sgen.get()    
    
    fractions=surropt.rfg.get_fractions(original_atoms)
    
    test_atoms=original_atoms.copy()
    positions=test_atoms.positions.copy()


    writer=surropt.initiate_writer(test_atoms, fractions, model, 'dummy')
    
    
    # transform params
    natoms=len(test_atoms)
    
    transformed_fractions=CoordinateTransformer.fractions_real_to_transformed(fractions, surropt.fraction_rescale)
            

    transformed_positions = CoordinateTransformer.positions_real_to_transformed(positions,  
                                                                                surropt.coord_rescale)
    
    params=ICEParamsHandler.pack_atomic_params(natoms, len(surropt.elements), 
                                               transformed_positions, 
                                               transformed_fractions)   

    args=(writer, test_atoms, model)


    # calculate objective function
    transformed_energy, transformed_derivatives = surropt._calculate_properties(params, *args)

    # get the derivatives
    (transformed_gradients, 
     transformed_fraction_gradients) = ICEParamsHandler.unpack_atomic_params(natoms,
                                                                             len(surropt.elements), 
                                                                             transformed_derivatives) 
                                                                                                                               
    numerical_gradients=np.zeros( np.shape(positions) )
        
    numerical_fraction_gradients=np.zeros( np.shape(fractions) )

    
    natoms=len(test_atoms)
    
    for a in range(len(atoms)):
        
        for i1 in range(3):
            numerical_gradients[a,i1]=numeric_gradients(natoms, transformed_positions, 
                                                        transformed_fractions, 
                                                        surropt, args, a, i1)
                        
        for i2 in range(len(surropt.elements)):
            numerical_fraction_gradients[a,i2]=numeric_fraction_gradients(natoms, 
                                                                          transformed_positions, 
                                                                          transformed_fractions, 
                                                                          surropt, args, a, i2)
                        
    print(numerical_gradients/transformed_gradients)

    print(numerical_fraction_gradients/transformed_fraction_gradients)
    
    assert np.allclose(numerical_gradients/transformed_gradients, 1, atol=1e-2, rtol=0 )
    
    assert np.allclose(numerical_fraction_gradients/transformed_fraction_gradients, 1, atol=1e-2, rtol=0 )
                                              








def numeric_gradients_uc(natoms, positions, fractions, deformation_tensor, 
                         surropt, args, a, i, d=1e-6):
    """Compute numeric force on atom with index a, Cartesian component i,
    with finite step of size d
    """
    positions=positions.copy()

    positions[a, i] += d
    
    params=ICEParamsHandler.pack_params(natoms, 
                                        len(surropt.elements), 
                                        positions, 
                                        fractions,
                                        deformation_tensor)
    
    e_plus, derivs_plus=surropt._calculate_properties_unitcell(params, *args)

    positions[a, i] -= 2 * d
    
    params=ICEParamsHandler.pack_params(natoms,
                                        len(surropt.elements), 
                                        positions, 
                                        fractions,
                                        deformation_tensor)
    
    e_minus, derivs_minus=surropt._calculate_properties_unitcell(params, *args)

    return (e_plus-e_minus) / (2 * d)




def numeric_fraction_gradients_uc(natoms, positions, fractions, 
                                  deformation_tensor, surropt, args, a, i, d=1e-6):
    
    fractions=fractions.copy()   
    
    fractions[a, i] += d
    
    params=ICEParamsHandler.pack_params(natoms,
                                        len(surropt.elements), 
                                        positions,
                                        fractions,
                                        deformation_tensor)
    
    e_plus, derivs_plus=surropt._calculate_properties_unitcell(params, *args)

    fractions[a, i] -= 2 * d
    
    params=ICEParamsHandler.pack_params(natoms, 
                                        len(surropt.elements), 
                                        positions, 
                                        fractions,
                                        deformation_tensor)
    
    e_minus, derivs_minus=surropt._calculate_properties_unitcell(params, *args)
    
    return  (e_plus-e_minus) / (2 * d)






def numeric_virial(natoms, positions, fractions, deformation_tensor, 
                   surropt, args, d=1e-6):
    
    
    virial = np.zeros((3, 3), dtype=float)

    for i in range(3):
        deformation_tensor[i, i] += d

        params=ICEParamsHandler.pack_params(natoms, 
                                            len(surropt.elements), 
                                            positions, 
                                            fractions,
                                            deformation_tensor)
    
        e_plus, derivs_plus=surropt._calculate_properties_unitcell(params, *args)
        


        deformation_tensor[i, i] -= 2 * d
        params=ICEParamsHandler.pack_params(natoms, 
                                            len(surropt.elements), 
                                            positions, 
                                            fractions,
                                            deformation_tensor)
    
        e_minus, derivs_minus=surropt._calculate_properties_unitcell(params, *args)

        virial[i, i] = (e_plus - e_minus) / (2 * d)
        deformation_tensor[i, i] += d  # set back

        j = i - 2
        deformation_tensor[i, j] = d
        deformation_tensor[j, i] = d

        params=ICEParamsHandler.pack_params(natoms,
                                            len(surropt.elements), 
                                            positions,
                                            fractions,
                                            deformation_tensor)
    
        e_plus, derivs_plus=surropt._calculate_properties_unitcell(params, *args)


        deformation_tensor[i, j] = -d
        deformation_tensor[j, i] = -d
      
        params=ICEParamsHandler.pack_params(natoms,
                                            len(surropt.elements), 
                                            positions, 
                                            fractions,
                                            deformation_tensor)
    
        e_minus, derivs_minus=surropt._calculate_properties_unitcell(params, *args)
      

        virial[i, j] = (e_plus - e_minus) / (4 * d)
        virial[j, i] = virial[i, j]

    return virial
    

    
def test_objective_function_unitcell():
    
    #RUNNING THE SIMULATION

    atoms= Atoms(['Al'] * 3 + ['Cu']*2  , positions=[[0., 0., 0.]] * 5)
    atoms.pbc = False
    atoms.cell=[10,10,10]
    
    ice_info=ICEInfo(atoms, ice_elements=['Al','Cu'])

    rng = np.random.RandomState(88)

    r_atom = (covalent_radii[atomic_numbers['Al']] )  

    potential=RepulsivePotential(potential_type='parabola', 
                                 prefactor=10, rc=0.9)

    relaxer=AtomsRelaxer(potential, with_unit_cell=True)

    sgen=RandomBox(atoms, box=[(3,7)]*3, relaxer=relaxer, rng=rng)


    fp_args = {'r_cutoff': r_atom*4.25, 'a_cutoff': r_atom*2.25, 'aweight': 1}
    fp=PartialFingerPrint(fp_args=fp_args, 
                          angular=True, calc_strain=True)

    relax_potential=WeightedRepulsivePotential(potential_type='parabola', 
                                               prefactor=10, rc=0.9)

    prior=WeightedCalculatorPrior(calculator=relax_potential, constant=0)

    gp_args=dict(prior=prior,    
                 hp={'scale': 1000, 'weight': 100, 
                     'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1}, 
                 use_forces=True)
    
    gp=ICEGaussianProcess(**gp_args)

    model=PartialModel(gp=gp, fp=fp)

    training_set=[]
    for i in range(2):
        atoms_i=sgen.get()
        atoms_i.calc=EMT()
        training_set.append(atoms_i)

    model.add_data_points(training_set)

    rfg=RandomFractionGenerator(ice_info, randomtype='drs', rng=rng)

    # the most parameters are just here for placeholding as they a re not directly used in the objective method
    surropt=ICESurrogateOptimizer(ice_info,
                                  fmax=0.001,
                                  random_fraction_generator=rfg,
                                  weak_existing_steps=5,
                                  after_steps=5, 
                                  post_rounding_steps=5,
                                  derivative_modulation=1,
                                  fraction_rescale=20,
                                  coord_rescale=100,
                                  cell_rescale=3,
                                  write_surropt_trajs=True,
                                  with_unit_cell=True)
    surropt.steps_taken=0

    # get test_values
    original_atoms=sgen.get()    
    
    fractions=surropt.rfg.get_fractions(original_atoms)
    
    original_cell=atoms.get_cell()
    
    # make a skewed cell
    d=1.1
    x = np.eye(3)
    x[0, 0] = +d
    x[1, 1] = +d
    x[2, 2] = +d
    
    cell=original_cell.copy()   
    test_atoms=original_atoms.copy()
    test_atoms.set_cell(np.dot(cell, x), scale_atoms=True)


    writer=surropt.initiate_writer(test_atoms, fractions, model, 'dummy')

    # check that changing system actually happened
    assert not np.allclose( original_atoms.positions.flatten(),  test_atoms.positions.flatten())
    assert not np.allclose( original_atoms.cell.flatten(),  test_atoms.cell.flatten())   
    
    
    # transform params
    natoms=len(test_atoms)
        
    cell_factor=surropt.cell_rescale*float(natoms) 
    
    transformed_fractions=CoordinateTransformer.fractions_real_to_transformed(fractions, surropt.fraction_rescale)
            
    deformation_tensor, deformed_positions = UnitCellHandler.atoms_real_to_deformed(test_atoms, original_cell, cell_factor)

    transformed_deformed_positions = CoordinateTransformer.positions_real_to_transformed(deformed_positions, surropt.coord_rescale)
                                                                                                                         
    params=ICEParamsHandler.pack_params(natoms, len(surropt.elements), 
                                        transformed_deformed_positions, 
                                        transformed_fractions, 
                                        deformation_tensor)   

    args=(writer, test_atoms, model, original_cell, cell_factor)


    # calculate objective function
    transformed_energy, transformed_derivatives = surropt._calculate_properties_unitcell(params, *args)

    # get the derivatives
    (transformed_deformed_gradients, 
     transformed_fraction_gradients,
     deformed_virial) = ICEParamsHandler.unpack_params(natoms,
                                                       len(surropt.elements), 
                                                       transformed_derivatives) 
                 
                                                               
    numerical_gradients=np.zeros( np.shape(deformed_positions) )
    
    numerical_fraction_gradients=np.zeros( np.shape(fractions) )
    
    for a in range(len(atoms)):
        
        for i1 in range(3):
            numerical_gradients[a,i1]=numeric_gradients_uc(natoms, transformed_deformed_positions, 
                                                           transformed_fractions, 
                                                           deformation_tensor, 
                                                           surropt, args, a, i1)
                        
        for i2 in range(len(surropt.elements)):
            numerical_fraction_gradients[a,i2]=numeric_fraction_gradients_uc(natoms, transformed_deformed_positions, 
                                                                             transformed_fractions, 
                                                                             deformation_tensor, 
                                                                             surropt, args, a, i2)


    numerical_virial=numeric_virial(natoms, transformed_deformed_positions, 
                                    transformed_fractions, 
                                    deformation_tensor, surropt, args)
            
    print(numerical_gradients/transformed_deformed_gradients)
    
    print(numerical_fraction_gradients/transformed_fraction_gradients)
    
    print(numerical_virial/deformed_virial)
    
    assert np.allclose(numerical_gradients/transformed_deformed_gradients, 1, atol=1e-2, rtol=0 )
    
    assert np.allclose(numerical_fraction_gradients/transformed_fraction_gradients, 1, atol=1e-2, rtol=0 )
    
    assert np.allclose(numerical_virial/deformed_virial, 1, atol=1e-2, rtol=0 ) 