#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 14:47:39 2023

@author: casper
"""

import pytest
import numpy as np
from ase.build import fcc111
from ase.data import covalent_radii

from ase import Atoms
from gpatom.beacon.str_gen import RandomBranch


from ase.stress import (full_3x3_to_voigt_6_stress,
                        voigt_6_to_full_3x3_stress)


from gpatom.fractional_beacon.gpfp.prior import WeightedHighDimRepulsivePotential

from gpatom.fractional_beacon.gpfp.prior import GhostExtraPotential

from ase.data import atomic_numbers


def get_energy(atoms):
    atoms.calc.results.clear() 
    energy=atoms.get_potential_energy()
    return energy


def numeric_force(atoms, a, i, d=1e-6):
    """Compute numeric force on atom with index a, Cartesian component i,
    with finite step of size d#
    """
    
    p0 = atoms.get_positions()
    p = p0.copy()
    p[a, i] += d
    
    atoms.set_positions(p, apply_constraint=False)
    eplus=get_energy(atoms)
    
    p[a, i] -= 2 * d
    atoms.set_positions(p, apply_constraint=False)
    eminus=get_energy(atoms)
        
    atoms.set_positions(p0, apply_constraint=False)
    return (eminus - eplus) / (2 * d)



def numeric_extra_force(atoms, a, i, d=1e-6):
   
    extra_coords=atoms.extra_coords.copy()
    
    extra_coords[a, i] += d
    atoms.extra_coords=extra_coords  
    eplus=get_energy(atoms)
    
    extra_coords[a, i] -= 2 * d
    atoms.extra_coords=extra_coords 
    eminus=get_energy(atoms)

    return (eminus - eplus) / (2 * d)



def numeric_frac_gradients(atoms, a, i, d=1e-6):
    
    fractions=atoms.fractions.copy()   
    
    fractions[a, i] += d
    atoms.fractions=fractions        
    eplus=get_energy(atoms)
    
    fractions[a, i] -= 2 * d
    atoms.fractions=fractions        
    eminus=get_energy(atoms)
    
    return  (eplus-eminus) / (2 * d)



def numeric_stress(atoms, d=1e-6, voigt=False):
    
    stress = np.zeros((3, 3), dtype=float)

    cell = atoms.cell.copy()
    V = atoms.get_volume()
    for i in range(3):
        x = np.eye(3)
        x[i, i] += d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eplus=get_energy(atoms)

        x[i, i] -= 2 * d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eminus=get_energy(atoms)

        stress[i, i] = (eplus - eminus) / (2 * d * V)
        x[i, i] += d

        j = i - 2
        x[i, j] = d
        x[j, i] = d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eplus=get_energy(atoms)

        x[i, j] = -d
        x[j, i] = -d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eminus=get_energy(atoms)

        stress[i, j] = (eplus - eminus) / (4 * d * V)
        stress[j, i] = stress[i, j]
    atoms.set_cell(cell, scale_atoms=True)

    if voigt:
        return stress.flat[[0, 4, 8, 5, 2, 1]]
    else:
        return stress    


def new_atoms(rattle_seed=8686):     
    atoms = fcc111('Ag', size=(2, 1, 1))
    atoms.center(vacuum=4.0, axis=2)
    
    atoms.rattle(0.55, seed=rattle_seed)
    return atoms



@pytest.mark.parametrize('dimensions', [3,4,5,6])
@pytest.mark.parametrize('potential', [WeightedHighDimRepulsivePotential(potential_type='LJ', prefactor=1, rc_factor=0.9)])
def test_WeightedHighDimRepulsivePrior(dimensions, potential):
            
    atoms = Atoms(['Au']*2 +['Ni']*3, positions=[[0., 0., 0.]] * 5)
    
    n_elements=len(set(atoms.symbols))
    atoms.pbc = True
    atoms.center(vacuum=6.0)
    rng = np.random.RandomState(73721) 
    sgen = RandomBranch(atoms, llim=1.6, ulim=1.8, rng=rng)
    
    
    testatoms=sgen.get()
    
    rng = np.random.RandomState(8)
    
    
    if dimensions==3:
        extra_coords=None
    else:
        extra_coords = 2*(rng.rand(len(testatoms), dimensions-3 ) - 0.5)
        
    
    fractions = rng.rand(len(atoms), n_elements )
    frac_sum=np.sum(fractions,axis=1)
    for i in range(len(atoms)):        
        fractions[i,:]/=frac_sum[i]
        
    
    testatoms.extra_coords=extra_coords
    testatoms.dims=dimensions
    testatoms.fractions=fractions
    
    
    testatoms.calc=potential
    
    energy=get_energy(testatoms)

    
    all_analytical_forces=potential.results['forces']
    
    analytical_forces=all_analytical_forces[:,0:3]
    
    analytical_extra_forces=all_analytical_forces[:,3:dimensions]

    analytical_frac_gradients=potential.results['frac_grads']
    
    
    numerical_forces=np.zeros( np.shape(analytical_forces) )
    
    numerical_extra_forces=np.zeros( np.shape(analytical_extra_forces) )
    
    numerical_frac_gradients=np.zeros( np.shape(analytical_frac_gradients) )
    
                    
    for a in range(len(testatoms)):
            
        for i1 in range(3):
            numerical_forces[a,i1]=numeric_force(testatoms, a, i1)
                
        for i2 in range(dimensions-3):
            numerical_extra_forces[a,i2]=numeric_extra_force(testatoms, a, i2)

        for i3 in range(n_elements):
            numerical_frac_gradients[a,i3]=numeric_frac_gradients(testatoms, a, i3)
                

    assert np.allclose(numerical_forces/analytical_forces, 1, atol=1e-2, rtol=0 )

    assert np.allclose(numerical_extra_forces/analytical_extra_forces, 1, atol=1e-2, rtol=0 )

    assert np.allclose(numerical_frac_gradients/analytical_frac_gradients, 1, atol=1e-2, rtol=0 )   

    stress=potential.results['stress']
    
    analytical_stress = voigt_6_to_full_3x3_stress(stress)
    
    numerical_stress=numeric_stress(testatoms)

    assert np.allclose(numerical_stress/analytical_stress, 1, atol=1e-2, rtol=0 ) 
    
    
def test_GhostExtraPotential():

    symbol='Cu'
    
    rc=covalent_radii[atomic_numbers[symbol]]
    
    factor=np.linspace(1.75, 2.25, 50)
    x_array=rc*factor
    images=[]
    
    fractions=np.array([ 1,1e-7 ]).reshape(2,1)
    for x in x_array:
        atoms = Atoms([symbol]*2, positions=[(0, 0, 0), (x, 0, 0)])
        atoms.pbc=False
        atoms.cell=[10,10,10]
        atoms.dims=3
        atoms.fractions=fractions
        atoms.extra_coords=None
        images.append(atoms)
        
    
    gep=GhostExtraPotential(strength=0.01, onset=1e-5)   
    x_force_ghost=[]
    x_force_real=[]
    for image in images:
        forces=gep.forces(image)
        x_force_real.append(forces[0,0])
        x_force_ghost.append(forces[1,0])
        
    x_force_real=np.array(x_force_real)
    x_force_ghost=np.array(x_force_ghost)
    
    
    min_index=np.where(np.diff(np.sign(x_force_ghost)) < 0)[0] + 1  
    min_index=min_index[0] # get the index out of list

    expected_min=2*rc
    assert np.isclose (x_array[min_index], expected_min, atol=0.01, rtol=0)
    assert np.isclose (x_force_ghost[min_index], 0, atol=1e-3, rtol=0)
    assert np.all(x_force_ghost[0:min_index]>0)
    assert np.all(x_force_ghost[min_index:]<0)
    assert np.all(x_force_real==0)
