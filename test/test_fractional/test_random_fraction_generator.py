#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  8 13:21:46 2023

@author: casper
"""

import pytest
from gpatom.fractional_beacon.icebeacon import RandomFractionGenerator, ICEInfo
from ase import Atoms
import numpy as np



@pytest.mark.parametrize('randomtype', ['uniform', 'drs'])
def test_fraction_generator_ice_only(randomtype):
    
    # atoms are deliberately not added in alphabetacal order.
    # as iceinfo will sort them.
    atoms = Atoms(['Ag'] * 3 + ['Au']*2 +  ['Ni']*1 + ['Cu'] * 3 + ['Mn'] *1 , positions=[[0., 0., 0.]] * 10)
    atoms.pbc = False
    atoms.center(vacuum=6.0)
    
    ice_elements=[['Ag', 'Au', 'Ni'], ['Cu', 'Mn'] ]

    ice_info=ICEInfo(atoms, ice_elements=ice_elements)
  
    # asser that ice_info properties are as expected
    assert np.all(ice_info.elements == ['Ag', 'Au', 'Cu', 'Mn', 'Ni'])

    n_real = ice_info.n_real

    assert np.all(ice_info.n_real==[3,2,3,1,1])
            
    rfg=RandomFractionGenerator(ice_info,
                                randomtype=randomtype,
                                rng=np.random.RandomState(34388))
    
    fractions=rfg.get_fractions(atoms)
        
    # check that randomtypes uniform and drs are correct
    expected_fraction_sums=np.array(n_real)
    
    assert np.allclose(np.sum(fractions,axis=0) , expected_fraction_sums)
   
 
    existence_fractions=np.sum(fractions, axis=1)
 
    assert np.allclose(existence_fractions, np.ones(len(atoms)))    
 
    
    assert np.all(fractions>=0)
    
    assert np.all(fractions<=1)
            

    # check that randomtype uniform has the right values
    if randomtype=='uniform':  
        expected_uniform_fracs=np.array( [[3/6, 2/6, 0,   0, 1/6],
                                          [3/6, 2/6, 0,   0, 1/6],
                                          [3/6, 2/6, 0,   0, 1/6],
                                          [3/6, 2/6, 0,   0, 1/6],
                                          [3/6, 2/6, 0,   0, 1/6],
                                          [3/6, 2/6, 0,   0, 1/6],
                                          [0,    0, 3/4, 1/4, 0],
                                          [0,    0, 3/4, 1/4, 0],
                                          [0,    0, 3/4, 1/4, 0],
                                          [0,    0, 3/4, 1/4, 0]] )

        
        assert np.all(expected_uniform_fracs==fractions)
        
        
        
        


@pytest.mark.parametrize('randomtype', ['whole_atoms','uniform', 'drs'])
def test_fraction_generator(randomtype):
    
    # elements have been sorted allready for simplicty of writing test
    atoms = Atoms(['Ag']*3 + ['Al']*2 +  ['Au']*3 + ['Co']*2 + ['Cu']*2 + ['Fe']*2 + ['Mn']*2 + ['Ni']*1)
    atoms.pbc = False
    atoms.center(vacuum=6.0)
    
    atoms_real = Atoms(['Ag']*3 + ['Al']*2 +  ['Au']*2 + ['Co']*2 + ['Cu']*2 + ['Fe']*1 + ['Mn']*1 + ['Ni']*1)
        
    ice_elements=[ ['Ag', 'Al', 'Au'], ['Co', 'Cu'] ]
    
    lower_lim=0.2
    
    ice_info=ICEInfo(atoms, atoms_real=atoms_real, ice_elements=ice_elements, 
                     lower_lim=lower_lim, frac_cindex=[0,8])
    
    elements=ice_info.elements
    
    
    # assert that ice_info properties are as expected
    assert np.all(elements==['Ag', 'Al', 'Au', 'Co', 'Cu', 'Fe', 'Mn', 'Ni'])
    
    n_real=ice_info.n_real
    
    assert np.all(n_real==[3,2,2,2,2,1,1,1])
    
    n_ghost=ice_info.n_ghost
    
    assert np.all(n_ghost==[0,0,1,0,0,1,1,0])
    
    
    rfg=RandomFractionGenerator(ice_info,
                                randomtype=randomtype,
                                rng=np.random.RandomState(34388))
    
    fractions=rfg.get_fractions(atoms)
    
    expected_fraction_sums=np.array(n_real)+lower_lim*np.array(n_ghost)
    
    # test option whole_atoms is correct
    if randomtype=='whole_atoms':
        expected_whole_atom_fracs=np.array( [[1, 0, 0, 0, 0, 0, 0, 0],
                                             [1, 0, 0, 0, 0, 0, 0, 0],
                                             [1, 0, 0, 0, 0, 0, 0, 0],
                                             [0, 1, 0, 0, 0, 0, 0, 0],
                                             [0, 1, 0, 0, 0, 0, 0, 0],
                                             [0, 0, 1, 0, 0, 0, 0, 0],
                                             [0, 0, 1, 0, 0, 0, 0, 0],
                                             [0, 0, 1, 0, 0, 0, 0, 0],
                                             [0, 0, 0, 1, 0, 0, 0, 0],
                                             [0, 0, 0, 1, 0, 0, 0, 0],
                                             [0, 0, 0, 0, 1, 0, 0, 0],
                                             [0, 0, 0, 0, 1, 0, 0, 0],
                                             [0, 0, 0, 0, 0, 1, 0, 0],
                                             [0, 0, 0, 0, 0, 1, 0, 0],
                                             [0, 0, 0, 0, 0, 0, 1, 0],
                                             [0, 0, 0, 0, 0, 0, 1, 0],
                                             [0, 0, 0, 0, 0, 0, 0, 1]] ,dtype=float )
                                    
       
        
        assert np.all(expected_whole_atom_fracs==fractions)
        
        return
    
    # check that randomtypes uniform and drs are correct
    expected_fraction_sums=np.array(n_real)+lower_lim*np.array(n_ghost)
    
    assert np.allclose(np.sum(fractions,axis=0) , expected_fraction_sums)
    
    unity_indices=[0,8,9,10,11,16]
    
    ghost_indices=[1,2,3,4,5,6,7,9,10,11,12,13,14,15]
    
    existence_fractions=np.sum(fractions, axis=1)
    
    for idx in range(len(atoms)):
        if idx in unity_indices:
            assert np.isclose(existence_fractions[idx] , 1.0)
        else:
            assert existence_fractions[idx]<=1
            
        if idx in ghost_indices:
            assert existence_fractions[idx]>lower_lim
            
    
    expected_entry_matrix=np.array( [[1, 0, 0, 0, 0, 0, 0, 0],
                                     [1, 1, 1, 0, 0, 0, 0, 0],
                                     [1, 1, 1, 0, 0, 0, 0 ,0],
                                     [1, 1, 1, 0, 0, 0, 0 ,0],
                                     [1, 1, 1, 0, 0, 0, 0 ,0],
                                     [1, 1, 1, 0, 0, 0, 0 ,0],
                                     [1, 1, 1, 0, 0, 0, 0 ,0],
                                     [1, 1, 1, 0, 0, 0, 0 ,0],
                                     [0, 0, 0, 1, 0, 0, 0 ,0],
                                     [0, 0, 0, 1, 1, 0, 0, 0],
                                     [0, 0, 0, 1, 1, 0, 0 ,0],
                                     [0, 0, 0, 1, 1, 0, 0 ,0],
                                     [0, 0, 0, 0, 0, 1, 0 ,0],
                                     [0, 0, 0, 0, 0, 1, 0 ,0],
                                     [0, 0, 0, 0, 0, 0, 1 ,0],
                                     [0, 0, 0, 0, 0, 0, 1 ,0],
                                     [0, 0, 0, 0, 0, 0, 0 ,1]] )

    
    entry_fracs=fractions.copy()
    entry_fracs[fractions>0]=1
    
    assert np.all(expected_entry_matrix==entry_fracs.astype(int))

    # check that randomtype uniform has the right values
    if randomtype=='uniform':  
        
        expected_uniform_fracs=np.array( [[1, 0, 0, 0, 0, 0, 0, 0],
                                          [2/7, 2/7, 2.2/7, 0, 0, 0, 0, 0],
                                          [2/7, 2/7, 2.2/7, 0, 0, 0, 0, 0],
                                          [2/7, 2/7, 2.2/7, 0, 0, 0, 0, 0],
                                          [2/7, 2/7, 2.2/7, 0, 0, 0, 0, 0],
                                          [2/7, 2/7, 2.2/7, 0, 0, 0, 0, 0],
                                          [2/7, 2/7, 2.2/7, 0, 0, 0, 0, 0],
                                          [2/7, 2/7, 2.2/7, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 1/3, 2/3, 0, 0, 0],
                                          [0, 0, 0, 1/3, 2/3, 0, 0, 0],
                                          [0, 0, 0, 1/3, 2/3, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1.2/2, 0, 0],
                                          [0, 0, 0, 0, 0, 1.2/2, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 1.2/2, 0],
                                          [0, 0, 0, 0, 0, 0, 1.2/2, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 1]] )
        
        assert np.all(expected_uniform_fracs==fractions)
        
        
