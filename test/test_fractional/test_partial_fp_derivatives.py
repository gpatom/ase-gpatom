import pytest
from ase.build import bulk
from gpatom.fractional_beacon.gpfp.fingerprint import (PartialFP,
                                                       PartialRadAngFP)
import numpy as np

from ase import Atoms
from gpatom.beacon.str_gen import RandomBranch


def fractional_derivatives_ICE(atoms, fractions, fp_class, n_elements):
    
    fp1 = fp_class(atoms=atoms.copy(), fractions=fractions)
    
    v1 = fp1.vector
    
    drho_analytical = fp1.reduce_frac_gradients()
    
    dx = 0.00001
    
    for index in range(len(atoms)): 
        for elem in range(n_elements):
   
            fractions_copy = fractions.copy()
            
            fractions_copy[index,elem] += dx 
            
            v2 = fp_class(atoms=atoms.copy(), fractions=fractions_copy).vector

            drho_numerical = (v2 - v1) / dx
            
            
            assert np.allclose(drho_numerical, drho_analytical[index,:,elem], rtol=1e-2, atol=1e-2)
            



    
def coordinate_derivatives(atoms, fractions, fp_class):

    fp1 = fp_class(atoms=atoms.copy(), fractions=fractions)
 
    v1 = fp1.vector
    
    drho_analytical = fp1.reduce_coord_gradients()
     
    dx = 0.00001
    
    for index in range(len(atoms)):
        for coord in range(3):
    
            atoms_copy = atoms.copy()
        
            atoms_copy[index].position[coord] +=dx 
            
            v2 = fp_class(atoms=atoms_copy, fractions=fractions).vector

            drho_numerical = (v2 - v1) / dx

            assert np.allclose(drho_numerical, drho_analytical[index, :, coord] , 
                               rtol=1e-2, atol=1e-2)    



def strain_derivatives(atoms, fractions, fp_class):
    i1, i2 = 0, 0
    dx = 0.000001
    
    atoms1 = atoms.copy()
    v1 = fp_class(atoms=atoms1, fractions=fractions, calc_strain=True).vector

    atoms2 = atoms.copy()
    x = np.eye(3)
    x[i1, i2] += dx
    x[i2, i1] += dx
    atoms2.set_cell(np.dot(atoms2.cell, x), scale_atoms=True)
    v2 = fp_class(atoms=atoms2, fractions=fractions, calc_strain=True).vector
    
    drho_numerical = (v2 - v1) / (2 * dx)
    
    fp1 = fp_class(atoms=atoms, fractions=fractions, calc_strain=True)
    drho_analytical = fp1.reduce_strain()[:, i1, i2]
    
     
    assert np.allclose(drho_numerical, drho_analytical, rtol=1e-2, atol=1e-2)


@pytest.mark.parametrize('fp_class', [PartialFP, PartialRadAngFP])
@pytest.mark.parametrize('periodic', [False, True])
def test_fp_derivative_ICE(fp_class, periodic):
    
    '''
    tests that gradient of fingerprint is correct with respect to
    fractions and coordinates
    '''
  
    atoms = Atoms(['Au'] * 2 + ['Cu'] * 2  + ['Ni']*2 + ['Ag']*2 , positions=[[0., 0., 0.]] * 8)

    atoms.pbc = periodic
    atoms.center(vacuum=3.0)
    rng = np.random.RandomState(9)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)
    
    testatoms=sgen.get()

    n_elements=len(set(atoms.symbols))

    rng = np.random.RandomState(400)
    
    testfractions = rng.random(  (len(atoms), n_elements) )

    fractional_derivatives_ICE(testatoms, testfractions, fp_class, n_elements)

    coordinate_derivatives(testatoms, testfractions, fp_class)
    
    strain_derivatives(testatoms, testfractions, fp_class)

