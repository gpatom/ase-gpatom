import pytest
from gpatom.fractional_beacon.gpfp.fingerprint import PartialFP, PartialRadAngFP
from gpatom.fractional_beacon.gpfp.kerneltypes import ICESquaredExp
from ase import Atoms
import numpy as np
from gpatom.beacon.str_gen import RandomBranch, RandomCell



@pytest.mark.parametrize('fp_class', [PartialFP, PartialRadAngFP])
def test_dk_drm(fp_class):
    atoms = Atoms(['Cu'] * 4 + ['Au'] * 2, positions=[[0., 0., 0.]] * 6)
    natoms = len(atoms)
    atoms.center(vacuum=6.0)

    kerneltype = ICESquaredExp(scale=100, weight=100)

    fpparams = dict()

    rng = np.random.RandomState(33)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)

    atoms1 = sgen.get()
    atoms2 = sgen.get()
    
    n_elements=len(set(atoms.symbols))

    fractions1 = rng.random( (natoms, n_elements) )
    fractions2 = rng.random( (natoms, n_elements) )

    fp1 = fp_class(atoms=atoms1, fractions=fractions1, **fpparams)
    fp2 = fp_class(atoms=atoms2, fractions=fractions2, **fpparams)

    analytical_frac = kerneltype.kernel_gradient_frac(fp1, fp2)

    dx = 0.000001
    
    for atomindex in range(len(atoms)):
        for elem in range(n_elements):
            fractions3 = fractions1.copy()
            fractions3[atomindex,elem] += dx
            fp3 = fp_class(atoms=atoms1.copy(), fractions=fractions3, **fpparams)

            kernelA = kerneltype.kernel(fp1, fp2)
            kernelB = kerneltype.kernel(fp3, fp2)

            numerical_frac = ((kernelB - kernelA) / dx).flatten()
            
            print('atom:', atomindex, 'element:', elem)
            print()
            print('numerical', numerical_frac)
            print()
            print('analytical', analytical_frac[atomindex,elem])
            
            assert np.allclose(numerical_frac/analytical_frac[atomindex,elem], 
                               1, atol=1e-4, rtol=0)

    
    analytical_coord = kerneltype.kernel_gradient(fp1, fp2)

    dx = 0.000001

    for atomindex in range(len(atoms)):
        for coord in range(3):
            atoms3 = atoms1.copy()
            atoms3.positions[atomindex, coord] += dx
            fp3 = fp_class(atoms=atoms3, fractions=fractions1.copy(), **fpparams)

            kernelA = kerneltype.kernel(fp1, fp2)
            kernelB = kerneltype.kernel(fp3, fp2)

            numerical_coord = ((kernelB - kernelA) / dx).flatten()
            
            print('atom:', atomindex, 'coordinate:', coord)
            print()
            print('numerical', numerical_coord)
            print()
            print('analytical', analytical_coord[atomindex,coord])

            assert np.allclose(numerical_coord/analytical_coord[atomindex,coord], 
                               1, atol=1e-4, rtol=0)




@pytest.mark.parametrize('fp_class', [PartialFP, PartialRadAngFP])
def test_dk_drm_dq(fp_class):
    atoms = Atoms(['Cu'] * 4 + ['Au'] * 2, positions=[[0., 0., 0.]] * 6)
    natoms = len(atoms)
    atoms.center(vacuum=6.0)

    kerneltype = ICESquaredExp(scale=1000)

    fpparams = dict()

    rng = np.random.RandomState(33)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)

    atoms1 = sgen.get()
    atoms2 = sgen.get()

    n_elements=len(set(atoms.symbols))

    fractions1 = rng.random( (natoms, n_elements) )
    fractions2 = rng.random( (natoms, n_elements) )

    fp1 = fp_class(atoms=atoms1, fractions=fractions1, **fpparams)
    fp2 = fp_class(atoms=atoms2, fractions=fractions2, **fpparams)

    analytical = kerneltype.dkernelgradient_dq(fp1, fp2)

    dx = 0.000001
    
    for atomindex in range(len(atoms)):
        for elem in range(n_elements):
    
            fractions3 = fractions1.copy()
            fractions3[atomindex,elem] += dx
            fp3 = fp_class(atoms=atoms1.copy(), fractions=fractions3, **fpparams)

            gradientA = kerneltype.kernel_gradient(fp2, fp1)
            gradientB = kerneltype.kernel_gradient(fp2, fp3)

            numerical = ((gradientB - gradientA) / dx)

            print('atom:', atomindex, 'element:', elem)
            print()
            print('numerical', numerical)
            print()
            print('analytical', analytical[atomindex,:,:,elem])

            assert np.allclose(numerical.flatten()/analytical[atomindex,:,:,elem].flatten(), 
                               1, atol=1e-4, rtol=0)


@pytest.mark.parametrize('fp_class', [PartialFP, PartialRadAngFP])
def test_dk_dc_dq(fp_class):
    atoms = Atoms(['Cu'] * 4 + ['Au'] * 2, positions=[[0., 0., 0.]] * 6)
    natoms = len(atoms)
    atoms.center(vacuum=2.5)
    atoms.pbc=True

    kerneltype = ICESquaredExp(scale=1000)

    fpparams = dict()

    rng = np.random.RandomState(33)
    sgen = RandomCell(atoms, rng=rng)

    atoms1 = sgen.get()
    atoms2 = sgen.get()
    
    n_elements=len(set(atoms.symbols))

    fractions1 = rng.random( (natoms, n_elements) )
    fractions2 = rng.random( (natoms, n_elements) )

    fp1 = fp_class(atoms=atoms1, fractions=fractions1, calc_strain=True, **fpparams)
    fp2 = fp_class(atoms=atoms2, fractions=fractions2, calc_strain=True, **fpparams)

    analytical = kerneltype.ddq_dkdc(fp1, fp2)

    dx = 0.000001
    
    for atomindex in range(len(atoms)):
        for elem in range(n_elements):
    
            fractions3 = fractions1.copy()
            fractions3[atomindex,elem] += dx
            fp3 = fp_class(atoms=atoms1.copy(), fractions=fractions3, **fpparams)

            gradientA = kerneltype.dkernel_dc(fp2, fp1)
            gradientB = kerneltype.dkernel_dc(fp2, fp3)

            numerical = ((gradientB - gradientA) / dx)

            print('atom:', atomindex, 'element:', elem)
            print()
            print('numerical', numerical)
            print()
            print('analytical', analytical[atomindex,:,:,elem])
                
            print(numerical.flatten()/ analytical[atomindex,:,:,elem].flatten())

            assert np.allclose(numerical.flatten()/ analytical[atomindex,:,:,elem].flatten(), 
                               1, atol=1e-4, rtol=0)
