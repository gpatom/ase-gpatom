import pytest
from ase import Atoms
from gpatom.beacon.str_gen import RandomBranch

from gpatom.fractional_beacon.gpfp.atoms_gp_interface import PartialModel, PartialLCBModel
from gpatom.fractional_beacon.gpfp.gp import ICEGaussianProcess
from gpatom.fractional_beacon.gpfp.fingerprint import HighDimPartialFingerPrint

from gpatom.fractional_beacon.gpfp.prior import (WeightedHighDimRepulsivePotential,
                                                 WeightedHighDimCalculatorPrior)

from ase.calculators.emt import EMT
import numpy as np

from ase.stress import (full_3x3_to_voigt_6_stress,
                        voigt_6_to_full_3x3_stress)

def get_training_data(sgen, atoms):
    
    training_atoms=[]
    ninit = 2
    for i in range(ninit):
        atoms = sgen.get()
        atoms.calc = EMT()
        training_atoms.append(atoms)
        
    return training_atoms



def numeric_gradients(atoms, extra_coords, fractions, model, a, i, d=1e-3):
    """Compute numeric force on atom with index a, Cartesian component i,
    with finite step of size d
    """
    p0 = atoms.get_positions()
    p = p0.copy()
    p[a, i] += d
    atoms.set_positions(p, apply_constraint=False)
    eplus=model.calculate(atoms, extra_coords=extra_coords, fractions=fractions)[0]
    
    p[a, i] -= 2 * d
    atoms.set_positions(p, apply_constraint=False)
    eminus=model.calculate(atoms, extra_coords=extra_coords, fractions=fractions)[0]
        
    atoms.set_positions(p0, apply_constraint=False)

    return (eplus -eminus) / (2 * d)


def numeric_extra_gradients(atoms, extra_coords, fractions, model, a, i, d=1e-3):
    
    extra_coords_0=extra_coords.copy()

    extra_coords_0[a, i] += d
    eplus=model.calculate(atoms, extra_coords=extra_coords_0, fractions=fractions)[0]
    
    extra_coords_0[a, i] -= 2 * d
    eminus=model.calculate(atoms, extra_coords=extra_coords_0, fractions=fractions)[0]
    
    return (eplus -eminus) / (2 * d)


def numeric_frac_gradients(atoms, extra_coords, fractions, model, a, i, d=1e-3):
    
    fractions_0=fractions.copy()   
    
    fractions_0[a, i] += d
    eplus=model.calculate(atoms, extra_coords=extra_coords, fractions=fractions_0)[0]
    
    fractions_0[a, i] -= 2 * d
    eminus=model.calculate(atoms, extra_coords=extra_coords, fractions=fractions_0)[0]
    
    return  (eplus-eminus) / (2 * d)


def numeric_stress(atoms, extra_coords, fractions, model, d=1e-3, voigt=False):
    stress = np.zeros((3, 3), dtype=float)

    cell = atoms.cell.copy()
    V = atoms.get_volume()
    for i in range(3):
        x = np.eye(3)
        x[i, i] += d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eplus=model.calculate(atoms, extra_coords=extra_coords, fractions=fractions)[0]

        x[i, i] -= 2 * d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eminus=model.calculate(atoms, extra_coords=extra_coords, fractions=fractions)[0]

        stress[i, i] = (eplus - eminus) / (2 * d * V)
        x[i, i] += d

        j = i - 2
        x[i, j] = d
        x[j, i] = d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eplus=model.calculate(atoms, extra_coords=extra_coords, fractions=fractions)[0]

        x[i, j] = -d
        x[j, i] = -d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eminus=model.calculate(atoms, extra_coords=extra_coords, fractions=fractions)[0]

        stress[i, j] = (eplus - eminus) / (4 * d * V)
        stress[j, i] = stress[i, j]
    atoms.set_cell(cell, scale_atoms=True)

    if voigt:
        return stress.flat[[0, 4, 8, 5, 2, 1]]
    else:
        return stress


    

@pytest.mark.parametrize('use_grads', [[False, False], [True, False], [True, True]])
@pytest.mark.parametrize('model_class', [PartialModel, PartialLCBModel] )
def test_forces_and_stress(use_grads, model_class): 

    atoms = Atoms(['Au'] * 3 + ['Cu'] * 2, positions=[[0., 0., 0.]] * 5)

    dimensions=5

    n_elements=len(set(atoms.symbols))
    atoms.pbc = True
    atoms.center(vacuum=6.0)
    rng = np.random.RandomState(73721) 
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)

    fp=HighDimPartialFingerPrint(calc_strain=True)

    potential=WeightedHighDimRepulsivePotential(potential_type='parabola', prefactor=10)
    prior=WeightedHighDimCalculatorPrior(potential, constant=0)
    
    gp_args=dict(prior=prior,
                 hp={'scale': 1000}, 
                 use_forces=use_grads[0], 
                 use_stress=use_grads[1])
        
    gp = ICEGaussianProcess(dims=dimensions, **gp_args)  

    model=model_class(gp=gp, fp=fp)

    training_atoms=get_training_data(sgen, atoms)

    model.add_data_points(training_atoms)

    testatoms=sgen.get()    
    
    rng = np.random.RandomState(8)
    
    
    if dimensions==3:
        extra_coords=None
    else:
        extra_coords = 2*(rng.rand(len(testatoms), dimensions-3 ) - 0.5)
        
    
    fractions = rng.rand(len(atoms), n_elements )
    frac_sum=np.sum(fractions,axis=1)
    for i in range(len(atoms)):        
        fractions[i,:]/=frac_sum[i]


    (energy, 
     all_analytical_grads, 
     analytical_frac_grads, 
     analytical_stress) = model.calculate(testatoms, extra_coords=extra_coords, 
                                          fractions=fractions, with_stress=True)
           
                                          
    analytical_stress=voigt_6_to_full_3x3_stress(analytical_stress)
    
    analytical_grads=all_analytical_grads[:,0:3]
    
    analytical_extra_grads=all_analytical_grads[:,3:dimensions]
        
    
    numerical_grads=np.zeros( np.shape(analytical_grads) )
    
    numerical_extra_grads=np.zeros( np.shape(analytical_extra_grads) )
    
    numerical_frac_grads=np.zeros( np.shape(analytical_frac_grads) )

    for a in range(len(atoms)):
        
        for i1 in range(3):
            numerical_grads[a,i1]=numeric_gradients(testatoms, extra_coords, fractions, model, a, i1)
            
        for i2 in range(dimensions-3):
            numerical_extra_grads[a,i2]=numeric_extra_gradients(testatoms, extra_coords, fractions, model, a, i2)

        for i3 in range(n_elements):
            numerical_frac_grads[a,i3]=numeric_frac_gradients(testatoms, extra_coords, fractions, model, a, i3)
            
                
    numerical_stress=numeric_stress(testatoms, extra_coords, fractions, model)
   
    
    print('grads')
    print(numerical_grads/analytical_grads)
    print('extragrads')
    print(numerical_extra_grads/analytical_extra_grads)
    print('frac_grads')
    print(numerical_frac_grads/analytical_frac_grads)
    print('stress')
    print(numerical_stress/analytical_stress)
    
    assert np.allclose(numerical_grads/analytical_grads, 1, atol=1e-2, rtol=0 )
    
    assert np.allclose(numerical_extra_grads/analytical_extra_grads, 1, atol=1e-2, rtol=0 )
    
    assert np.allclose(numerical_frac_grads/analytical_frac_grads, 1, atol=1e-2, rtol=0 )
    
    assert np.allclose(numerical_stress/analytical_stress, 1, atol=1e-2, rtol=0 )