#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 19:29:19 2024

@author: casper
"""

import numpy as np
from ase import Atoms
from ase.constraints import FixAtoms

from gpatom.beacon.str_gen import RandomBox, AtomsRelaxer
from gpatom.hyperspatial_beacon.str_gen import HighDimRandomBox
from gpatom.fractional_beacon.str_gen import (RandomStructureAndFractionGenerator,
                                              HighDimRandomStructureAndFractionGenerator)
from gpatom.fractional_beacon.icebeacon import RandomFractionGenerator, ICEInfo
from gpatom.fractional_beacon.gpfp.prior import  (WeightedRepulsivePotential, 
                                                  WeightedHighDimRepulsivePotential)
from gpatom.hyperspatial_beacon.str_gen import HighDimAtomsRelaxer

def get_constrained_atoms():
    atoms=Atoms(['Cu']*5 + ['O']*5, positions=[[0.,0.,0.]]*10)
    atoms.cell=[10,10,10]
    atoms.pbc=False
    
    constrained_atoms=[0,1]
    atoms.positions[0]=[4, 5, 5]    
    atoms.positions[1]=[6, 5, 5] 
    atoms.set_constraint(FixAtoms(constrained_atoms))

    return atoms, constrained_atoms


def assert_functioning_fixed_constraints(newatoms, template_atoms, 
                                         constrained_atoms):

    for idx in range(len(newatoms)):
        if idx in constrained_atoms:
            assert np.allclose(newatoms.positions[idx] , template_atoms.positions[idx])

    assert not np.allclose(newatoms.positions , template_atoms.positions)



def assert_functioning_fixed_highdim_constraints(newatoms, template_atoms, 
                                                constrained_atoms, 
                                                world_center):

    for idx in range(len(newatoms)):
        if idx in constrained_atoms:
            assert np.allclose(newatoms.positions[idx] , template_atoms.positions[idx])
            assert np.allclose(newatoms.extra_coords[idx,:] , world_center)
        else:
            assert np.all( abs(newatoms.extra_coords[idx,:] - world_center) > 0 )

    assert not np.allclose(newatoms.positions , template_atoms.positions)



def test_generator(): 
    
    dimensions=4
    
    atoms, constrained_atoms = get_constrained_atoms()
    
    atoms_real=Atoms(['Cu']*4 + ['O']*4)
    
    ice_info=ICEInfo(atoms, atoms_real=atoms_real, ice_elements=['Cu', 'O'],
                     lower_lim=0, frac_cindex=[0,1])
    
    
    rng = np.random.RandomState(2473)
    
    box=[(2., 8.)]*dimensions  
    
    sgen=RandomBox(atoms.copy(), box=box, covrad_inside=True, rng=rng)
    
    fgen=RandomFractionGenerator(ice_info,
                                 randomtype='drs', 
                                 rng=rng)
    
    weighted_potential = WeightedRepulsivePotential(potential_type='parabola', 
                                                    prefactor=5,
                                                    rc_factor=0.95, 
                                                    min_radius=True,
                                                    extrapotentials=None)

    # define a relaxer for the real space atoms generator
    relaxer=AtomsRelaxer(calculator=weighted_potential, fmax=0.001)
    
    generator=RandomStructureAndFractionGenerator(sgen, fgen, 
                                                  relaxer=relaxer)
    
    newatoms=generator.get()
    
    assert hasattr(newatoms, 'fractions')
    
    assert_functioning_fixed_constraints(newatoms, atoms, 
                                         constrained_atoms)

 
def test_highdim_generator(): 
    
    dimensions=4
    
    atoms, constrained_atoms = get_constrained_atoms()
    
    atoms_real=Atoms(['Cu']*4 + ['O']*4)
    
    ice_info=ICEInfo(atoms, atoms_real=atoms_real, ice_elements=['Cu', 'O'],
                     lower_lim=0, frac_cindex=[0,1])
    
    
    rng = np.random.RandomState(2473)
    
    box=[(2., 8.)]*dimensions  
    
    sgen=HighDimRandomBox(atoms.copy(), box=box, covrad_inside=True, rng=rng)
    
    fgen=RandomFractionGenerator(ice_info,
                                 randomtype='drs', 
                                 rng=rng)
    
    weighted_potential = WeightedHighDimRepulsivePotential(potential_type='parabola', 
                                                           prefactor=5,
                                                           rc_factor=0.95, 
                                                           min_radius=True,
                                                           extrapotentials=None)

    # define a relaxer for the real space atoms generator
    relaxer=HighDimAtomsRelaxer(calculator=weighted_potential, fmax=0.001)
    
    generator=HighDimRandomStructureAndFractionGenerator(sgen, fgen, 
                                                         relaxer=relaxer)
    
    
    world_center=sgen.world_center[3::]
    
    newatoms=generator.get()
    
    assert hasattr(newatoms, 'extra_coords')
    assert hasattr(newatoms, 'fractions')
    assert np.shape(newatoms.extra_coords) == (len(newatoms),dimensions-3)  
    
    assert_functioning_fixed_highdim_constraints(newatoms, atoms, 
                                                 constrained_atoms, 
                                                 world_center)
    