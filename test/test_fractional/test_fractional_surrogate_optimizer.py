
import numpy as np

from gpatom.fractional_beacon.gpfp.fingerprint import HighDimPartialFingerPrint, PartialFingerPrint

from ase import Atoms

from ase.calculators.emt import EMT

from ase.data import covalent_radii, atomic_numbers

from gpatom.fractional_beacon.gpfp.gp import ICEGaussianProcess

from gpatom.fractional_beacon.hsicebeacon import HSICESurrogateOptimizer

from gpatom.fractional_beacon.icebeacon import (ICESurrogateOptimizer, 
                                                RandomFractionGenerator,
                                                ICEInfo)

from gpatom.fractional_beacon.gpfp.prior import (WeightedCalculatorPrior,
                                                 WeightedRepulsivePotential,
                                                 WeightedHighDimCalculatorPrior,
                                                 WeightedHighDimRepulsivePotential)
                                                   
from gpatom.hyperspatial_beacon.str_gen import HighDimAtomsRelaxer, HighDimRandomBox

from gpatom.fractional_beacon.icebeacon import AtomsConverter

from gpatom.hyperspatial_beacon.gpfp.prior import HighDimRepulsivePotential

from gpatom.gpfp.prior import RepulsivePotential

from gpatom.fractional_beacon.gpfp.atoms_gp_interface import PartialModel

from ase.constraints import FixAtoms

from ase.io import read

from gpatom.beacon.str_gen import RandomBox, AtomsRelaxer



def assert_position_constraints(fixed_atoms_indices, fixed_atoms_positions, positions):
    for i, atom_idx in enumerate(fixed_atoms_indices):
        assert np.allclose( positions[atom_idx,:],  fixed_atoms_positions[i] ) 

def assert_extra_coord_constraints(fixed_atoms_indices, world_center, extra_coords):
    
    for atom_idx in fixed_atoms_indices:    
        assert np.allclose( extra_coords[atom_idx,:], world_center )


def assert_fraction_constraints_preround(frac_cindex, n_real, n_ghost, lower_lim, fractions):

    unity_indices=[0,8,9,10,11,14]    
    
    ghost_indices=[1,2,3,4,5,6,7,12,13]

    expected_fraction_sums=np.array(n_real)+lower_lim*np.array(n_ghost)
    
    expected_entry_matrix=np.array( [[1, 0, 0, 0, 0, 0, 0],
                                     [1, 1, 1, 0, 0, 0, 0],
                                     [1, 1, 1, 0, 0, 0, 0],
                                     [1, 1, 1, 0, 0, 0, 0],
                                     [1, 1, 1, 0, 0, 0, 0],
                                     [1, 1, 1, 0, 0, 0, 0],
                                     [1, 1, 1, 0, 0, 0, 0],
                                     [1, 1, 1, 0, 0, 0, 0],
                                     [0, 0, 0, 1, 0, 0, 0],
                                     [0, 0, 0, 1, 1, 0, 0],
                                     [0, 0, 0, 1, 1, 0, 0],
                                     [0, 0, 0, 1, 1, 0, 0],
                                     [0, 0, 0, 0, 0, 1, 0],
                                     [0, 0, 0, 0, 0, 1, 0],
                                     [0, 0, 0, 0, 0, 0, 1]] )
    
    assert np.allclose(np.sum(fractions,axis=0) , expected_fraction_sums)

    existence_fractions=np.sum(fractions, axis=1)

    eps=1e-10  # a small number because the optimizer aint perfect in satisfying constraints
    
    # check all fractions between 0 and 1
    assert  np.all(fractions>0-eps)
    assert  np.all(fractions<1+eps)
    
    # check existence fractions are correct
    for idx in range(len(fractions[:,0])):
        if idx in unity_indices:
            assert np.isclose(existence_fractions[idx] , 1.0)
        else:
            assert existence_fractions[idx]<= 1 + eps
            
        if idx in ghost_indices:
            assert existence_fractions[idx]>lower_lim - eps

    # check all expected zeros are zeros 
    #(there may be some new zeros due to fractions going to zero)
    A=expected_entry_matrix[expected_entry_matrix == 0]
    B=fractions[expected_entry_matrix == 0]

    assert np.allclose (A,B, rtol=1e-10)




def assert_fraction_constraints_postround(expected_fractions, fractions):
    assert np.all(expected_fractions==fractions.astype(int))






def test_ice_surrogate_optimizer():
    
    # The atoms are ordered alphabetically from the start for simplicity of the test
    atoms_g = Atoms(['Ag'] * 3 + ['Al']*2 +  ['Au'] * 3 + ['Cu'] *2 + ['Ni']*2 + ['Pd']*2 +['Pt']*1 , positions=[[0., 0., 0.]] * 15)
    atoms_g.pbc = False
    atoms_g.cell=[20,20,20]
    
    atoms = Atoms(['Ag'] * 3 + ['Al']*2 +  ['Au'] * 2+ ['Cu'] *2 + ['Ni']*2 + ['Pd']*1 +['Pt']*1 , positions=[[0., 0., 0.]] * 13)
    atoms.pbc = False
    atoms.cell=[20,20,20]
    
    ice_elements=[ ['Ag', 'Al', 'Au'], ['Cu', 'Ni'] ]
    lower_lim=0.05
    frac_cindex=[0,8]
    ice_info=ICEInfo(atoms_g, atoms_real=atoms, ice_elements=ice_elements,
                     lower_lim=lower_lim, frac_cindex=frac_cindex)

    elements=ice_info.elements
    
    assert np.all(elements==['Ag', 'Al', 'Au', 'Cu', 'Ni', 'Pd', 'Pt'])
    
    n_real=ice_info.n_real
    
    assert np.all(n_real==[3,2,2,2,2,1,1])
    
    n_ghost=ice_info.n_ghost
    
    assert np.all(n_ghost==[0,0,1,0,0,1,0])

    fixed_atoms = [0, 14]
    fixed_atoms_positions=[ [9,9,9], [11,11,11]]
    for i, atom_idx in enumerate(fixed_atoms):
        atoms_g.positions[atom_idx,:]=fixed_atoms_positions[i] 
        
    fixed_atoms_constraints = FixAtoms(indices=fixed_atoms)
    atoms_g.set_constraint(fixed_atoms_constraints)


    rng = np.random.RandomState(88)

    r_atom = (covalent_radii[atomic_numbers['Au']] )  

    potential=RepulsivePotential(potential_type='parabola', 
                                 prefactor=10, rc=0.9)

    relaxer=AtomsRelaxer(potential)

    sgen = RandomBox(atoms, box=[(5,15)]*3, relaxer=relaxer, rng=rng)

    sgen_g = RandomBox(atoms_g, box=[(7, 13)]*3, relaxer=relaxer, rng=rng)


    fp_args = {'r_cutoff': r_atom*4.25, 'a_cutoff': r_atom*2.25, 'aweight': 1}
    fp=PartialFingerPrint(fp_args=fp_args, 
                          angular=True, calc_strain=False)

    relax_potential=WeightedRepulsivePotential(potential_type='parabola', 
                                               prefactor=10, rc=0.9)

    prior=WeightedCalculatorPrior(calculator=relax_potential, constant=0)

    gp_args=dict(prior=prior,    
                 hp={'scale': 1000, 'weight': 100, 
                     'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1}, 
                 use_forces=True)
    
    gp=ICEGaussianProcess(**gp_args)

    model=PartialModel(gp=gp, fp=fp)

    training_set=[]
    for i in range(2):
        atoms_i=sgen.get()
        atoms_i.calc=EMT()
        training_set.append(atoms_i)

    model.add_data_points(training_set)
    
    rfg=RandomFractionGenerator(ice_info, randomtype='drs', rng=rng)

    # running with 
    # 5 steps of extra_coords and lower existence limit
    # 5 steps of no extra_coords and no lower extence limit
    # 5 steps with fixed fractions
    # as the steps are so few it is assumed that they are all carried out
    surropt=ICESurrogateOptimizer(ice_info,
                                  fmax=0.001,
                                  random_fraction_generator=rfg,
                                  weak_existing_steps=5,
                                  after_steps=5, 
                                  post_rounding_steps=5,
                                  derivative_modulation=0.01,
                                  fraction_rescale=2,
                                  coord_rescale=2,
                                  write_surropt_trajs=True,
                                  with_unit_cell=False)

    test_atoms=sgen_g.get()

    surropt.relax(test_atoms, model,  file_identifier='fractional')


    # CARRYING OUT THE TESTS
    trajectory=read('opt_fractional.xyz', ':')


    fixed_atoms_indices = [0, -1]
    
    # steps without extra_coords and with lower_lim
    for i in range(0,6):
        positions_i=trajectory[i].positions
        fractions_i=trajectory[i].info['fractions']
        
        assert_position_constraints(fixed_atoms_indices, fixed_atoms_positions, positions_i)
        assert_fraction_constraints_preround(fixed_atoms_indices, n_real, n_ghost, lower_lim, fractions_i)

    
    # steps without extra_coords and lower_lim
    for i in range(6,11):
        positions_i=trajectory[i].positions
        fractions_i=trajectory[i].info['fractions']
        
        assert_position_constraints(fixed_atoms_indices, fixed_atoms_positions, positions_i)
        assert_fraction_constraints_preround(fixed_atoms_indices, n_real, n_ghost, 0, fractions_i)
    
    # check that the rounded fractions has an expected form
    rounded_fractions=AtomsConverter.atoms2fractions(trajectory[16], elements)
    assert np.allclose(np.sum(rounded_fractions,axis=0) , n_real)
    assert np.allclose(np.count_nonzero(rounded_fractions, axis=1) , np.ones( (len(atoms),1) ))
    assert np.isclose(rounded_fractions[0,0], 1 )
    assert np.isclose(rounded_fractions[7,3], 1 )
    assert np.isclose(rounded_fractions[12,6], 1 )
    
    # steps without extra_coords where all fractions are rounded to 1 or 0
    for i in range(11,16):
        positions_i=trajectory[i].positions
        fractions_i=trajectory[i].info['fractions']
         
        assert_position_constraints(fixed_atoms_indices, fixed_atoms_positions, positions_i)
        assert_fraction_constraints_postround(rounded_fractions, fractions_i)
    





def test_HSICE_surrogate_optimizer():
    
    #RUNNING THE SIMULATION
    '''
    atoms_g = Atoms(['Al'] * 3 + ['Cu']*2 +  ['Ag'] * 3 + ['Au'] *2 + ['Ni']*2 + ['Pd']*2 +['Pt']*1 , positions=[[0., 0., 0.]] * 15)
    atoms_g.pbc = False
    atoms_g.cell=[20,20,20]


    atoms = Atoms(['Al'] * 3 + ['Cu']*2 +  ['Ag'] * 2+ ['Au'] *2 + ['Ni']*2 + ['Pd']*1 +['Pt']*1 , positions=[[0., 0., 0.]] * 13)
    atoms.pbc = False
    atoms.cell=[20,20,20]

    elements=['Al', 'Cu', 'Ag', 'Au', 'Ni', 'Pd', 'Pt']      
    ice_elements=[[True,True, True, False,False,False,False],          
                  [False, False, False,True,True,False,False]] 
    '''
    
    atoms_g = Atoms(['Ag'] * 3 + ['Al']*2 +  ['Au'] * 3 + ['Cu'] *2 + ['Ni']*2 + ['Pd']*2 +['Pt']*1 , positions=[[0., 0., 0.]] * 15)
    atoms_g.pbc = False
    atoms_g.cell=[20,20,20]
    
    atoms = Atoms(['Ag'] * 3 + ['Al']*2 +  ['Au'] * 2+ ['Cu'] *2 + ['Ni']*2 + ['Pd']*1 +['Pt']*1 , positions=[[0., 0., 0.]] * 13)
    atoms.pbc = False
    atoms.cell=[20,20,20]
    
    ice_elements=[ ['Ag', 'Al', 'Au'], ['Cu', 'Ni'] ]
    lower_lim=0.05
    frac_cindex=[0,8]
    ice_info=ICEInfo(atoms_g, atoms_real=atoms, ice_elements=ice_elements,
                     lower_lim=lower_lim, frac_cindex=frac_cindex)

    elements=ice_info.elements
    
    assert np.all(elements==['Ag', 'Al', 'Au', 'Cu', 'Ni', 'Pd', 'Pt'])
    
    n_real=ice_info.n_real
    
    assert np.all(n_real==[3,2,2,2,2,1,1])
    
    n_ghost=ice_info.n_ghost
    
    assert np.all(n_ghost==[0,0,1,0,0,1,0])
    
    
    fixed_atoms = [0, 14]
    fixed_atoms_positions=[ [9,9,9], [11,11,11]]
    for i, atom_idx in enumerate(fixed_atoms):
        atoms_g.positions[atom_idx,:]=fixed_atoms_positions[i] 
        
        
        
    fixed_atoms_constraints = FixAtoms(indices=fixed_atoms)
    atoms_g.set_constraint(fixed_atoms_constraints)

    rng = np.random.RandomState(88)

    r_atom = (covalent_radii[atomic_numbers['Au']] )  

    lowD=3
    highD=4

    potential=HighDimRepulsivePotential(potential_type='parabola', 
                                        prefactor=10, rc=0.9)

    relaxer_lowD=HighDimAtomsRelaxer(potential)

    relaxer_highD=HighDimAtomsRelaxer(potential)


    sgen_lowD = HighDimRandomBox(atoms, box=[(5,15)]*lowD, 
                                 relaxer=relaxer_lowD, rng=rng)

    sgen_highD = HighDimRandomBox(atoms_g, box=[(7, 13)]*highD,
                                  relaxer=relaxer_highD, rng=rng)


    fp_args = {'r_cutoff': r_atom*4.25, 'a_cutoff': r_atom*2.25, 'aweight': 1}
    fp=HighDimPartialFingerPrint(fp_args=fp_args, 
                                 angular=True, calc_strain=False)

    relax_potential=WeightedHighDimRepulsivePotential(potential_type='parabola', 
                                                      prefactor=10, rc=0.9)

    prior=WeightedHighDimCalculatorPrior(calculator=relax_potential, constant=0)

    gp_args=dict(prior=prior,    
                 hp={'scale': 1000, 'weight': 100, 
                     'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1}, 
                 use_forces=True)
    
    gp=ICEGaussianProcess(**gp_args)

    model=PartialModel(gp=gp, fp=fp)

    training_set=[]
    for i in range(2):
        atoms_i=sgen_lowD.get()
        atoms_i.calc=EMT()
        training_set.append(atoms_i)

    model.add_data_points(training_set)

    rfg=RandomFractionGenerator(ice_info, randomtype='drs', rng=rng)

    # running with 
    # 5 steps of extra_coords and lower existence limit
    # 5 steps of no extra_coords and no lower extence limit
    # 5 steps with fixed fractions
    # as the steps are so few it is assumed that they are all carried out
    surropt=HSICESurrogateOptimizer(ice_info, 
                                    fmax=0.001,
                                    random_fraction_generator=rfg,
                                    rattle_rng=rng,
                                    to_dims=lowD,
                                    strength_array=np.array([0.5]),
                                    cycles=1,
                                    relax_steps=5, 
                                    weak_existing_steps=5,
                                    after_steps=5, 
                                    post_rounding_steps=5,
                                    derivative_modulation=0.01,
                                    fraction_rescale=2,
                                    coord_rescale=2,
                                    write_surropt_trajs=True,
                                    with_unit_cell=False)

    test_atoms=sgen_highD.get()

    surropt.relax(test_atoms, model,  file_identifier='hsice')



    # CARRYING OUT THE TESTS
    trajectory=read('opt_hsice.xyz', ':')

    world_center=sgen_highD.world_center[lowD:highD]
    
    fixed_atoms_indices = [0, -1]
    
    # steps with extra_coords and lower_lim
    for i in range(0,6):
        positions_i=trajectory[i].positions
        extra_coords_i=trajectory[i].info['extra_coords']
        fractions_i=trajectory[i].info['fractions']
        assert_position_constraints(fixed_atoms_indices, fixed_atoms_positions, positions_i)
        assert_extra_coord_constraints(fixed_atoms_indices, world_center, extra_coords_i)
        assert_fraction_constraints_preround(fixed_atoms_indices, n_real, n_ghost, lower_lim, fractions_i)

    # steps without extra_coords and with lower_lim
    for i in range(6,11):
        positions_i=trajectory[i].positions
        extra_coords_i=trajectory[i].info['extra_coords']
        fractions_i=trajectory[i].info['fractions']
        
        assert extra_coords_i == 'None' 
        assert_position_constraints(fixed_atoms_indices, fixed_atoms_positions, positions_i)
        assert_fraction_constraints_preround(fixed_atoms_indices, n_real, n_ghost, lower_lim, fractions_i)

    
    # steps without extra_coords and lower_lim
    for i in range(11,16):
        positions_i=trajectory[i].positions
        extra_coords_i=trajectory[i].info['extra_coords']
        fractions_i=trajectory[i].info['fractions']
        
        assert extra_coords_i == 'None' 
        assert_position_constraints(fixed_atoms_indices, fixed_atoms_positions, positions_i)
        assert_fraction_constraints_preround(fixed_atoms_indices, n_real, n_ghost, 0, fractions_i)
    
    # check that the rounded fractions has an expected form
    rounded_fractions=AtomsConverter.atoms2fractions(trajectory[16], elements)
    assert np.allclose(np.sum(rounded_fractions,axis=0) , n_real)
    assert np.allclose(np.count_nonzero(rounded_fractions, axis=1) , np.ones( (len(atoms),1) ))
    assert np.isclose(rounded_fractions[0,0], 1 )
    assert np.isclose(rounded_fractions[7,3], 1 )
    assert np.isclose(rounded_fractions[12,6], 1 )
    
    # steps without extra_coords where all fractions are rounded to 1 or 0
    for i in range(16,22):
        positions_i=trajectory[i].positions
        extra_coords_i=trajectory[i].info['extra_coords']
        fractions_i=trajectory[i].info['fractions']
        
        assert extra_coords_i == 'None' 
        assert_position_constraints(fixed_atoms_indices, fixed_atoms_positions, positions_i)
        assert_fraction_constraints_postround(rounded_fractions, fractions_i)

