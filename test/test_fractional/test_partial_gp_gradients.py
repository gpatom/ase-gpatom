import pytest
from ase import Atoms
from gpatom.beacon.str_gen import RandomBranch
from gpatom.fractional_beacon.gpfp.gp import ICEGaussianProcess
from gpatom.fractional_beacon.gpfp.fingerprint import PartialFingerPrint
from gpatom.fractional_beacon.gpfp.prior import (WeightedConstantPrior, 
                                                 WeightedRepulsivePotential, 
                                                 WeightedCalculatorPrior)
from ase.calculators.emt import EMT
import numpy as np


def get_trained_gp(sgen, fp, gp, use_forces):

    trainingfeatures = []
    trainingtargets = []
    ninit = 2
    for i in range(ninit):
        atoms = sgen.get()
        atoms.calc = EMT()
        atoms.get_potential_energy()
        trainingfeatures.append(fp.get(atoms))
        
        if use_forces:
            trainingtargets.append([atoms.get_potential_energy()] +
                                   list(atoms.get_forces().flatten()))
        else: 
            trainingtargets.append([atoms.get_potential_energy()]) 
        
    trainingtargets = np.array(trainingtargets).flatten()
    
    gp.train(trainingfeatures, trainingtargets)

    return gp


def assert_correct_forces(gp, fp, testatoms, testfractions):
    
    testfp = fp.get(atoms=testatoms.copy(), fractions=testfractions)
    ef0, V = gp.predict(testfp)
    
    all_grads=ef0[1::].reshape(len(testatoms),-1)
    
    coord_grads=all_grads[:,0:3]

    dx = 0.00001
    
    for index in range(len(testatoms)):
        for coord in range(3):

            testatoms_copy=testatoms.copy()
            testatoms_copy.positions[index, coord] += dx
            testfp = fp.get(atoms=testatoms_copy, fractions=testfractions)
            ef1, V = gp.predict(testfp)
            
            numerical_force = (ef1[0] - ef0[0]) / dx
            analytical_force = coord_grads[index,coord]
            print('coord grads:', 'entry:', index, coord, 'num:', numerical_force, 'ana:', analytical_force)
            assert np.isclose(numerical_force/analytical_force, 1, atol=1e-2, rtol=0 )



def assert_correct_fraction_gradients(gp, fp, testatoms, testfractions, n_elements):

    testfp = fp.get(atoms=testatoms.copy(), fractions=testfractions)
    ef0, V = gp.predict(testfp)
    
    all_grads=ef0[1::].reshape(len(testatoms),-1)
    
    frac_grads=all_grads[:,3::]
  
    dx = 0.00001    
  
    for index in range(len(testatoms)):
        for elem in range(n_elements):
            
            testfractions_copy=testfractions.copy()
            testfractions_copy[index,elem] += dx
            testfp = fp.get(atoms=testatoms.copy(), fractions=testfractions_copy)
            ef1, V = gp.predict(testfp)

            numerical_gradient = (ef1[0] - ef0[0]) / dx
            analytical_gradient=frac_grads[index,elem]
            print('frac grads:', 'entry:', index, elem, 'num:', numerical_gradient, 'ana:', analytical_gradient)
            assert np.isclose(numerical_gradient/analytical_gradient, 1, atol=1e-2, rtol=0 )


def get_numeric_stress(atoms, fractions, gp, fp, d=1e-6, voigt=False):
    stress = np.zeros((3, 3), dtype=float)

    cell = atoms.cell.copy()
    V = atoms.get_volume()
    for i in range(3):
        x = np.eye(3)
        x[i, i] += d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, fractions=fractions)
        predict, unc = gp.predict(atoms_fp)
        eplus=predict[0]

        x[i, i] -= 2 * d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, fractions=fractions)
        predict, unc = gp.predict(atoms_fp)
        eminus=predict[0]

        stress[i, i] = (eplus - eminus) / (2 * d * V)
        x[i, i] += d

        j = i - 2
        x[i, j] = d
        x[j, i] = d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, fractions=fractions)
        predict, unc = gp.predict(atoms_fp)
        eplus=predict[0]


        x[i, j] = -d
        x[j, i] = -d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, fractions=fractions)
        predict, unc = gp.predict(atoms_fp)
        eminus=predict[0]

        stress[i, j] = (eplus - eminus) / (4 * d * V)
        stress[j, i] = stress[i, j]
    atoms.set_cell(cell, scale_atoms=True)

    if voigt:
        return stress.flat[[0, 4, 8, 5, 2, 1]]
    else:
        return stress    


def get_stresses(gp, fp, testatoms, testfractions):
    dx=0.0001
    
    testfp = fp.get(atoms=testatoms, fractions=testfractions)
    analytical_stress = gp.predict_stress(testfp)
    
    numerical_stress=get_numeric_stress(testatoms, testfractions, gp, fp, d=dx, voigt=True)
    
    return numerical_stress, analytical_stress



@pytest.mark.parametrize('prior', [WeightedConstantPrior(constant=0),
                                   WeightedCalculatorPrior(WeightedRepulsivePotential(potential_type='parabola', prefactor=10), constant=0)])
@pytest.mark.parametrize('use_forces', [True, False] )  
def test_predict_forces_and_gradients_ICE(use_forces, prior):
    '''
    Test that the predicted forces and fraction gradients are
    correct by comparing to finite-difference energies.
    '''
    atoms = Atoms(['Au'] * 2 + ['Cu'] * 2  + ['Ni']*2 + ['Ag'] * 2 , positions=[[0., 0., 0.]] * 8)

    atoms.pbc = True
    atoms.center(vacuum=6.0)
    rng = np.random.RandomState(9)
    sgen = RandomBranch(atoms, llim=2.5, ulim=3, rng=rng)
    
    n_elements=len(set(atoms.symbols))

    fp=PartialFingerPrint(calc_strain=True)

    gp_args=dict(prior=prior,
                 hp={'scale': 1000, 'weight': 100, 
                     'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1}, 
                 use_forces=use_forces)
        
    gp = ICEGaussianProcess(**gp_args)  

    gp=get_trained_gp(sgen, fp, gp, use_forces)

    testatoms = sgen.get()
    
    testfractions = rng.random(  (len(testatoms), n_elements) )
    
    
    assert_correct_forces(gp, fp, testatoms, testfractions)

    assert_correct_fraction_gradients(gp, fp, testatoms, testfractions, 
                                      n_elements)
        
    numerical_stress, analytical_stress = get_stresses(gp, fp, testatoms, testfractions)
    assert np.allclose(numerical_stress/analytical_stress, 1, atol=1e-2, rtol=0 ) 
        
    return