#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 17 23:52:30 2023

@author: casper
"""

import pytest
from gpatom.fractional_beacon.gpfp.fingerprint import (HighDimPartialFP,
                                                       HighDimPartialRadAngFP)
import numpy as np

from ase import Atoms
from gpatom.beacon.str_gen import RandomBranch

def coordinate_derivatives(atoms, extra_coords, fractions, fp_class):

    fp1 = fp_class(atoms=atoms.copy(), extra_coords=extra_coords, fractions=fractions)
 
    v1 = fp1.vector
    
    drho_analytical = fp1.reduce_coord_gradients()
     
    dx = 0.00001
    
    for index in range(len(atoms)):
        for coord in range(3):
    
            atoms_copy = atoms.copy()
        
            atoms_copy[index].position[coord] +=dx 
            
            v2 = fp_class(atoms=atoms_copy, extra_coords=extra_coords, fractions=fractions).vector

            drho_numerical = (v2 - v1) / dx

            assert np.allclose(drho_numerical, drho_analytical[index, :, coord] , 
                               rtol=1e-2, atol=1e-2)    



def extra_coordinate_derivatives(atoms, extra_coords, fractions, fp_class):

    fp1 = fp_class(atoms=atoms.copy(), extra_coords=extra_coords, fractions=fractions)
 
    v1 = fp1.vector
    
    drho_analytical = fp1.reduce_coord_gradients()
     
    dx = 0.00001
    
    extra_dimensions=len(extra_coords[0,:])
    
    for index in range(len(atoms)):
        for extra_coord_idx in range(extra_dimensions):
    
            extra_coords_copy=extra_coords.copy()
        
            extra_coords_copy[index,extra_coord_idx] +=dx 
            
            v2 = fp_class(atoms=atoms.copy(), extra_coords=extra_coords_copy, fractions=fractions).vector

            drho_numerical = (v2 - v1) / dx
           
            assert np.allclose(drho_numerical, drho_analytical[index, :, 3 + extra_coord_idx] , 
                               rtol=1e-2, atol=1e-2)   





def fractional_derivatives(atoms, extra_coords, fractions, fp_class, n_elements):
    
    fp1 = fp_class(atoms=atoms.copy(), extra_coords=extra_coords, fractions=fractions)
    
    v1 = fp1.vector
    
    drho_analytical = fp1.reduce_frac_gradients()
    
    dx = 0.00001
    
    for index in range(len(atoms)): 
        for elem in range(n_elements):
   
            fractions_copy = fractions.copy()
            
            fractions_copy[index,elem] += dx 
            
            v2 = fp_class(atoms=atoms.copy(), extra_coords=extra_coords, fractions=fractions_copy).vector

            drho_numerical = (v2 - v1) / dx
                        
            assert np.allclose(drho_numerical, drho_analytical[index,:,elem], rtol=1e-2, atol=1e-2)




def strain_derivatives(atoms, extra_coords, fractions, fp_class):
    i1, i2 = 0, 0
    dx = 0.000001
    
    atoms1 = atoms.copy()
    v1 = fp_class(atoms=atoms1, extra_coords=extra_coords, fractions=fractions, 
                  calc_strain=True).vector

    atoms2 = atoms.copy()
    x = np.eye(3)
    x[i1, i2] += dx
    x[i2, i1] += dx
    atoms2.set_cell(np.dot(atoms2.cell, x), scale_atoms=True)
    v2 = fp_class(atoms=atoms2, extra_coords=extra_coords, fractions=fractions, 
                  calc_strain=True).vector
    
    drho_numerical = (v2 - v1) / (2 * dx)
    
    fp1 = fp_class(atoms=atoms, extra_coords=extra_coords, fractions=fractions, 
                   calc_strain=True)
    
    drho_analytical = fp1.reduce_strain()[:, i1, i2]
        
    assert np.allclose(drho_numerical, drho_analytical, rtol=1e-2, atol=1e-2)
    
    
    
    
@pytest.mark.parametrize('fp_class', [HighDimPartialFP, 
                                      HighDimPartialRadAngFP])
@pytest.mark.parametrize('periodic', [False, True])
def test_fp_derivative_ICE_new(fp_class, periodic):
    
    '''
    tests that gradient of fingerprint is correct with respect to
    fractions and coordinates
    '''
  
    atoms = Atoms(['Au'] * 2 + ['Cu'] * 2  + ['Ni']*2 , positions=[[0., 0., 0.]] * 6)

    atoms.pbc = periodic
    atoms.center(vacuum=3.0)
    rng = np.random.RandomState(9)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)
    
    testatoms=sgen.get()

    n_elements=len(set(atoms.symbols))

    rng = np.random.RandomState(400)
    
    test_extra_coords =   rng.random( (len(atoms), 2) ) - 0.5
    
    testfractions = rng.random(  (len(atoms), n_elements) )
    
    coordinate_derivatives(testatoms, test_extra_coords, testfractions, fp_class)
    
    extra_coordinate_derivatives(testatoms, test_extra_coords, testfractions, fp_class)

    fractional_derivatives(testatoms, test_extra_coords, testfractions, fp_class, n_elements)

    strain_derivatives(testatoms, test_extra_coords, testfractions, fp_class)
