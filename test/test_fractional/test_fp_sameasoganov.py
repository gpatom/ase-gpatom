from ase.build import bulk
from gpatom.fractional_beacon.gpfp.fingerprint import PartialRadAngFP, HighDimPartialRadAngFP
from gpatom.gpfp.fingerprint import RadialAngularFP
import numpy as np
import pytest

@pytest.mark.parametrize('periodic', [False, True])
def test_partial_fp(periodic):

    atoms = bulk('NaCl', 'rocksalt', a=4.6)
    atoms = atoms.repeat((2, 2, 2))
    
    atoms.pbc = periodic
    
    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1}
    
    fp1 = PartialRadAngFP(atoms=atoms, fractions=None, **fp_args)
 
    v1 = fp1.vector

    fp2 = RadialAngularFP(atoms=atoms, **fp_args)
    v2 = fp2.vector
           
    assert np.allclose(v1, v2, rtol=1e-5)
    


@pytest.mark.parametrize('periodic', [False, True])
def test_highdimpartial_fp(periodic):

    atoms = bulk('NaCl', 'rocksalt', a=4.6)
    atoms = atoms.repeat((2, 2, 2))
    
    atoms.pbc = periodic

    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1}  

    
    fp1 = HighDimPartialRadAngFP(atoms=atoms, extra_coords=None, 
                                 fractions=None, **fp_args)
 
    v1 = fp1.vector

    fp2 = RadialAngularFP(atoms=atoms, **fp_args)
    v2 = fp2.vector
    
    assert np.allclose(v1, v2, rtol=1e-5)
    

