#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 00:23:00 2024

@author: casper
"""

import numpy as np
from ase import Atoms
from gpatom.beacon.str_gen import RandomCell
from gpatom.gpfp.kernel import FPStressKernel, FPKernel, FPStressKernelNoforces
from gpatom.gpfp.fingerprint import FingerPrint

import pytest    

@pytest.mark.parametrize('kernel', [FPKernel, FPStressKernelNoforces, FPStressKernel])
def test_kernel_positive_definitenes(kernel):
    
    # check that kernelmatrix is positive definite. 
    # otherwise we cant cholesky factorize
    # note that no targets are used here
    
    kernel=kernel('sqexp')
    kernel.kerneltype.params = {'scale': 1000, 'weight': 100}


    atoms = Atoms(['Au']*4, positions=[[0., 0., 0.]]*4)
    atoms.center(vacuum=1.5)
    atoms.pbc=False

    sgen = RandomCell(atoms, rng=np.random.RandomState(102))

    train_images = [sgen.get(), sgen.get()]

        
    fp=FingerPrint(angular=True, calc_strain=True)
    
    fp_data = [fp.get(atoms) for atoms in train_images]
          
    # regularize to make sure we dont have numerical issues
    km=kernel.kernel_matrix(fp_data)
    km += np.eye(km.shape[0]) * 0.001**2
    
    #assert matrix is symmetric
    assert np.allclose(km, km.T)
    
    #assert all eigenvalues are positive
    eigvals=np.linalg.eigvals(km)
    assert np.all(eigvals>0)