from gpatom.gpfp.prior import (RepulsivePotential, AreaPunisher, 
                               VolumePunisher, TallStructurePunisher,
                               CalculatorExtraPotential)

from gpatom.gpfp.calculator import PriorCalculator

from ase.calculators.emt import EMT

from ase import Atoms
import numpy as np
import pytest

def create_atoms():
    atoms = Atoms(['Au']*4, positions=[[0., 0., 0.]]*4)
    atoms.pbc = True

    atoms.positions[1] = [1.3, 1.6, 2.0]
    atoms.positions[2] = [2.2, 1.0, 0.5]
    atoms.positions[3] = [2.5, 0.1, 2.5]

    rng=np.random.RandomState(3423)

    atoms.cell=[[0.0, 3.04, 3.04], [3.04, 0.0, 3.04], [3.04, 3.04, 0.0]] + rng.rand(3,3)

    atoms.center()
    
    return atoms


def create_xy_atoms():
    atoms = Atoms(['Au']*4, positions=[[0., 0., 0.]]*4)
    atoms.pbc = [True, True, False]

    atoms.positions[1] = [1.3, 1.6, 2.0]
    atoms.positions[2] = [2.2, 1.0, 0.5]
    atoms.positions[3] = [2.5, 0.1, 2.5]

    atoms.cell=[[1.0, 2.04, 0], [3.54, 0.1, 0], [0, 0, 10.0]]

    atoms.center()
    
    return atoms



def get_num_and_ana_graidents(atoms, extrapotentials):

    potential=RepulsivePotential(potential_type='parabola', 
                                 prefactor=10, 
                                 extrapotentials=extrapotentials)
        
    calc=PriorCalculator(potential, calculate_stress=True)
   
    atoms.calc=calc 
   
    atoms.get_potential_energy() 
    
    analytical_forces=calc.results['forces']
    
    numerical_forces = atoms.calc.calculate_numerical_forces(atoms)    

    analytical_stress=calc.results['stress']
    
    numerical_stress = atoms.calc.calculate_numerical_stress(atoms) 
    
    print("ana-num dif forces:", analytical_forces - numerical_forces)  

    print("ana-num dif stress:", analytical_stress - numerical_stress) 

    return analytical_forces, numerical_forces, analytical_stress, numerical_stress


def test_VolumePunisher():
    
    atoms=create_atoms()
    
    volume=atoms.get_volume()
    
    extrapot_small_V=VolumePunisher(V_small=1.5*volume, 
                                    V_large=2*volume,
                                    strength=10)
    
    (analytical_forces, 
     numerical_forces, 
     analytical_stress, 
     numerical_stress) = get_num_and_ana_graidents(atoms, [extrapot_small_V])
    
    assert np.allclose(analytical_forces, numerical_forces, atol=1e-4, rtol=0)
    
    assert np.allclose(analytical_stress, numerical_stress, atol=1e-4, rtol=0)

    
    extrapot_large_V=VolumePunisher(V_small=0.25*volume, 
                                    V_large=0.5*volume,
                                    strength=10)
    
    (analytical_forces, 
     numerical_forces, 
     analytical_stress, 
     numerical_stress) = get_num_and_ana_graidents(atoms, [extrapot_large_V])
    
    assert np.allclose(analytical_forces, numerical_forces, atol=1e-4, rtol=0)
    
    assert np.allclose(analytical_stress, numerical_stress, atol=1e-4, rtol=0)


def test_AreaPunisher():
    
    atoms=create_xy_atoms()
    
    variable_stress_components=[True, True, False, False, False, True]
    
    cell=atoms.get_cell().uncomplete(pbc=[True, True, False])
    area = cell.area(2)
    
    extrapot_small_A=AreaPunisher(A_small=1.25*area, 
                                  A_large=2*area,
                                  strength=10)
    
    (analytical_forces, 
     numerical_forces, 
     analytical_stress, 
     numerical_stress) = get_num_and_ana_graidents(atoms, [extrapot_small_A])
    
    
    analytical_stress*=variable_stress_components
     
    numerical_stress*=variable_stress_components
    
    
    assert np.allclose(analytical_forces, numerical_forces, atol=1e-4, rtol=0)
    
    assert np.allclose(analytical_stress, numerical_stress, atol=1e-4, rtol=0)

    
    extrapot_large_A=AreaPunisher(A_small=0.25*area, 
                                  A_large=0.75*area,
                                  strength=10)
    
    (analytical_forces, 
     numerical_forces, 
     analytical_stress, 
     numerical_stress) = get_num_and_ana_graidents(atoms, [extrapot_large_A])
    
    analytical_stress*=variable_stress_components
     
    numerical_stress*=variable_stress_components
    
    assert np.allclose(analytical_forces, numerical_forces, atol=1e-4, rtol=0)
    
    assert np.allclose(analytical_stress, numerical_stress, atol=1e-4, rtol=0)


def test_CalculatorExtraPotential():
    
    atoms=create_atoms()
    
    calculator=EMT()
    
    extrapot_calc=CalculatorExtraPotential(calculator)
        
    (analytical_forces, 
     numerical_forces, 
     analytical_stress, 
     numerical_stress) = get_num_and_ana_graidents(atoms, [extrapot_calc])
    
    assert np.allclose(analytical_forces, numerical_forces, atol=5e-3, rtol=0)
    
    assert np.allclose(analytical_stress, numerical_stress, atol=5e-3, rtol=0)



@pytest.mark.parametrize('extrapotentials', [[TallStructurePunisher(zlow=4.0, zhigh=6.0, strength=100)],
                                             [AreaPunisher(A_small=5, A_large=50, strength=10), 
                                             TallStructurePunisher(zlow=4.0, zhigh=6.0, strength=100)]  ])
def test_None_and_double(extrapotentials):
    
    atoms=create_xy_atoms()
    
    variable_stress_components=[True, True, False, False, False, True]
        
    (analytical_forces, 
     numerical_forces, 
     analytical_stress, 
     numerical_stress) = get_num_and_ana_graidents(atoms, extrapotentials)

    analytical_stress*=variable_stress_components
     
    numerical_stress*=variable_stress_components
    
    assert np.allclose(analytical_forces, numerical_forces, atol=1e-4, rtol=0)
    
    assert np.allclose(analytical_stress, numerical_stress, atol=1e-4, rtol=0)
