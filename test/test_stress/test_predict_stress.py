from ase import Atoms

from gpatom.gpfp.calculator import GPCalculator, PriorCalculator

from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.gpfp.gp import GaussianProcess
from gpatom.gpfp.fingerprint import FingerPrint
from gpatom.gpfp.prior import RepulsivePotential, CalculatorPrior, ConstantPrior


from gpatom.beacon.str_gen import RandomCell
from ase.calculators.emt import EMT

import numpy as np
import pytest




def create_atoms():
    atoms = Atoms(['Au']*4, positions=[[0., 0., 0.]]*4)
    atoms.pbc = True
    atoms.positions[1] = [1.3, 1.6, 2.0]
    atoms.positions[2] = [2.2, 1.0, 0.5]
    atoms.positions[3] = [2.5, 0.1, 2.5]
    atoms.center(vacuum=1.5)
    return atoms




def create_gpcalc(prior, angular=True):
    atoms = create_atoms()

    sgen = RandomCell(atoms, rng=np.random.RandomState(102))

    train_images = [sgen.get(), sgen.get()]
    for atoms in train_images:
        atoms.calc = EMT()


    gp_args = dict(prior=prior, 
                   hp={'scale': 1000, 'weight': 100, 
                       'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1},
                   use_forces=True) 



    fp_args={'r_cutoff': 8.0,
             'r_delta': 0.4,
             'r_nbins': 200,
             'a_cutoff': 4.0,
             'a_delta': 0.4,
             'a_nbins': 100,
             'gamma': 0.5,
             'aweight': 1}


    fp=FingerPrint(angular=angular, fp_args=fp_args, calc_strain=True)
    
    gp=GaussianProcess(**gp_args)


    model = Model(gp=gp, fp=fp)
    
    
    model.add_data_points(train_images)
    
    
    return GPCalculator(model, calculate_stress=True)




def test_nummerical_stress_and_forces():

    #just test that this way of estimating numerical forces and stresses
    # actually work on EMT before we use it on gaussian process calculator    

    atoms = create_atoms()
    
    calc_emt=EMT()
    atoms.calc = calc_emt
    
    atoms.get_potential_energy()
    
    EMT_analytical_stress = atoms.get_stress()
    
    EMT_numerical_stress = atoms.calc.calculate_numerical_stress(atoms)    
    
    EMT_analytical_forces = atoms.get_forces()
    
    EMT_numerical_forces = atoms.calc.calculate_numerical_forces(atoms)
    
    
    print("calculated stress EMT:", EMT_analytical_stress)
    print("numerical stress EMT:", EMT_numerical_stress)
    print("calculated forces EMT:", EMT_analytical_forces)
    print("numerical forces EMT:", EMT_numerical_forces)
    
    
    assert np.allclose(EMT_analytical_forces, EMT_numerical_forces, atol=5e-3, rtol=1e-3)

    assert np.allclose(EMT_analytical_stress, EMT_numerical_stress, atol=5e-3, rtol=1e-3)






def test_predicted_stress_and_forces_RepulsivePrior():

    potential=RepulsivePotential()
    
    calc=PriorCalculator(potential, calculate_stress=True)
    
    atoms=create_atoms()
   
    atoms.calc=calc 
   
    atoms.get_potential_energy() 
    
    
    analytical_forces=calc.results['forces']
    
    analytical_stress=calc.results['stress']
    
    numerical_forces = atoms.calc.calculate_numerical_forces(atoms)    

    numerical_stress = calc.calculate_numerical_stress(atoms) 
    
    
    print("analytical stress:", analytical_stress)
    print("numerical stress:", numerical_stress)
 
    
    print("analytical forces:", analytical_forces)
    print("numerical forces:", numerical_forces)    
    
    
    assert np.allclose(analytical_forces, numerical_forces, atol=5e-3, rtol=1e-3)
    
    assert np.allclose(analytical_stress, numerical_stress, atol=5e-3, rtol=1e-3)
    
   
    





@pytest.mark.parametrize('angular', [False, True])
@pytest.mark.parametrize('prior', [ConstantPrior(constant=0), 
                                   CalculatorPrior(RepulsivePotential(potential_type='LJ', prefactor=10), constant=0),
                                   CalculatorPrior(RepulsivePotential(potential_type='parabola', prefactor=10), constant=0)])
def test_predicted_stress_and_forces_GP(prior, angular):
    
    # test that the predicted forces and stresses are identical to the 
    # ones estimated by the ase build in numerical force and stress predictor

    atoms = create_atoms()
        
    calc = create_gpcalc(prior, angular)
    atoms.calc = calc
    atoms.get_potential_energy()


    # Get GP predicted stress and forces
    GP_analytical_stress = calc.results['stress'] 
    
    GP_numerical_stress = atoms.calc.calculate_numerical_stress(atoms)
    
    GP_analytical_forces = calc.results['forces'] 
    
    GP_numerical_forces = atoms.calc.calculate_numerical_forces(atoms)
    
    
    print("predicted stress:", GP_analytical_stress)
    
    print("numerical stress:", GP_numerical_stress)

    print("predicted forces", GP_analytical_forces)

    print("numerical forces:", GP_numerical_forces)
    

    assert np.allclose(GP_analytical_forces, GP_numerical_forces, atol=5e-3, rtol=1e-3)

    assert np.allclose(GP_analytical_stress, GP_numerical_stress, atol=5e-3, rtol=1e-3)
    
    
    
