from ase import Atoms
from ase.visualize import view
from gpatom.gpfp.fingerprint import RadialAngularFP
from gpatom.gpfp.kerneltypes import SquaredExp
from matplotlib import pyplot as plt
import numpy as np
import time


def test_dk_dc():
    dx = 0.00001

    atoms = Atoms(['Au']*3, positions=[[0., 0., 0.]]*3)
    atoms.pbc = True
    atoms.positions[1] = [1.3, 1.6, 2.0]
    atoms.positions[2] = [2.2, 1.0, 0.5]
    atoms.center(vacuum=0.5)

    fp0 = RadialAngularFP(atoms=atoms, calc_strain=True)

    atoms2 = atoms.copy()
    atoms2.rattle(0.2, seed=43)
    fp2 = RadialAngularFP(atoms=atoms2, calc_strain=True)

    kernel = SquaredExp(weight=1.0, scale=100.)

    numerical = np.zeros((3, 3))

    for i1 in range(3):
        for i2 in range(3):

            atoms1 = atoms.copy()
            x = np.eye(3)
            x[i1, i2] += dx
            x[i2, i1] += dx
            atoms1.set_cell(np.dot(atoms1.cell, x), scale_atoms=True)
            fp1 = RadialAngularFP(atoms=atoms1)

            k1 = kernel.kernel(fp0, fp2)
            k2 = kernel.kernel(fp1, fp2)
            numerical[i1, i2] = (k2 - k1) / (2 * dx)

    print('Numerical:')
    print(numerical)

    analytical = kernel.dkernel_dc(fp0, fp2)
    print('Analytical:')
    print(analytical)

    assert np.allclose(numerical, analytical, atol=1e-2, rtol=1e-3)