from ase import Atoms
from gpatom.gpfp.fingerprint import RadialAngularFP
from gpatom.gpfp.kerneltypes import SquaredExp
import numpy as np

from gpatom.beacon.str_gen import RandomCell, RandomBranch

def get_atoms(natoms=4, nelem=1, pbc=False):

    elems = ['Cu', 'Au', 'Ag', 'Pt']  # elements with EMT potential

    atoms = Atoms([elems[0]] * natoms, positions=[[0., 0., 0.]] * natoms)

    for j in range(natoms):
        atoms.symbols[j] = elems[j % nelem]

    if pbc:
        atoms.pbc = True
        density = 20.0  # cubic angstroms per atom
        atoms.center(vacuum=(0.5 * (natoms * density)**(1/3.)))
        new_atoms = RandomCell(atoms, rng=np.random.RandomState(715517)).get()

    else:
        atoms.center(vacuum=6.0)
        atoms.pbc = False
        new_atoms = RandomBranch(atoms, llim=2.0, ulim=2.2,
                                 rng=np.random.RandomState(715517)).get()

    return new_atoms
    

def numerical_gradient(function, dx=0.001, **params):

    result_init = function(**params, dx=-dx)
    result_final = function(**params, dx=dx)

    return (result_final - result_init) / (2*dx)


def get_differentiated_kernelgradient(atoms, fp2, atom_indices, kernel, fp_params, dx):

    atoms1 = atoms.copy()
    atom_idx, coord_idx = atom_indices
    atoms1.positions[atom_idx, coord_idx]+=dx

    fp_test = RadialAngularFP(atoms=atoms1, **fp_params)
        
    return kernel.dkernel_dc(fp2, fp_test) 


def test_ddr_dkc():
    '''
    test that the gradient of the cell derivative of 
    the kernel match with the numerical one
    '''
    atoms = get_atoms(natoms=4, nelem=1, pbc=True)

    atoms2 = atoms.copy()
    atoms2.rattle(0.2)

    fp_params={}

    fp2=RadialAngularFP(atoms=atoms2, calc_strain=True, **fp_params)

    kernel = SquaredExp(weight=1.0, scale=100.)

    params = dict(atoms=atoms, fp2=fp2, kernel=kernel, fp_params=fp_params)

    numerical = np.zeros((3, 3, len(atoms), 3))
    for i1 in range(len(atoms)):
        for i2 in range(3):

            params.update({'atom_indices': (i1, i2)})

            numerical[:,:, i1, i2] = numerical_gradient(get_differentiated_kernelgradient, **params)

    print('Numerical:')
    print(numerical)

    fp_test = RadialAngularFP(atoms=atoms.copy(), calc_strain=True, **fp_params)
    analytical = kernel.ddr_dkdc(fp_test, fp2)

    print('Analytical:')
    print(analytical)

    assert np.allclose(numerical, analytical, atol=1e-4, rtol=1e-3)