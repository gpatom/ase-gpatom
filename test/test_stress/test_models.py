
import numpy as np
from ase import Atoms
from gpatom.beacon.str_gen import RandomCell
from gpatom.gpfp.fingerprint import FingerPrint

from gpatom.gpfp.calculator import GPCalculator

from gpatom.gpfp.atoms_gp_interface import Model, LCBModel
from gpatom.gpfp.gp import GaussianProcess
from gpatom.gpfp.prior import RepulsivePotential, CalculatorPrior, ConstantPrior

from ase.calculators.emt import EMT

import pytest    


def get_train_set(atoms):
    sgen = RandomCell(atoms, rng=np.random.RandomState(102))

    train_images = [sgen.get(), sgen.get()]
    for atoms in train_images:
        atoms.calc = EMT()
    return train_images


def create_atoms():
    atoms = Atoms(['Au']*4, positions=[[0., 0., 0.]]*4)
    atoms.pbc = True
    atoms.positions[1] = [1.3, 1.6, 2.0]
    atoms.positions[2] = [2.2, 1.0, 0.5]
    atoms.positions[3] = [2.5, 0.1, 2.5]
    atoms.center(vacuum=1.5)
    return atoms


def create_gpcalc(model_class, prior, use_grads):
    atoms = create_atoms()

    train_images=get_train_set(atoms)

    gp_args = dict(prior=prior, 
                   hp={'scale': 1000, 'weight': 100, 
                       'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1},
                   use_forces=use_grads[0], use_stress=use_grads[1]) 

    fp=FingerPrint(angular=True, calc_strain=True)
     
    gp=GaussianProcess(**gp_args)
    
    model = model_class(gp=gp, fp=fp)
        
    model.add_data_points(train_images)
    
    return GPCalculator(model, calculate_stress=True)




@pytest.mark.parametrize('use_grads', [[False, False], [True, False], [True, True]])
@pytest.mark.parametrize('prior', [ConstantPrior(constant=0),
                                   CalculatorPrior(RepulsivePotential(constant=0))]) 
@pytest.mark.parametrize('model_class',  [Model, LCBModel])
def test_predicted_stress_and_forces_GP(model_class, prior, use_grads):
    
    # test that the predicted forces and stresses are identical to the 
    # ones estimated by the ase build in numerical force and stress predictor

    atoms = create_atoms()
    
    calc = create_gpcalc(model_class, prior, use_grads)
    atoms.calc = calc
    atoms.get_potential_energy()

    # Get GP predicted stress and forces
    GP_analytical_stress = calc.results['stress'] 
    
    GP_numerical_stress = atoms.calc.calculate_numerical_stress(atoms)
    
    GP_analytical_forces = calc.results['forces'] 
    
    GP_numerical_forces = atoms.calc.calculate_numerical_forces(atoms)
    
    
    print("predicted stress:", GP_analytical_stress)
    
    print("numerical stress:", GP_numerical_stress)

    print("predicted forces", GP_analytical_forces)

    print("numerical forces:", GP_numerical_forces)
    

    assert np.allclose(GP_analytical_forces, GP_numerical_forces, atol=5e-3, rtol=1e-3)

    assert np.allclose(GP_analytical_stress, GP_numerical_stress, atol=5e-3, rtol=1e-3)