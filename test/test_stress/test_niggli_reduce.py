#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 23 15:44:00 2024

@author: casper
"""

import pytest
from ase import Atoms
import numpy as np
from gpatom.gpfp.fingerprint import RadialAngularFP
from gpatom.beacon.str_gen import RandomCell

from ase.build import niggli_reduce

from ase.visualize import view

from ase.calculators.emt import EMT

import matplotlib.pyplot as plt

def test_niggli_reduce():
    
    atoms=Atoms(['Au']*4, positions=[(0,0,0)]*4)
    atoms.pbc=True
    atoms.cell=[5,5,5]
    
    rng=np.random.RandomState(89787)
    
    sgen=RandomCell(atoms, scale=0.50, rng=rng)
    
    test_atoms=sgen.get()

    fp_args={'r_cutoff': 8, 'a_cutoff': 4}
    
    test_atoms_niggli=test_atoms.copy()
    niggli_reduce(test_atoms_niggli)

    test_atoms.calc=EMT()
    test_atoms_niggli.calc=EMT()

    eng=test_atoms.get_potential_energy()
    eng_niggli=test_atoms.get_potential_energy()
    
    assert np.isclose(eng, eng_niggli)
    
    fp=RadialAngularFP(test_atoms, calc_strain=True, **fp_args)
    fp_niggli=RadialAngularFP(test_atoms_niggli, calc_strain=True, **fp_args)

    assert np.allclose(fp.vector , fp_niggli.vector)   
