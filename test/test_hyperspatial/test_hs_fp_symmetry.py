import pytest
from gpatom.hyperspatial_beacon.gpfp.fingerprint import HighDimRadialAngularFP
from ase.cluster import Icosahedron
import numpy as np


def check_fingerprints_equality(fp1, fp2):

    vec1 = fp1.vector
    vec2 = fp2.vector

    d = np.linalg.norm(vec1 - vec2)
    
    # check atoms are different
    assert  not (fp1.atoms.positions == fp2.atoms.positions).all()
    
    distance_is_small = (d < 1e-4)
    vectors_are_equal = np.allclose(vec1, vec2, atol=1e-8)

    assert distance_is_small
    assert vectors_are_equal


@pytest.mark.parametrize('dimensions', [4,5])
def test_fingerprint_symmetries(dimensions):

    rng = np.random.RandomState(34534)
    slab = Icosahedron('Cu', noshells=2, latticeconstant=2.5)
    slab.center(vacuum=4.0)

    extra_coords = rng.rand(len(slab),dimensions-3)-0.5
    
    rng = np.random.RandomState(53453)
    
    fp = HighDimRadialAngularFP

    fp0 = fp(atoms=slab.copy(), extra_coords=extra_coords)

    # TRANSLATION
    slab_trans=slab.copy()
    slab_trans.positions += rng.random(3)
    extra_coords_trans = extra_coords.copy() + rng.random(dimensions-3)  
    fp_trans = fp(atoms=slab_trans, extra_coords=extra_coords_trans)

    check_fingerprints_equality(fp0, fp_trans)

    # ROTATION (just 3D.  4D rotation was too difficult to set up)  
    slab_rot=slab.copy()      
    slab_rot.rotate(a=180. * rng.random(), v=rng.random(3), center='COM')
    fp_rot = fp(atoms=slab_rot, extra_coords=extra_coords)

    check_fingerprints_equality(fp0, fp_rot)
    
    # PERMUTATION (swap both positions and extra_coords)
    slab_perm=slab.copy()
    extra_coords_perm=extra_coords.copy()
    
    tmp = slab_perm[3].position.copy()
    slab_perm[3].position = slab_perm[4].position
    slab_perm[4].position = tmp
    
    tmp = extra_coords_perm[3,:].copy()
    extra_coords_perm[3,:]=extra_coords_perm[4,:]
    extra_coords_perm[4,:]=tmp
    
    fp_perm = fp(atoms=slab_perm, extra_coords=extra_coords_perm)

    check_fingerprints_equality(fp0, fp_perm)
       