#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 23 19:09:52 2023

@author: casper
"""

import pytest
from ase import Atoms
from gpatom.gpfp.atoms_gp_interface import Model, LCBModel
from gpatom.beacon.str_gen import RandomBranch
from gpatom.hyperspatial_beacon.gpfp.gp import HighDimGaussianProcess
from gpatom.hyperspatial_beacon.gpfp.fingerprint import HighDimFingerPrint
from gpatom.hyperspatial_beacon.gpfp.prior import (HighDimRepulsivePotential,
                                                   HighDimCalculatorPrior)
from ase.calculators.emt import EMT
import numpy as np

from ase.stress import (full_3x3_to_voigt_6_stress,
                        voigt_6_to_full_3x3_stress)


def numeric_grads(atoms, extra_coords, model, a, i, d=1e-3):
    """Compute numeric force on atom with index a, Cartesian component i,
    with finite step of size d
    """
    p0 = atoms.get_positions()
    p = p0.copy()
    p[a, i] += d
    atoms.set_positions(p, apply_constraint=False)
    eplus=model.calculate(atoms, extra_coords=extra_coords)[0]
    
    p[a, i] -= 2 * d
    atoms.set_positions(p, apply_constraint=False)
    eminus=model.calculate(atoms, extra_coords=extra_coords)[0]
        
    atoms.set_positions(p0, apply_constraint=False)
    return (eplus -eminus) / (2 * d)


def numeric_extra_grads(atoms, extra_coords, model, a, i, d=1e-3):
    
    ei=i-3
    extra_coords_0=extra_coords.copy()

    extra_coords_0[a, ei] += d
    eplus=model.calculate(atoms, extra_coords=extra_coords_0)[0]
    
    extra_coords_0[a, ei] -= 2 * d
    eminus=model.calculate(atoms, extra_coords=extra_coords_0)[0]
    
    return (eplus -eminus) / (2 * d)


def numeric_stress(atoms, extra_coords, model, d=1e-3, voigt=False):
    stress = np.zeros((3, 3), dtype=float)

    cell = atoms.cell.copy()
    V = atoms.get_volume()
    for i in range(3):
        x = np.eye(3)
        x[i, i] += d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eplus=model.calculate(atoms, extra_coords=extra_coords)[0]

        x[i, i] -= 2 * d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eminus=model.calculate(atoms, extra_coords=extra_coords)[0]

        stress[i, i] = (eplus - eminus) / (2 * d * V)
        x[i, i] += d

        j = i - 2
        x[i, j] = d
        x[j, i] = d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eplus=model.calculate(atoms, extra_coords=extra_coords)[0]


        x[i, j] = -d
        x[j, i] = -d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eminus=model.calculate(atoms, extra_coords=extra_coords)[0]

        stress[i, j] = (eplus - eminus) / (4 * d * V)
        stress[j, i] = stress[i, j]
    atoms.set_cell(cell, scale_atoms=True)

    if voigt:
        return stress.flat[[0, 4, 8, 5, 2, 1]]
    else:
        return stress


def get_training_data(sgen, atoms):
    
    training_atoms=[]
    ninit = 2
    for i in range(ninit):
        atoms = sgen.get()
        atoms.calc = EMT()
        training_atoms.append(atoms)
        
    return training_atoms







@pytest.mark.parametrize('use_grads', [ [False, False], [True, False], [True, True] ] ) 
@pytest.mark.parametrize('model_class', [Model, LCBModel])
def test_forces_and_stress(use_grads, model_class): 

    
    atoms = Atoms(['Au'] * 3 + ['Cu'] * 2, positions=[[0., 0., 0.]] * 5)
    atoms.pbc = True
    atoms.center(vacuum=6.0)
    
    dimensions=5
    
    rng = np.random.RandomState(73721) 
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)

    potential=HighDimRepulsivePotential(potential_type='parabola', prefactor=10)
    prior=HighDimCalculatorPrior(potential, constant=0)
    
    gp_args=dict(prior=prior,
                 hp={'scale': 1000}, 
                 use_forces=use_grads[0], 
                 use_stress=use_grads[1])
        
    gp = HighDimGaussianProcess(**gp_args)  

    fp=HighDimFingerPrint(calc_strain=True)

    model=model_class(gp=gp, fp=fp)
    
    training_atoms=get_training_data(sgen, atoms)

    model.add_data_points(training_atoms)

    testatoms=sgen.get()
    
    rng_extra = np.random.RandomState(8)
    extra_coords= 1*(rng_extra.rand(len(testatoms), dimensions-3 ) - 0.5)
    
    
    (energy, 
     analytical_grads, 
     stress) = model.calculate(testatoms, extra_coords=extra_coords, with_stress=True)
    
    analytical_stress = voigt_6_to_full_3x3_stress(stress)
                    
           
    numerical_grads=np.zeros( np.shape(analytical_grads) )

    for a in range(len(atoms)):
        for i in range(dimensions):            
            if i>2:
                numerical_grads[a,i]=numeric_extra_grads(testatoms, extra_coords, model, a, i)
            else:
                numerical_grads[a,i]=numeric_grads(testatoms, extra_coords, model, a, i)
    
    assert np.allclose(numerical_grads/analytical_grads, 1, atol=1e-3, rtol=0 )
    
    numerical_stress=numeric_stress(testatoms, extra_coords, model)

    assert np.allclose(numerical_stress/analytical_stress, 1, atol=1e-3, rtol=0 ) 