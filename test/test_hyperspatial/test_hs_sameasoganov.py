#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 23 17:43:22 2023

@author: casper
"""

import pytest
from ase.build import bulk
from gpatom.hyperspatial_beacon.gpfp.fingerprint import HighDimRadialAngularFP
from gpatom.gpfp.fingerprint import RadialAngularFP
import numpy as np


def basetest_fingerprint(atoms=None, extra_coords=None):
    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1}  

    fp = HighDimRadialAngularFP(atoms=atoms, calc_strain=True, **fp_args)
    v1 = fp.vector
    v1_grad=fp.reduce_coord_gradients()
    v1_strain=fp.reduce_strain()

    fp = RadialAngularFP(atoms=atoms,calc_strain=True, **fp_args)
    v2 = fp.vector
    v2_grad=fp.reduce_coord_gradients()
    v2_strain=fp.reduce_strain()

    assert np.allclose(v1, v2, atol=1e-6)
    assert np.allclose(v1_grad, v2_grad, atol=1e-6)
    assert np.allclose(v1_strain, v2_strain, atol=1e-6)
    
    
@pytest.mark.parametrize('dimensions', [4,5] )
@pytest.mark.parametrize('periodic', [True, False])
def test_fingerprint(periodic, dimensions):
    atoms = bulk('Cu', 'fcc', a=3.6)
    atoms=atoms.repeat( (2,2,2) )
    atoms.pbc = periodic
    extra_coords=np.zeros( (len(atoms), dimensions-3) )
    basetest_fingerprint(atoms=atoms,extra_coords=extra_coords)

@pytest.mark.parametrize('periodic', [True, False])
def test_fingerprint_dimensions_None(periodic):
    atoms = bulk('Cu', 'fcc', a=3.6)
    atoms=atoms.repeat( (2,2,2) )
    atoms.pbc = periodic
    basetest_fingerprint(atoms=atoms,extra_coords=None)

    