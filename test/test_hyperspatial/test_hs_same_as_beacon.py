#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 23 19:11:12 2023

@author: casper
"""
from gpatom.hyperspatial_beacon.gpfp.fingerprint import HighDimFingerPrint
from gpatom.gpfp.fingerprint import FingerPrint
import numpy as np

from ase import Atoms
from gpatom.beacon.str_gen import RandomBranch
from ase.calculators.emt import EMT
from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.gpfp.gp import GaussianProcess
from gpatom.hyperspatial_beacon.gpfp.gp import HighDimGaussianProcess
from gpatom.gpfp.prior import ConstantPrior
 
def test_highdim_gp_and_beacon_gp_predictions_agree():

    atoms = Atoms(['Au'] * 2 + ['Cu'] * 2, positions=[[0., 0., 0.]] * 4)
    atoms.pbc = False
    atoms.center(vacuum=6.0)
    
    rng = np.random.RandomState(9)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)
    
    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1}  

    highdim_fp=HighDimFingerPrint(fp_args=fp_args, calc_strain=True)

    fp=FingerPrint(fp_args=fp_args, calc_strain=True)    
    
    gp_args=dict(prior=ConstantPrior(constant=0), 
                 hp={'scale': 1000}, use_forces=True)
    
    highdim_gp=HighDimGaussianProcess(**gp_args)
    
    gp=GaussianProcess(**gp_args)      
    
    highdim_model=Model(gp=highdim_gp, fp=highdim_fp)
    
    model=Model(gp=gp, fp=fp)

    training_atoms=[]
    for i in range(2):
        atoms=sgen.get()
        atoms.calc=EMT()
        training_atoms.append(atoms)
            
    model.add_data_points(training_atoms)    
    
    highdim_model.add_data_points(training_atoms)

    test_atoms=sgen.get()
    
    highdim_fp=highdim_model.fp.get(atoms=test_atoms, extra_coords=None)
    highdim_predictions, highdim_uncertainty = highdim_model.gp.predict(highdim_fp, get_variance=True)
    highdim_stress = highdim_model.gp.predict_stress(highdim_fp)  
    
    normal_fp=model.fp.get(atoms=test_atoms)
    predictions, uncertainty = model.gp.predict(normal_fp, get_variance=True)
    stress = model.gp.predict_stress(normal_fp)
    
    assert(np.allclose(highdim_predictions, predictions, atol=0.0, rtol=0.01))
    assert(np.allclose(highdim_stress, stress, atol=0.0, rtol=0.01))
    assert(np.allclose(highdim_uncertainty.flatten(), uncertainty.flatten(), atol=0.0, rtol=0.01))
    