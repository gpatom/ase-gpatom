#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 23 16:32:03 2023

@author: casper
"""
import pytest

import numpy as np
from gpatom.hyperspatial_beacon.gpfp.fingerprint import (HighDimRadialFP,
                                                         HighDimRadialAngularFP)

from ase import Atoms

from gpatom.beacon.str_gen import RandomBox       


@pytest.mark.parametrize('periodic', [False,True])
@pytest.mark.parametrize('fp_class', [HighDimRadialFP, HighDimRadialAngularFP])
def test_dimensional_indifference(fp_class, periodic):
    atoms=Atoms(['Cu']*5, positions=[[0,0,0]]*5)
    atoms.cell=[6,6,6]
    atoms.pbc=periodic
    
    rng=np.random.RandomState(3423)

    sgen=RandomBox(atoms, box=[(1., 5.), (1., 5.), (1., 5.)], rng=rng)

    hd_atoms = sgen.get()

    extra_coords_4d=np.array([[0.1],    
                              [0.2],
                              [0.3],
                              [0.4],
                              [0.5]])
    
    extra_coords_5d=np.array([[0.1,0.0],    
                              [0.2,0.0],
                              [0.3,0.0],
                              [0.4,0.0],
                              [0.5,0.0]])
    
    v4d = fp_class(atoms=hd_atoms, extra_coords=extra_coords_4d).vector   
    
    v5d = fp_class(atoms=hd_atoms, extra_coords=extra_coords_5d).vector
    
    assert np.allclose(v4d, v5d)
    


@pytest.mark.parametrize('dimensions', [4,5])
@pytest.mark.parametrize('periodic', [False,True])
@pytest.mark.parametrize('fp_class', [HighDimRadialFP, HighDimRadialAngularFP])
def test_fp_derivative(fp_class, dimensions, periodic):
    
    '''
    tests that gradient of fingerprint is correct with respect to
    coordinates and extra_coordinates
    '''
    
    atoms=Atoms(['Cu']*5, positions=[[0,0,0]]*5)
    atoms.cell=[6,6,6]
    atoms.pbc=periodic
    
    rng=np.random.RandomState(3423)

    sgen=RandomBox(atoms, box=[(1., 5.), (1., 5.), (1., 5.)], rng=rng)

    extra_coords=rng.rand(len(atoms),  dimensions-3 )-0.5

    dx = 0.000001
    
    hd_atoms = sgen.get()

    fp1 = fp_class(atoms=hd_atoms, extra_coords=extra_coords)
    drho_analytical = fp1.reduce_coord_gradients()[0,:, :] # just test atom_0
    v1 = fp_class(atoms=hd_atoms, extra_coords=extra_coords).vector
    
    drho_numerical=np.zeros(np.shape(drho_analytical))
    
    
    for j in range(dimensions):
        extra_coords_copy=extra_coords.copy()
        atoms_copy=hd_atoms.copy()
        
        if j>2:
            extra_coords_copy[0,j-3]+=dx
        else:
            atoms_copy.positions[0,j]+=dx
    
        v2 = fp_class(atoms=atoms_copy, extra_coords=extra_coords_copy).vector
 
        drho_numerical[:,j] = (v2 - v1) / dx

    print(drho_numerical)
    print(drho_analytical)     
    assert np.allclose(drho_numerical, drho_analytical, rtol=1e-5, atol=1e-2)



@pytest.mark.parametrize('fp_class', [HighDimRadialFP, HighDimRadialAngularFP])
def test_fp_strain(fp_class):

    '''
    Test that the fingerprint strain is correct by comparing
    to numerical finite-difference result.
    '''

    dx = 0.00001

    atoms = Atoms(['Au']*3, positions=[[0., 0., 0.]]*3)
    atoms.pbc = True
    atoms.positions[1] = [1.3, 1.6, 2.0]
    atoms.positions[2] = [2.2, 1.0, 0.5]
    atoms[2].symbol = 'Cu'
    atoms.center(vacuum=0.5)
    
    rng=np.random.RandomState(7681)
        
    extra_coords=10*rng.rand(len(atoms)).reshape(-1,1)

    i1, i2 = 0, 0

    fp0 = fp_class(atoms=atoms, extra_coords=extra_coords, calc_strain=True)
    v0 = fp0.vector
    atoms1 = atoms.copy()
    x = np.eye(3)
    x[i1, i2] += dx
    x[i2, i1] += dx
    atoms1.set_cell(np.dot(atoms1.cell, x), scale_atoms=True)
    fp1 = fp_class(atoms=atoms1, extra_coords=extra_coords, calc_strain=True)
    v1 = fp1.vector

    numerical = (v1 - v0) / (2 * dx)

    analytical = fp0.reduce_strain()[:, i1, i2]

    assert np.allclose(analytical, numerical, atol=5e-3, rtol=1e-3)