#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 15:28:46 2024

@author: casper
"""


from gpatom.hyperspatial_beacon.hyperspacebeacon import HighDimChecker
from ase.build.surface import fcc100

def test_HighDimChecker():

    atoms = fcc100('Au', (2, 2, 1))
    atoms.pbc = True
    atoms.info['squeeze_complete']=True

    checker=HighDimChecker(check_squeeze=True)

    distances=[10]  # just need to be there to work

    structure_ok, output_string=checker.check(atoms, distances)

    assert structure_ok
    
    atoms.info['squeeze_complete']=False
    
    structure_ok, output_string=checker.check(atoms, distances)
    
    assert not structure_ok