#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 23 19:09:52 2023

@author: casper
"""

import pytest
from ase import Atoms
from gpatom.beacon.str_gen import RandomBranch
from gpatom.hyperspatial_beacon.gpfp.gp import HighDimGaussianProcess
from gpatom.hyperspatial_beacon.gpfp.fingerprint import HighDimFingerPrint
from gpatom.hyperspatial_beacon.gpfp.prior import (HighDimConstantPrior, 
                                                   HighDimRepulsivePotential,
                                                   HighDimCalculatorPrior)
from ase.calculators.emt import EMT
import numpy as np

from ase.stress import (full_3x3_to_voigt_6_stress,
                        voigt_6_to_full_3x3_stress)


def numeric_force(atoms, extra_coords, gp, fp, a, i, d=1e-6):
    """Compute numeric force on atom with index a, Cartesian component i,
    with finite step of size d
    """
    p0 = atoms.get_positions()
    p = p0.copy()
    p[a, i] += d
    atoms.set_positions(p, apply_constraint=False)
    
    atoms_fp=fp.get(atoms, extra_coords=extra_coords)
    predict, unc = gp.predict(atoms_fp)
    eplus=predict[0]
    
    p[a, i] -= 2 * d
    atoms.set_positions(p, apply_constraint=False)
        
    atoms_fp=fp.get(atoms, extra_coords=extra_coords)
    predict, unc = gp.predict(atoms_fp)
    eminus=predict[0]
        
    atoms.set_positions(p0, apply_constraint=False)
    return (eminus - eplus) / (2 * d)


def numeric_extra_force(atoms, extra_coords, gp, fp, a, i, d=1e-6):
    
    ei=i-3
    extra_coords_0=extra_coords.copy()

    extra_coords_0[a, ei] += d
    atoms_fp=fp.get(atoms, extra_coords=extra_coords_0)
    predict, unc = gp.predict(atoms_fp)
    eplus=predict[0]
    
    extra_coords_0[a, ei] -= 2 * d
    atoms_fp=fp.get(atoms, extra_coords=extra_coords_0)
    predict, unc = gp.predict(atoms_fp)
    eminus=predict[0]
    
    return (eminus - eplus) / (2 * d)


def numeric_stress_hs(atoms, extra_coords, gp, fp, d=1e-6, voigt=False):
    stress = np.zeros((3, 3), dtype=float)

    cell = atoms.cell.copy()
    V = atoms.get_volume()
    for i in range(3):
        x = np.eye(3)
        x[i, i] += d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, extra_coords=extra_coords)
        predict, unc = gp.predict(atoms_fp)
        eplus=predict[0]

        x[i, i] -= 2 * d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, extra_coords=extra_coords)
        predict, unc = gp.predict(atoms_fp)
        eminus=predict[0]

        stress[i, i] = (eplus - eminus) / (2 * d * V)
        x[i, i] += d

        j = i - 2
        x[i, j] = d
        x[j, i] = d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, extra_coords=extra_coords)
        predict, unc = gp.predict(atoms_fp)
        eplus=predict[0]


        x[i, j] = -d
        x[j, i] = -d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, extra_coords=extra_coords)
        predict, unc = gp.predict(atoms_fp)
        eminus=predict[0]

        stress[i, j] = (eplus - eminus) / (4 * d * V)
        stress[j, i] = stress[i, j]
    atoms.set_cell(cell, scale_atoms=True)

    if voigt:
        return stress.flat[[0, 4, 8, 5, 2, 1]]
    else:
        return stress


def get_trained_gp(sgen, fp, gp, use_forces):

    trainingfeatures = []
    trainingtargets = []
    ninit = 2
    for i in range(ninit):
        atoms = sgen.get()
        atoms.calc = EMT()
        atoms.get_potential_energy()
        trainingfeatures.append(fp.get(atoms))
        
        if use_forces:
            trainingtargets.append([atoms.get_potential_energy()] +
                                   list(atoms.get_forces().flatten()))
        else: 
            trainingtargets.append([atoms.get_potential_energy()]) 
        
    trainingtargets = np.array(trainingtargets).flatten()
    
    gp.train(trainingfeatures, trainingtargets)

    return gp
  
@pytest.mark.parametrize('use_forces', [True, False] ) 
@pytest.mark.parametrize('dimensions', [3, 4, 5, 6])
@pytest.mark.parametrize('prior', [HighDimConstantPrior(constant=0),
                                   HighDimCalculatorPrior(HighDimRepulsivePotential(potential_type='parabola', prefactor=10), constant=0)])
def test_forces_and_stress(dimensions, use_forces, prior): 

    atoms = Atoms(['Au'] * 3 + ['Cu'] * 2, positions=[[0., 0., 0.]] * 5)
    atoms.pbc = True
    atoms.center(vacuum=6.0)
    rng = np.random.RandomState(73721) 
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)

    fp=HighDimFingerPrint(calc_gradients=True, calc_strain=True)

    gp_args=dict(prior=prior,
                 hp={'scale': 1000}, use_forces=use_forces)
        
    gp = HighDimGaussianProcess(**gp_args)  

    gp=get_trained_gp(sgen, fp, gp, use_forces)

    testatoms=sgen.get()
    
    rng_extra = np.random.RandomState(8)
    
    if dimensions is None:
        extra_coords=None
    else:
        extra_coords= 1*(rng_extra.rand(len(testatoms), dimensions-3 ) - 0.5)
    
    testfp0 = fp.get(atoms=testatoms, extra_coords=extra_coords)
    ef0, V = gp.predict(testfp0)
    
    analytical_forces=ef0[1:].reshape(len(atoms),dimensions)
        
    numerical_forces=np.zeros( np.shape(analytical_forces) )
    for a in range(len(atoms)):
        for i in range(dimensions):            
            if i>2:
                numerical_forces[a,i]=-numeric_extra_force(testatoms, extra_coords, gp, fp, a, i)
            else:
                numerical_forces[a,i]=-numeric_force(testatoms, extra_coords, gp, fp, a, i)
    
    assert np.allclose(numerical_forces/analytical_forces, 1, atol=1e-3, rtol=0 )
    
    stress = gp.predict_stress(testfp0)
    analytical_stress = voigt_6_to_full_3x3_stress(stress)
    
    numerical_stress=numeric_stress_hs(testatoms, extra_coords, gp, fp)

    assert np.allclose(numerical_stress/analytical_stress, 1, atol=1e-2, rtol=0 ) 
    