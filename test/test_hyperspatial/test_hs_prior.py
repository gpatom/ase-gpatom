#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 14:47:39 2023

@author: casper
"""

import pytest
import numpy as np
from ase.build import fcc111
from ase.data import covalent_radii

from gpatom.gpfp.calculator import PriorCalculator
from gpatom.hyperspatial_beacon.gpfp.prior import (HighDimRepulsivePotential,  
                                                   ExtraDimTallStructurePunisher)

from ase.stress import (full_3x3_to_voigt_6_stress,
                        voigt_6_to_full_3x3_stress)


def numeric_force(atoms, a, i, d=1e-6):
    """Compute numeric force on atom with index a, Cartesian component i,
    with finite step of size d#
    """
    p0 = atoms.get_positions()
    p = p0.copy()
    p[a, i] += d
    
    atoms.set_positions(p, apply_constraint=False)
    eplus=atoms.get_potential_energy()
    
    p[a, i] -= 2 * d
    atoms.set_positions(p, apply_constraint=False)
    eminus=atoms.get_potential_energy()
        
    atoms.set_positions(p0, apply_constraint=False)
    return (eminus - eplus) / (2 * d)



def numeric_extra_force(atoms, a, i, d=1e-6):
 
    ei=i-3    
    
    extra_coords=atoms.extra_coords.copy()
    
    extra_coords[a, ei] += d
    atoms.extra_coords=extra_coords  
    atoms.calc.results.clear()      
    eplus=atoms.get_potential_energy()
    
    extra_coords[a, ei] -= 2 * d
    atoms.extra_coords=extra_coords
    atoms.calc.results.clear() 
    eminus=atoms.get_potential_energy()

    return (eminus - eplus) / (2 * d)


def numeric_stress(atoms, d=1e-6, voigt=False):
    
    stress = np.zeros((3, 3), dtype=float)

    cell = atoms.cell.copy()
    V = atoms.get_volume()
    for i in range(3):
        x = np.eye(3)
        x[i, i] += d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eplus=atoms.get_potential_energy()

        x[i, i] -= 2 * d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eminus=atoms.get_potential_energy()

        stress[i, i] = (eplus - eminus) / (2 * d * V)
        x[i, i] += d

        j = i - 2
        x[i, j] = d
        x[j, i] = d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eplus=atoms.get_potential_energy()

        x[i, j] = -d
        x[j, i] = -d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        eminus=atoms.get_potential_energy()

        stress[i, j] = (eplus - eminus) / (4 * d * V)
        stress[j, i] = stress[i, j]
    atoms.set_cell(cell, scale_atoms=True)

    if voigt:
        return stress.flat[[0, 4, 8, 5, 2, 1]]
    else:
        return stress    


def new_atoms(rattle_seed=8686):     
    atoms = fcc111('Ag', size=(2, 1, 1))
    atoms.center(vacuum=4.0, axis=2)
    
    atoms.rattle(0.55, seed=rattle_seed)
    return atoms



@pytest.mark.parametrize('extrapotentials', [None,
                                            [ExtraDimTallStructurePunisher(eclow=-0.1, echigh=0.1, strength=2)]])
@pytest.mark.parametrize('dimensions', [3,4,5])
def test_HighDimRepulsiveStressPrior(dimensions, extrapotentials):
        
    atoms=new_atoms()
    
    rng=np.random.RandomState(4232) 
    if dimensions==3:
        extra_coords=None
    else:
        extra_coords=1* ( rng.random( (len(atoms),dimensions-3) ) - 0.5 )
    
    atoms.extra_coords=extra_coords
    atoms.dims=dimensions
    
    potential=HighDimRepulsivePotential(potential_type='parabola', prefactor=10, 
                                        extrapotentials=extrapotentials)
    atoms.calc=potential
        
    analytical_forces=atoms.get_forces()    
   
    numerical_forces=np.zeros(np.shape(analytical_forces))
    
    for a in range(len(atoms)):
        for i in range(dimensions):
            if i>2:
                numerical_forces[a,i]=numeric_extra_force(atoms, a, i)
            else:
                numerical_forces[a,i]=numeric_force(atoms, a, i)
    
    analytical_stress=voigt_6_to_full_3x3_stress(atoms.get_stress())
    
    numerical_stress=numeric_stress(atoms)

    assert np.allclose(analytical_forces, numerical_forces, atol=5e-3, rtol=1e-3)    
    assert np.allclose(analytical_stress, numerical_stress, atol=5e-3, rtol=1e-3)
