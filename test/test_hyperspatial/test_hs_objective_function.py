import numpy as np
from gpatom.hyperspatial_beacon.gpfp.fingerprint import HighDimFingerPrint

from ase import Atoms

from ase.calculators.emt import EMT

from ase.data import covalent_radii, atomic_numbers

from gpatom.hyperspatial_beacon.gpfp.gp import HighDimGaussianProcess

from gpatom.hyperspatial_beacon.gpfp.prior import HighDimCalculatorPrior

from gpatom.hyperspatial_beacon.str_gen import HighDimAtomsRelaxer, HighDimRandomBox

from gpatom.hyperspatial_beacon.gpfp.prior import HighDimRepulsivePotential

from gpatom.gpfp.atoms_gp_interface import Model

from gpatom.hyperspatial_beacon.hyperspacebeacon import HSParamsHandler, HighDimSurrogateOptimizer

from gpatom.fractional_beacon.icebeacon import UnitCellHandler


def numeric_gradients(natoms, positions, extra_coords, surropt, args, a, i, d=1e-6):
    """Compute numeric force on atom with index a, Cartesian component i,
    with finite step of size d
    """
    positions=positions.copy()

    positions[a, i] += d
    
    params=HSParamsHandler.pack_atomic_params(natoms, surropt.from_dims, 
                                              positions, extra_coords)
    
    e_plus, derivs_plus=surropt._calculate_properties(params, *args)

    positions[a, i] -= 2 * d
    
    params=HSParamsHandler.pack_atomic_params(natoms, surropt.from_dims,  
                                              positions, extra_coords)
    
    e_minus, derivs_minus=surropt._calculate_properties(params, *args)

    return (e_plus-e_minus) / (2 * d)



def numeric_extra_gradients(natoms, positions, extra_coords, surropt, args, a, i, d=1e-6):
    
    extra_coords=extra_coords.copy()

    extra_coords[a, i] += d
    
    params=HSParamsHandler.pack_atomic_params(natoms, surropt.from_dims,  
                                              positions, extra_coords)
    
    e_plus, derivs_plus=surropt._calculate_properties(params, *args)

    extra_coords[a, i] -= 2 * d

    params=HSParamsHandler.pack_atomic_params(natoms, surropt.from_dims, 
                                              positions, extra_coords)
    
    e_minus, derivs_minus=surropt._calculate_properties(params, *args)
    
    return (e_plus-e_minus) / (2 * d)



def test_objective_function():
    
    #RUNNING THE SIMULATION

    atoms= Atoms(['Al'] * 3 + ['Cu']*2  , positions=[[0., 0., 0.]] * 5)
    atoms.pbc = False
    atoms.cell=[10,10,10]

    rng = np.random.RandomState(88)

    r_atom = (covalent_radii[atomic_numbers['Al']] )  

    lowD=3
    
    highD=4


    potential=HighDimRepulsivePotential(potential_type='parabola', 
                                        prefactor=10, rc=0.9)

    relaxer=HighDimAtomsRelaxer(potential, with_unit_cell=False)

    sgen_lowD = HighDimRandomBox(atoms, box=[(3,7)]*lowD, rng=rng, 
                                 relaxer=relaxer)

    sgen_highD = HighDimRandomBox(atoms, box=[(4, 6)]*highD, rng=rng,
                                  relaxer=relaxer)


    fp_args = {'r_cutoff': r_atom*4.25, 'a_cutoff': r_atom*2.25, 'aweight': 1}
    fp=HighDimFingerPrint(fp_args=fp_args, 
                          angular=True, calc_strain=False)

    prior=HighDimCalculatorPrior(calculator=potential, constant=0)

    gp_args=dict(prior=prior,    
                 hp={'scale': 1000, 'weight': 100, 
                     'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1}, 
                 use_forces=True)
    
    gp=HighDimGaussianProcess(**gp_args)

    model=Model(gp=gp, fp=fp)

    training_set=[]
    for i in range(2):
        atoms_i=sgen_lowD.get()
        atoms_i.calc=EMT()
        training_set.append(atoms_i)

    model.add_data_points(training_set)

    # the most parameters are just here for placeholding as they a re not directly used in the objective method
    surropt=HighDimSurrogateOptimizer(fmax=0.001,
                                      rattle_rng=rng,
                                      to_dims=lowD,
                                      strength_array=np.array([0.5]),
                                      cycles=1,
                                      relax_steps=5, 
                                      after_steps=5, 
                                      write_surropt_trajs=True,
                                      with_unit_cell=False)
    
    surropt.set_world_center(sgen_highD.world_center, highD)
    
    
    surropt.steps_taken=0

    # get test_values
    original_atoms=sgen_highD.get()    
    
    extra_coords=original_atoms.extra_coords
    
    test_atoms=original_atoms.copy()
    positions=test_atoms.positions.copy()

    writer=surropt.initiate_writer(test_atoms, model, extra_coords, 'dummy')
    
    # transform params
    natoms=len(test_atoms)         
    
    params=HSParamsHandler.pack_atomic_params(natoms, highD, 
                                              positions, extra_coords)   

    penalize=True
    args=(writer, test_atoms, model, highD, penalize)

    # calculate objective function
    energy, derivatives = surropt._calculate_properties(params, *args)

    gradients, extra_gradients = HSParamsHandler.unpack_atomic_params(natoms, highD, 
                                                                      derivatives)

    numerical_gradients=np.zeros( np.shape(positions) )
    
    numerical_extra_gradients=np.zeros( np.shape(extra_coords) )
     
    
    dimensions=highD
    
    natoms=len(test_atoms)
    
    for a in range(len(atoms)):
        
        for i1 in range(3):
            numerical_gradients[a,i1]=numeric_gradients(natoms, positions, 
                                                        extra_coords, 
                                                        surropt, args, a, i1)
            
        for i2 in range(dimensions-3):
            numerical_extra_gradients[a,i2]=numeric_extra_gradients(natoms, 
                                                                    positions, 
                                                                    extra_coords, 
                                                                    surropt, args, a, i2)
            
    print(numerical_gradients/gradients)
    
    print(numerical_extra_gradients/extra_gradients)
    
    assert np.allclose(numerical_gradients/gradients, 1, atol=1e-2, rtol=0 )
    
    assert np.allclose(numerical_extra_gradients/extra_gradients, 1, atol=1e-2, rtol=0 )                                           








def numeric_gradients_uc(natoms, positions, extra_coords, 
                         deformation_tensor, surropt, args, a, i, d=1e-6):
    """Compute numeric force on atom with index a, Cartesian component i,
    with finite step of size d
    """
    positions=positions.copy()

    positions[a, i] += d
    
    params=HSParamsHandler.pack_params(natoms, surropt.from_dims,  
                                       positions, extra_coords, 
                                       deformation_tensor)
    
    e_plus, derivs_plus=surropt._calculate_properties_unitcell(params, *args)

    positions[a, i] -= 2 * d
    
    params=HSParamsHandler.pack_params(natoms, surropt.from_dims, 
                                       positions, extra_coords, 
                                       deformation_tensor)
    
    e_minus, derivs_minus=surropt._calculate_properties_unitcell(params, *args)

    return (e_plus-e_minus) / (2 * d)



def numeric_extra_gradients_uc(natoms, positions, extra_coords,
                               deformation_tensor, surropt, args, a, i, d=1e-6):
    
    extra_coords=extra_coords.copy()

    extra_coords[a, i] += d
    
    params=HSParamsHandler.pack_params(natoms, surropt.from_dims, 
                                       positions, extra_coords, 
                                       deformation_tensor)
    
    e_plus, derivs_plus=surropt._calculate_properties_unitcell(params, *args)

    extra_coords[a, i] -= 2 * d

    params=HSParamsHandler.pack_params(natoms, surropt.from_dims, 
                                       positions, extra_coords, 
                                       deformation_tensor)
    
    e_minus, derivs_minus=surropt._calculate_properties_unitcell(params, *args)
    
    return (e_plus-e_minus) / (2 * d)



def numeric_fraction_gradients_uc(natoms, positions, extra_coords, fractions, 
                                  deformation_tensor, surropt, args, a, i, d=1e-6):
    
    fractions=fractions.copy()   
    
    fractions[a, i] += d
    
    params=HSParamsHandler.pack_params(natoms, surropt.from_dims, 
                                       positions, extra_coords, 
                                       deformation_tensor)
    
    e_plus, derivs_plus=surropt._calculate_properties_unitcell(params, *args)

    fractions[a, i] -= 2 * d
    
    params=HSParamsHandler.pack_params(natoms, surropt.from_dims,
                                       positions, extra_coords, 
                                       deformation_tensor)
    
    e_minus, derivs_minus=surropt._calculate_properties_unitcell(params, *args)
    
    return  (e_plus-e_minus) / (2 * d)






def numeric_virial(natoms, positions, extra_coords, deformation_tensor, 
                   surropt, args, d=1e-6):
    
    
    virial = np.zeros((3, 3), dtype=float)

    for i in range(3):
        deformation_tensor[i, i] += d

        params=HSParamsHandler.pack_params(natoms, surropt.from_dims, 
                                           positions, extra_coords, 
                                           deformation_tensor)
    
        e_plus, derivs_plus=surropt._calculate_properties_unitcell(params, *args)
        

        deformation_tensor[i, i] -= 2 * d
        params=HSParamsHandler.pack_params(natoms, surropt.from_dims, 
                                           positions, extra_coords, 
                                           deformation_tensor)
    
        e_minus, derivs_minus=surropt._calculate_properties_unitcell(params, *args)

        virial[i, i] = (e_plus - e_minus) / (2 * d)
        deformation_tensor[i, i] += d  # set back

        j = i - 2
        deformation_tensor[i, j] = d
        deformation_tensor[j, i] = d

        params=HSParamsHandler.pack_params(natoms, surropt.from_dims, 
                                           positions, extra_coords, 
                                           deformation_tensor)
    
        e_plus, derivs_plus=surropt._calculate_properties_unitcell(params, *args)


        deformation_tensor[i, j] = -d
        deformation_tensor[j, i] = -d
      
        params=HSParamsHandler.pack_params(natoms, surropt.from_dims, 
                                           positions, extra_coords, 
                                           deformation_tensor)
    
        e_minus, derivs_minus=surropt._calculate_properties_unitcell(params, *args)
      

        virial[i, j] = (e_plus - e_minus) / (4 * d)
        virial[j, i] = virial[i, j]

    return virial
    

    
def test_objective_function_unitcell():
    
    #RUNNING THE SIMULATION

    atoms= Atoms(['Al'] * 3 + ['Cu']*2  , positions=[[0., 0., 0.]] * 5)
    atoms.pbc = False
    atoms.cell=[10,10,10]

    rng = np.random.RandomState(88)

    r_atom = (covalent_radii[atomic_numbers['Al']] )  

    lowD=3
    
    highD=4


    potential=HighDimRepulsivePotential(potential_type='parabola', 
                                        prefactor=10, rc=0.9)

    relaxer=HighDimAtomsRelaxer(potential)


    sgen_lowD = HighDimRandomBox(atoms, box=[(3,7)]*lowD, rng=rng, 
                                 relaxer=relaxer)

    sgen_highD = HighDimRandomBox(atoms, box=[(4, 6)]*highD, rng=rng,
                                  relaxer=relaxer)




    fp_args = {'r_cutoff': r_atom*4.25, 'a_cutoff': r_atom*2.25, 'aweight': 1}
    fp=HighDimFingerPrint(fp_args=fp_args, 
                          angular=True, calc_strain=True)


    prior=HighDimCalculatorPrior(calculator=potential, constant=0)

    gp_args=dict(prior=prior,    
                 hp={'scale': 1000, 'weight': 100, 
                     'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1}, 
                 use_forces=True)
    
    gp=HighDimGaussianProcess(**gp_args)

    model=Model(gp=gp, fp=fp)

    training_set=[]
    for i in range(2):
        atoms_i=sgen_lowD.get()
        atoms_i.calc=EMT()
        training_set.append(atoms_i)

    model.add_data_points(training_set)

    # the most parameters are just here for placeholding as they a re not directly used in the objective method
    surropt=HighDimSurrogateOptimizer(fmax=0.001,
                                      rattle_rng=rng,
                                      to_dims=lowD,
                                      strength_array=np.array([0.5]),
                                      cycles=1,
                                      relax_steps=5, 
                                      after_steps=5, 
                                      write_surropt_trajs=True,
                                      with_unit_cell=True)
    
    surropt.set_world_center(sgen_highD.world_center, highD)
    
    surropt.steps_taken=0

    # get test_values
    original_atoms=sgen_highD.get()    
    
    extra_coords=original_atoms.extra_coords
    
    original_cell=atoms.get_cell()
    
    # make a skewed cell
    d=1.1
    x = np.eye(3)
    x[0, 0] = +d
    x[1, 1] = +d
    x[2, 2] = +d
    
    cell=original_cell.copy()   
    test_atoms=original_atoms.copy()
    test_atoms.set_cell(np.dot(cell, x), scale_atoms=True)


    writer=surropt.initiate_writer(test_atoms, model, extra_coords, 'dummy')

    # check that changing system actually happened
    assert not np.allclose( original_atoms.positions.flatten(),  test_atoms.positions.flatten())
    assert not np.allclose( original_atoms.cell.flatten(),  test_atoms.cell.flatten())   
    
    
    # transform params
    natoms=len(test_atoms)
        
    cell_factor=float(natoms) 
        
    deformation_tensor, deformed_positions = UnitCellHandler.atoms_real_to_deformed(test_atoms, original_cell, cell_factor)
                                                                                                                         
    params=HSParamsHandler.pack_params(natoms, highD, deformed_positions, 
                                       extra_coords, deformation_tensor)   

    penalize=True
    args=(writer, test_atoms, model, highD, penalize, original_cell, cell_factor)


    # calculate objective function
    energy, derivatives = surropt._calculate_properties_unitcell(params, *args)

    # get the derivatives
    (deformed_gradients, 
     extra_gradients, 
     deformed_virial) = HSParamsHandler.unpack_params(natoms, highD,  
                                                      derivatives) 
                 
                                                               
    numerical_gradients=np.zeros( np.shape(deformed_positions) )
    
    numerical_extra_gradients=np.zeros( np.shape(extra_coords) )

    dimensions=highD

    for a in range(len(atoms)):
        
        for i1 in range(3):
            numerical_gradients[a,i1]=numeric_gradients_uc(natoms, deformed_positions, 
                                                           extra_coords, 
                                                           deformation_tensor, 
                                                           surropt, args, a, i1)
            
        for i2 in range(dimensions-3):
            numerical_extra_gradients[a,i2]=numeric_extra_gradients_uc(natoms, deformed_positions, 
                                                                       extra_coords,  
                                                                       deformation_tensor, 
                                                                       surropt, args, a, i2)
            
    numerical_virial=numeric_virial(natoms, deformed_positions, 
                                    extra_coords, deformation_tensor, 
                                    surropt, args)
            
    print(numerical_gradients/deformed_gradients)
    
    print(numerical_extra_gradients/extra_gradients)
    
    print(numerical_virial/deformed_virial)
    
    assert np.allclose(numerical_gradients/deformed_gradients, 1, atol=1e-2, rtol=0 )
    
    assert np.allclose(numerical_extra_gradients/extra_gradients, 1, atol=1e-2, rtol=0 )
    
    assert np.allclose(numerical_virial/deformed_virial, 1, atol=1e-2, rtol=0 ) 