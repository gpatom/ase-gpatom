#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 23 19:12:33 2023

@author: casper
"""

import pytest

from gpatom.beacon.str_gen import RandomBox, AtomsRelaxer
from gpatom.gpfp.prior import RepulsivePotential

from gpatom.hyperspatial_beacon.str_gen import HighDimRandomBox, HighDimAtomsRelaxer

from ase.calculators.emt import EMT
from ase.constraints import FixAtoms

from ase.io import read
import numpy as np

from ase import Atoms
from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.hyperspatial_beacon.gpfp.gp import HighDimGaussianProcess
from gpatom.hyperspatial_beacon.gpfp.prior import HighDimConstantPrior
from gpatom.hyperspatial_beacon.hyperspacebeacon import HighDimSurrogateOptimizer
from gpatom.hyperspatial_beacon.gpfp.fingerprint import HighDimFingerPrint
from gpatom.hyperspatial_beacon.gpfp.prior import HighDimRepulsivePotential

import os

def test_penalty():

    surropt=HighDimSurrogateOptimizer(fmax=0.05, 
                                      relax_steps=40,  
                                      after_steps=10,
                                      to_dims=3,
                                      write_surropt_trajs=True)
    surropt.set_world_center([5]*5, 5)
    
    
    rng=np.random.RandomState(2432)
    
    penalty_coords=rng.rand(1,2)
  
    strength=0.5
    
    HS_eng0, HS_deriv0 = surropt.HS_penalty(penalty_coords, strength)

   
    # assert that penalty force only acts on extra dimensions
    assert np.allclose(HS_deriv0[0][0:3], np.zeros(3))
    assert all ( abs(HS_deriv0[0][3::])>0 )
    
 
    # assert that the gradient is correct derivative of the energy
    dx=0.001
    analytical_force=HS_deriv0[0][3::]
    
    numerical_force=np.zeros(  (len(penalty_coords[0,:])) )  
    for i in range(2):
        penalty_coords_copy=penalty_coords.copy()
        penalty_coords_copy[0,i]+=dx
    
        HS_eng1, HS_deriv1 = surropt.HS_penalty(penalty_coords_copy, strength)    
    
        numerical_force[i]=  (HS_eng1 - HS_eng0)/dx

    assert  np.allclose(analytical_force , numerical_force, atol=1e-6,  rtol=1e-3) 
    #assert that gradients has the right right direction and is minimized at the world_center
    wc=np.array(surropt.world_center[3]).reshape( (1,1) )
 
    penalty_coords_plus=wc+dx
    HS_eng_plus, HS_deriv_plus = surropt.HS_penalty(penalty_coords_plus, strength)    
    
    penalty_coords_minus=wc-dx
    HS_eng_minus, HS_deriv_minus = surropt.HS_penalty(penalty_coords_minus, strength)  

    penalty_coords_wc=wc
    HS_eng_wc, HS_deriv_wc = surropt.HS_penalty(penalty_coords_wc, strength)    
    
    assert(HS_deriv_plus[0][3]>0)
    assert(HS_deriv_minus[0][3]<0)
    assert np.isclose(HS_deriv_wc[0][3],0)
    assert(HS_eng_wc<HS_eng_plus)
    assert(HS_eng_wc<HS_eng_minus)


def test_hyperspace_surrogate_optimization( ):
        
    atoms = Atoms(['Au'] * 5 + ['Cu'] * 3, positions=[[0., 0., 0.]] * 8)
    atoms.cell=[12,12,12]
    atoms.pbc = False
    atoms.center()

    atoms.positions[0,:]=[7.5,6,6]
    atoms.positions[7,:]=[4.5,6,6]
    
    constrained_atoms=[0,7]
    
    atoms.set_constraint(FixAtoms(constrained_atoms))

    r_rng = np.random.RandomState(34)
    
    s_rng = np.random.RandomState(2473)
    
    rattle_rng= np.random.RandomState(223)

    potential_lowD=RepulsivePotential(prefactor=10, potential_type='parabola')
    relaxer_lowD=AtomsRelaxer(calculator=potential_lowD)
    
    box=[(0., 10.), (0., 10.), (0., 10.)]
    rgen=RandomBox(atoms=atoms, box=box, 
                   covrad_inside=[False, False, False],
                   relaxer=relaxer_lowD,  rng=r_rng)
    

    potential_highD=HighDimRepulsivePotential(prefactor=10, potential_type='parabola')
    relaxer_highD=HighDimAtomsRelaxer(calculator=potential_highD)
    
    hd_box=[(2., 8.), (2., 8.), (2., 8.), (2., 8.)]
    sgen=HighDimRandomBox(atoms=atoms, box=hd_box, 
                          covrad_inside=[False, False, False, False], 
                          relaxer=relaxer_highD,  rng=s_rng)

    training_atoms=[]
    for i in range(5):
        atoms_i=rgen.get()
        atoms_i.calc=EMT()
        training_atoms.append(atoms_i)
    
    cycles=1
    surropt=HighDimSurrogateOptimizer(fmax=0.05, 
                                      cycles=cycles,
                                      relax_steps=20,  
                                      after_steps=10,
                                      to_dims=3,
                                      strength_array=np.array([0.5]),
                                      rattle_rng=rattle_rng,
                                      write_surropt_trajs=True)
    
    surropt.set_world_center([atoms.cell[0][0]/2]*4, 4)
    
    
    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1}  

    fp=HighDimFingerPrint(fp_args)
    
    gp_args=dict(prior=HighDimConstantPrior(constant=0), 
                 hp={'scale': 1000}, use_forces=True)

    gp=HighDimGaussianProcess(**gp_args)

    model=Model(gp=gp, fp=fp)
    
    model.add_data_points(training_atoms)


    test_atoms=sgen.get()
    assert hasattr(test_atoms, 'extra_coords')

    opt_atoms, success = surropt.relax(test_atoms, model, file_identifier='hs')

    
    # check that files are written
    assert( os.path.isfile('opt_hs.xyz') )
    
    trajectory=read('opt_hs.xyz', ':')
    
    # fractions are stored in opt_hs.xyz initial charges
    extra_coords=np.zeros((len(trajectory),len(atoms)))
    for i in range(len(trajectory)):
        extra_coords[i,:]=trajectory[i].get_initial_charges()
    
    
    # check that position constrained atoms havent moved
    first_frame=trajectory[0]
    last_frame=trajectory[-1]
    for atom_index in constrained_atoms:
        coord_start=first_frame.get_positions()[atom_index]
        coord_end=last_frame.get_positions()[atom_index]
        
        assert np.allclose(coord_start, coord_end)
        
    # check that extra_coords stayed zero at all steps
    assert np.allclose(extra_coords[:,0], np.zeros(len(trajectory)))
    assert np.allclose(extra_coords[:,7], np.zeros(len(trajectory)))

    #check that all atoms have left fourth dimension in the end
    assert np.allclose(extra_coords[-1,:], np.zeros(len(atoms)))
    
        
    # energy wont neccessarily be lower in the end state than the start state
    # with this algorithm , hence it wont be tested

