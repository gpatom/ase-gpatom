#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 00:31:40 2024

@author: casper
"""

import numpy as np
from ase.stress import (full_3x3_to_voigt_6_stress,
                        voigt_6_to_full_3x3_stress)
    
class UnitCellHandler:
    
    @staticmethod
    def deform_grad(original_cell, new_cell):
        return np.linalg.solve(original_cell, new_cell).T    
    
    @staticmethod
    def atoms_deformed_to_real(atoms, deformation_tensor, deformed_positions, original_cell, cell_factor):
          
        new_deform_grad = deformation_tensor / cell_factor
        
        new_cell=original_cell @ new_deform_grad.T
        
        atoms.set_cell(new_cell, scale_atoms=True) 
        
        real_positions=deformed_positions @ new_deform_grad.T
        
        atoms.set_positions(real_positions)
        
        return atoms
    
    @staticmethod
    def atoms_real_to_deformed(atoms_real, original_cell, cell_factor):
        
        deformation_gradient = UnitCellHandler.deform_grad(original_cell, atoms_real.cell)
        
        deformation_tensor = cell_factor * deformation_gradient
        
        deformed_positions = np.linalg.solve(deformation_gradient,   
                                             atoms_real.positions.T).T   
        
        return deformation_tensor, deformed_positions    
    
    @staticmethod 
    def forces_real_to_deformed(atoms_real, atoms_forces, stress, original_cell, cell_factor):
        cur_deform_grad=UnitCellHandler.deform_grad(original_cell, atoms_real.cell) 
        
        deformed_forces = atoms_forces @ cur_deform_grad

        volume = atoms_real.get_volume()

        negative_virial = volume * (voigt_6_to_full_3x3_stress(stress) )      
        
        deformed_virial = np.linalg.solve(cur_deform_grad, negative_virial.T).T     
        
        deformed_virial=deformed_virial / cell_factor
        
        return deformed_forces, deformed_virial
    
    
    @staticmethod  # for testing
    def forces_deformed_to_real( deformation_tensor, deformed_forces, deformed_virial, atoms_real, cell_factor):
        
        new_deform_grad = deformation_tensor / cell_factor
        
        forces = np.linalg.solve(new_deform_grad, deformed_forces.T).T 
        
        
        volume=atoms_real.get_volume()
        
        volume_divided_negative_vririal=deformed_virial*cell_factor/volume
        
        stress = volume_divided_negative_vririal @ new_deform_grad.T
        
        stress=full_3x3_to_voigt_6_stress(stress)
        
        return forces, stress
    
    
    @staticmethod 
    def apply_cell_mask(deformed_virial, opt_cell_mask):
                
        mask = voigt_6_to_full_3x3_stress(opt_cell_mask)
            
        deformed_virial *= mask
        
        return deformed_virial