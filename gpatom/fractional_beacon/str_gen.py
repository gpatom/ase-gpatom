#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 18:09:36 2024

@author: casper
"""
class RandomStructureAndFractionGenerator():
    
    def __init__(self, structure_generator, fraction_generator, relaxer=None):
    
        self.sgen=structure_generator
        self.fgen=fraction_generator
        self.relaxer=relaxer
    
    def get(self):
        
        atoms = self.sgen.get()
        
        fractions=self.fgen.get_fractions(atoms)
        atoms.fractions=fractions
        
        if self.relaxer is not None:
            atoms=self.relaxer.run(atoms)
    
        return  atoms
    
    
    
class HighDimRandomStructureAndFractionGenerator():
    
    def __init__(self, structure_generator, fraction_generator, relaxer=None):
    
        self.sgen=structure_generator
        self.fgen=fraction_generator
        self.relaxer=relaxer
    
    def get(self):
        
        atoms = self.sgen.get()
        
        fractions=self.fgen.get_fractions(atoms)
        atoms.fractions=fractions
        
        if self.relaxer is not None:
            extra_coords=atoms.extra_coords
            atoms, extra_coords=self.relaxer.run(atoms, 
                                                 self.sgen.world_center, 
                                                 extra_coords)
            atoms.extra_coords=extra_coords

        return  atoms