import numpy as np
from gpatom.gpfp.fingerprint import (RadialFP, RadialFPCalculator,
                                     RadialFPGradientCalculator,
                                     RadialAngularFP, AngularFPCalculator,
                                     AngularFPGradientCalculator,
                                     AtomPairs, AtomTriples,
                                     FPElements, FPFactors)


from gpatom.hyperspatial_beacon.hyperspacebeacon import HSParamsHandler

from gpatom.hyperspatial_beacon.gpfp.fingerprint import (HighDimAtomPairs, 
                                                         HighDimAtomTriples)

from gpatom.fractional_beacon.icebeacon import ICEInfo

class PartialFingerPrint:
    
    
    def __init__(self, fp_args={}, 
                 fractional_elements=None, 
                 angular=True,
                 calc_coord_gradients=True,
                 calc_strain=False):
        
        self.fp_args=fp_args
        self.fractional_elements=fractional_elements
        self.calc_coord_gradients=calc_coord_gradients
        self.calc_strain=calc_strain
        
        if angular:
            self.fp_class=PartialRadAngFP
        else:
            self.fp_class=PartialFP
    
    def get(self, atoms, fractions=None):   
                        

            fp = self.fp_class(atoms=atoms, 
                               fractions=fractions,
                               calc_coord_gradients=self.calc_coord_gradients,
                               calc_strain=self.calc_strain,
                               **self.fp_args)
 
            return fp



class PartialFP(RadialFP):

    def __init__(self, atoms=None, 
                 fractions=None,
                 calc_coord_gradients=True,
                 calc_strain=False,
                 **kwargs):

        '''
        fractions: array(Natoms, Nelements) fraction for each atom
        '''
        
        self.check_cell(atoms)
        self.atoms = atoms.copy()
        self.atoms.wrap()
    
        self.dims=3
        
        default_parameters = {'r_cutoff': 8.0,
                              'r_delta': 0.4,
                              'r_nbins': 200}
        
        self.params = default_parameters.copy()        
        self.params.update(kwargs)
        
        fpparams = dict(cutoff=self.params['r_cutoff'],
                        width=self.params['r_delta'],
                        nbins=self.params['r_nbins'])
        

        self.pairs = AtomPairs(self.atoms, self.params['r_cutoff'])

        self.elements=ICEInfo.get_sorted_elements(self.atoms)
        self.output_columns=len(self.elements)

        if fractions is None:
            fractions = FPFactors.get_element_vectors(self.atoms, 
                                                      self.elements)

        self.fractions=fractions
        
        self.ei = [list(FPElements.get_elementset(self.atoms)).index(e) # element indices
                   for e in self.elements]
                
        factors = FPFactors.get_factors_for_pairs(self.pairs,
                                                  self.fractions)  
        
        (gaussians,
         fingerprint_ij)=RadialFPCalculator.get_gaussians(self.pairs, 
                                                          **fpparams)
        
        grads_ij=RadialFPGradientCalculator.get_grad_terms(gaussians,
                                                           self.pairs,
                                                           **fpparams)        
                
        self.rho_R = RadialFPCalculator.get_fp(fingerprint_ij, 
                                               factors, self.pairs,
                                               self.params['r_nbins'])
        
        self.vector = self.rho_R.flatten()

        self.gradients = (RadialFPGradientCalculator.
                          get_gradients(grads_ij, factors,  self.natoms, 
                                       self.pairs, self.params['r_nbins'])
                          if calc_coord_gradients else None)

        self.strain = (RadialFPGradientCalculator.
                       get_strain(grads_ij, factors, self.pairs, 
                                  self.params['r_nbins'])
                       if calc_strain else None)

        
        self.frac_gradients=(RadialFPFractionGradients.
                             get_frac_grads(self.natoms,
                                            fingerprint_ij,
                                            self.pairs,
                                            self.fractions,
                                            self.ei,
                                            nbins=self.params['r_nbins']))
        
    @property
    def natoms(self):
        return len(self.atoms)

    def reduce_frac_gradients(self):
        return self.frac_gradients.reshape(self.natoms, -1, self.output_columns)



class RadialFPFractionGradients(RadialFPCalculator):

    @classmethod
    def get_frac_grads(cls, natoms, fingerprint_ij, 
                       pairs, fractions, ei, nbins):
        
        nfelements=len(ei)

        f_gradients = np.zeros([natoms, pairs.elem.nelem, pairs.elem.nelem,
                                nbins, nfelements])
           
        if pairs.empty:
            ncombis=cls.get_ncombis(pairs.elem.nelem)
            return np.zeros([natoms, ncombis, nbins, nfelements])

        factors = cls.get_factor_product_gradients_for_pairs(natoms,  
                                                             pairs,
                                                             fractions)

        all_i=pairs.indices[:,0]
        all_j=pairs.indices[:,1]
        
        f_gradients_i = np.zeros((natoms, nfelements, nbins))
        f_gradients_j = np.zeros((natoms, nfelements, nbins))
        for a in range(natoms):
            
            i_mask=(all_i==a)
            j_mask=(all_j==a)
        
            f_gradients_i[a]=np.einsum('ij,il->jl',
                                       factors[i_mask,0],
                                       fingerprint_ij[i_mask],                              
                                       optimize=False)
            
            f_gradients_j[a]=np.einsum('ij,il->jl',
                                       factors[j_mask,1],
                                       fingerprint_ij[j_mask],                              
                                       optimize=False)

        f_gradients[:, ei, :, :,ei] += f_gradients_i
        f_gradients[:, :, ei, :,ei] += f_gradients_j
        
        f_gradients+=np.transpose(f_gradients, axes=(0, 2, 1, 3, 4))
        
        diagonal_mask = np.eye(pairs.elem.nelem, dtype=bool)
        f_gradients[:, diagonal_mask,:,:]/=2            
        triu_indices = np.triu_indices(pairs.elem.nelem)
        f_gradients=f_gradients[:,triu_indices[0], triu_indices[1],:,:]
        
        return f_gradients


    @staticmethod
    def get_factor_product_gradients_for_pairs(natoms, pairs, fractions):

        factors = np.zeros((len(pairs.indices), 2, pairs.elem.nelem))

        q = fractions
        
        i = pairs.indices[:, 0]
        j = pairs.indices[:, 1]

        factors[:, 0, :] =  q[j,:]
        factors[:, 1, :] =  q[i,:]

        return factors


class PartialRadAngFP(PartialFP, RadialAngularFP):

    def __init__(self, atoms=None, 
                 fractions=None,
                 calc_coord_gradients=True,
                 calc_strain=False,
                 **kwargs):
        
        '''
        fractions: array(Natoms, Nelements) fraction for each atom
        '''
        
        PartialFP.__init__(self, atoms, fractions,
                           calc_coord_gradients=calc_coord_gradients,
                           calc_strain=calc_strain,
                           **kwargs)

        default_parameters = {'r_cutoff': 8.0,
                              'r_delta': 0.4,
                              'r_nbins': 200,
                              'a_cutoff': 4.0,
                              'a_delta': 0.4,
                              'a_nbins': 100,
                              'gamma': 0.5,
                              'aweight': 1.0}

        self.params = default_parameters.copy()
        self.params.update(kwargs)

        fpparams=dict(width=self.params['a_delta'],
                      nbins=self.params['a_nbins'],
                      aweight=self.params['aweight'],
                      cutoff=self.params['a_cutoff'],
                      gamma=self.params['gamma'])

        assert self.params['r_cutoff'] >= self.params['a_cutoff']

        self.triples = AtomTriples(self.atoms,
                                   cutoff=self.params['a_cutoff'])

        factors= FPFactors.get_factors_for_triples(self.triples, 
                                                   self.fractions)
        
        (angle_gaussians,
         fingerprint_ijk)=AngularFPCalculator.get_angle_gaussians(self.triples,
                                                                  **fpparams)
                
        (grads_ij, 
         grads_ik, 
         grads_jk) = AngularFPGradientCalculator.get_grad_terms(angle_gaussians,
                                                                self.triples,
                                                                **fpparams)

        self.rho_a = AngularFPCalculator.get_fp(fingerprint_ijk, 
                                                factors, self.triples, 
                                                self.params['a_nbins'])
        
        self.vector = np.concatenate((self.rho_R.flatten(),
                                      self.rho_a.flatten()), axis=None)


        self.anglegradients = (AngularFPGradientCalculator.
                               get_gradients(grads_ij, grads_ik, grads_jk, 
                                             factors, self.natoms, self.triples,
                                             self.params['a_nbins'])
                               if calc_coord_gradients else None)

        
        self.anglestrain = (AngularFPGradientCalculator.
                            get_strain(grads_ij, grads_ik, grads_jk, factors, 
                                      self.triples, self.params['a_nbins'])
                            if calc_strain else None)
        
        self.frac_gradients_angles=(AngularFPFractionGradients.
                                    get_frac_grads(self.natoms,
                                                   fingerprint_ijk,
                                                   self.triples,
                                                   self.fractions, 
                                                   self.ei,
                                                   self.params['a_nbins']))
                                  

    def reduce_frac_gradients(self):

        return np.concatenate((self.frac_gradients.reshape(self.natoms, -1, self.output_columns),
                               self.frac_gradients_angles.reshape(self.natoms, -1, self.output_columns)),
                              axis=1)


class AngularFPFractionGradients(AngularFPCalculator):

    @classmethod
    def get_frac_grads(cls, natoms, fingerprint_ijk, triples, 
                       fractions, ei, nbins):
        
        nfelements=len(ei)

        f_gradients = np.zeros([natoms,                 
                                triples.elem.nelem,
                                triples.elem.nelem,
                                triples.elem.nelem,
                                nbins,
                                nfelements])

        if triples.empty:
            ncombis=cls.get_ncombis(triples.elem.nelem)
            return np.zeros([natoms, ncombis, nbins, nfelements])

        factors = cls.get_factor_product_gradients_for_triples(natoms,  
                                                               triples,
                                                               fractions)

        all_i=triples.indices[:,0]
        all_j=triples.indices[:,1]
        all_k=triples.indices[:,2]
                
        f_gradients_i = np.zeros((natoms, nfelements, nfelements, nbins))
        f_gradients_j = np.zeros((natoms, nfelements, nfelements, nbins))
        f_gradients_k = np.zeros((natoms, nfelements, nfelements, nbins))
        for a in range(natoms):
            
            i_mask=(all_i==a)
            j_mask=(all_j==a)
            k_mask=(all_k==a)
        
            f_gradients_i[a]=np.einsum('ijk,il->jkl',
                                       factors[i_mask,0],
                                       fingerprint_ijk[i_mask],
                                       optimize=False)

            f_gradients_j[a]=np.einsum('ijk,il->jkl',
                                       factors[j_mask,1],   
                                       fingerprint_ijk[j_mask],
                                       optimize=False)
            
            f_gradients_k[a]=np.einsum('ijk,il->jkl',
                                       factors[k_mask,2],
                                       fingerprint_ijk[k_mask],
                                       optimize=False)

        f_gradients[:, ei, :, :,:,ei] += f_gradients_i
        f_gradients[:, :, ei, :,:,ei] += f_gradients_j
        f_gradients[:, :, :, ei,:,ei] += f_gradients_k
        
        f_gradients+=np.transpose(f_gradients, axes=(0, 1, 3, 2, 4, 5))
        
        diagonal_mask = np.eye(triples.elem.nelem, dtype=bool)
        f_gradients[:,:, diagonal_mask,:,:]/=2
        triu_indices = np.triu_indices(triples.elem.nelem)
        f_gradients=f_gradients[:, :,triu_indices[0], triu_indices[1],:,:]
                
        return f_gradients
        

    @staticmethod
    def get_factor_product_gradients_for_triples(natoms, triples, fractions):

        factors = np.zeros((len(triples.indices), 3,
                            triples.elem.nelem, triples.elem.nelem))
        
        q = fractions
        
        i = triples.indices[:, 0]
        j = triples.indices[:, 1]
        k = triples.indices[:, 2]

        factors[:, 0, :, :] = np.einsum('pa,pb->pab', q[j,:] , q[k,:],optimize=False)
        factors[:, 1, :, :] = np.einsum('pa,pb->pab', q[i,:] , q[k,:],optimize=False)
        factors[:, 2, :, :] = np.einsum('pa,pb->pab', q[i,:] , q[j,:],optimize=False)

        return factors


class HighDimPartialFingerPrint(PartialFingerPrint):
    
    
    def __init__(self, fp_args={}, 
                 angular=True,
                 calc_coord_gradients=True,
                 calc_strain=False):
        
        self.fp_args=fp_args
        self.calc_coord_gradients=calc_coord_gradients
        self.calc_strain=calc_strain
        
        if angular:
            self.fp_class=HighDimPartialRadAngFP
        else:
            self.fp_class=HighDimPartialFP
    
    def get(self, atoms, extra_coords=None, fractions=None):   
                
            fp = self.fp_class(atoms=atoms, 
                               extra_coords=extra_coords,
                               fractions=fractions,
                               calc_coord_gradients=self.calc_coord_gradients,
                               calc_strain=self.calc_strain,
                               **self.fp_args)
 
            return fp
    
    
class HighDimPartialFP(PartialFP):

    def __init__(self, atoms=None,
                 extra_coords=None, 
                 fractions=None,
                 calc_coord_gradients=True,
                 calc_strain=False,
                 **kwargs):

        '''
        fractions: array(Natoms, Nelements) fraction for each atom
        '''
        
        self.check_cell(atoms)
        self.atoms = atoms.copy()
        self.atoms.wrap()
        

        default_parameters = {'r_cutoff': 8.0,
                              'r_delta': 0.4,
                              'r_nbins': 200}
        
        self.params = default_parameters.copy()        
        self.params.update(kwargs)
        
        fpparams = dict(cutoff=self.params['r_cutoff'],
                        width=self.params['r_delta'],
                        nbins=self.params['r_nbins'])


        self.dims = HSParamsHandler.get_dimension_from_exra_coords(extra_coords)
        self.extra_coords=extra_coords

        self.pairs = HighDimAtomPairs(self.atoms,
                                      extra_coords,
                                      self.params['r_cutoff'])


        self.elements=ICEInfo.get_sorted_elements(atoms)
        self.output_columns=len(self.elements)


        if fractions is None:
            fractions = FPFactors.get_element_vectors(atoms, 
                                                      self.elements)

        self.fractions=fractions

        self.ei = [list(FPElements.get_elementset(self.atoms)).index(e) # element indices
                   for e in self.elements]
        
        factors = FPFactors.get_factors_for_pairs(self.pairs,
                                                  self.fractions)  

        
        (gaussians,
         fingerprint_ij)=RadialFPCalculator.get_gaussians(self.pairs, 
                                                          **fpparams)
        
        grads_ij=RadialFPGradientCalculator.get_grad_terms(gaussians,
                                                           self.pairs,
                                                           **fpparams)        
                
        self.rho_R = RadialFPCalculator.get_fp(fingerprint_ij, 
                                               factors, self.pairs,
                                               self.params['r_nbins'])
        
        self.vector = self.rho_R.flatten()

        self.gradients = (RadialFPGradientCalculator.
                          get_gradients(grads_ij, factors, self.natoms, 
                                       self.pairs, self.params['r_nbins'],
                                       dimensions=self.dims)
                          if calc_coord_gradients else None)

        self.strain = (RadialFPGradientCalculator.
                       get_strain(grads_ij, factors, self.pairs, 
                                  self.params['r_nbins'])[:,:,:3,:3]
                       if calc_strain else None)   
        
        self.frac_gradients=(RadialFPFractionGradients.
                             get_frac_grads(self.natoms,
                                            fingerprint_ij,
                                            self.pairs,
                                            self.fractions,
                                            self.ei,
                                            self.params['r_nbins']))

    def reduce_coord_gradients(self):
        '''
        Reshape gradients by flattening the element-to-element
        contributions.
        '''
        return self.gradients.reshape(self.natoms, -1, self.dims) 


    def reduce_frac_gradients(self):
                
        return self.frac_gradients.reshape(self.natoms, -1, self.output_columns)




class HighDimPartialRadAngFP(HighDimPartialFP, PartialRadAngFP):

    def __init__(self, atoms=None, 
                 extra_coords=None, 
                 fractions=None,
                 calc_coord_gradients=True,
                 calc_strain=False,
                 **kwargs):
        
        '''
        fractions: array(Natoms, Nelements) fraction for each atom
        '''

        HighDimPartialFP.__init__(self, atoms, 
                                  extra_coords=extra_coords,
                                  fractions=fractions,
                                  calc_coord_gradients=calc_coord_gradients,
                                  calc_strain=calc_strain,
                                  **kwargs)

        default_parameters = {'r_cutoff': 8.0,
                              'r_delta': 0.4,
                              'r_nbins': 200,
                              'a_cutoff': 4.0,
                              'a_delta': 0.4,
                              'a_nbins': 100,
                              'gamma': 0.5,
                              'aweight': 1.0}

        self.params = default_parameters.copy()
        self.params.update(kwargs)

        fpparams=dict(width=self.params['a_delta'],
                      nbins=self.params['a_nbins'],
                      aweight=self.params['aweight'],
                      cutoff=self.params['a_cutoff'],
                      gamma=self.params['gamma'])

        assert self.params['r_cutoff'] >= self.params['a_cutoff']

        self.extra_coords=extra_coords

        self.triples = HighDimAtomTriples(self.atoms,          
                                          extra_coords,
                                          cutoff=self.params['a_cutoff'])

        factors= FPFactors.get_factors_for_triples(self.triples, 
                                                   self.fractions)
        

        (angle_gaussians,
         fingerprint_ijk)=AngularFPCalculator.get_angle_gaussians(self.triples,
                                                                  **fpparams)
                
        (grads_ij, 
         grads_ik, 
         grads_jk) = AngularFPGradientCalculator.get_grad_terms(angle_gaussians,
                                                                self.triples,
                                                                **fpparams)

        self.rho_a = AngularFPCalculator.get_fp(fingerprint_ijk, 
                                                factors, self.triples, 
                                                self.params['a_nbins'])
        
        self.vector = np.concatenate((self.rho_R.flatten(),
                                      self.rho_a.flatten()), axis=None)


        self.anglegradients = (AngularFPGradientCalculator.
                               get_gradients(grads_ij, grads_ik, grads_jk, 
                                             factors, self.natoms, self.triples,
                                             self.params['a_nbins'],
                                             dimensions=self.dims)
                               if calc_coord_gradients else None)

        
        self.anglestrain = (AngularFPGradientCalculator.
                            get_strain(grads_ij, grads_ik, grads_jk, 
                                       factors, self.triples, 
                                       self.params['a_nbins'])[:,:,:3,:3]
                            if calc_strain else None)
        
        
        self.frac_gradients_angles=(AngularFPFractionGradients.
                                    get_frac_grads(self.natoms,
                                                   fingerprint_ijk,
                                                   self.triples,
                                                   self.fractions,
                                                   self.ei,
                                                   self.params['a_nbins']))    
        
    def reduce_coord_gradients(self):
        '''
        Reshape gradients by flattening the element-to-element
        contributions and all angles, and concatenate those arrays.
        '''
        
        return np.concatenate((self.gradients.reshape(self.natoms, -1, self.dims),
                               self.anglegradients.reshape(self.natoms, -1, self.dims)),
                              axis=1)


    def reduce_frac_gradients(self):

        return np.concatenate((self.frac_gradients.reshape(self.natoms, -1, self.output_columns),
                               self.frac_gradients_angles.reshape(self.natoms, -1, self.output_columns)),
                              axis=1)
        