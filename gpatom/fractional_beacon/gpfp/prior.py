#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  7 13:29:24 2023

@author: casper
"""

import numpy as np
from ase.data import covalent_radii, atomic_numbers
from gpatom.gpfp.prior import ConstantPrior
from gpatom.hyperspatial_beacon.gpfp.prior import (HighDimCalculatorPrior, 
                                                   CalculatorPrior, 
                                                   RepulsivePotential)
from gpatom.hyperspatial_beacon.gpfp.prior import (HighDimConstantPrior, 
                                                   HighDimRepulsivePotential) 
from gpatom.fractional_beacon.icebeacon import ICEInfo
from ase.neighborlist import NeighborList




class WeightedConstantPrior(ConstantPrior):
    
    def get_frac_grads(self, x):
        frac_grads=np.zeros(np.shape(x.fractions))
        return frac_grads
    
class WeightedHighDimConstantPrior(WeightedConstantPrior, HighDimConstantPrior):
    
    def get_frac_grads(self, x):
        frac_grads=np.zeros(np.shape(x.fractions))
        return frac_grads
    
    
    
class WeightedCalculatorPrior(CalculatorPrior):
        
    def get_atoms(self,x):    
        atoms=x.atoms.copy()
        atoms.fractions=x.fractions.copy()
        return atoms
    
    def get_frac_grads(self, x):
        frac_grads=self.calculator.results['frac_grads']
        return frac_grads
    
    
class WeightedHighDimCalculatorPrior(WeightedCalculatorPrior, HighDimCalculatorPrior):
    
    def __init__(self, calculator, constant, **kwargs):
        HighDimCalculatorPrior.__init__(self, calculator, constant, **kwargs)
    
    def get_atoms(self,x):    
        atoms=x.atoms.copy()
    
        extra_coords=x.extra_coords
        atoms.extra_coords=extra_coords
        
        dims=x.dims
        atoms.dims=dims
        
        atoms.fractions=x.fractions.copy()
        
        return atoms
    
    def get_frac_grads(self,x):
        frac_grads=self.calculator.results['frac_grads']
        return frac_grads
    
    
    
class WeightedRepulsivePotential(RepulsivePotential):
    ''' Repulsive potential of the form
    sum_ij q_iq_j(0.7 * (Ri + Rj) / rij)**12

    where Ri and Rj are the covalent radii of atoms
    '''
        
    implemented_properties = ['energy', 'forces', 'frac_grads', 'stress']

    default_parameters = {'prefactor': 1,'rc_factor': 0.9, 
                          'potential_type': 'LJ', 'exponent': 2,
                          'extrapotentials': None,
                          'elements': None, 'min_radius': False}    
    
    nolabel = True    
    
    
    def get_covrads(self, elements):
        covrads = np.array([covalent_radii[atomic_numbers[e]] 
                            for e in elements])
                
        covrads*=self.parameters.rc_factor
        
        if self.parameters.min_radius is None:
            min_rad=min(covrads)
        else:
            min_rad=self.parameters.min_radius
             
        return covrads, min_rad
    
    
    def get_existence(self, fracs):
        existence = np.sum(fracs, axis=1)
        return existence  
    
    
    def get_weighted_covrad(self, elements, fracs, covrads, min_rad):     
        covrads=covrads.reshape(len(elements))

        weighted_rad=np.sum(fracs*np.array(covrads), axis=1)
        
        existence=self.get_existence(fracs)
    
        rc=weighted_rad  + (1-existence)*min_rad
        
        return rc
        
    
    def get_parameters(self, atoms):
       
        prefactor = self.parameters.prefactor 
       
        weights=atoms.fractions.copy()
            
        elements=ICEInfo.get_sorted_elements(atoms)
        
        covrads, min_rad=self.get_covrads(elements)

        rc=self.get_weighted_covrad(elements, weights, covrads, min_rad)
        
        exp=self.parameters.exponent 
        
        return rc, prefactor, exp
        
    
    def get_energy_and_derivatives(self, atoms, rc, prefactor, exp, neighbor_list):

        energy, forces = self.setup_energy_forces(atoms)  
        
        elements=ICEInfo.get_sorted_elements(atoms)
        
        existence=self.get_existence(atoms.fractions.copy())
        
        frac_grads=np.zeros(  (len(atoms), len(elements))  )
        
        stress = np.zeros((3, 3))

        for a1 in range(len(atoms)):
            
            neighbors, d = self.get_distance_info(atoms, a1, neighbor_list)
            
            if len(neighbors)>0:     
                
                u, potential, dudr, dudq = self.get_potential(a1, rc, prefactor, exp, d, 
                                                              neighbors, existence, elements)   

                energy = self.update_energy(energy, potential)
                
                forces = self.update_forces(forces, dudr, a1, neighbors)      # its gradienst not forces
                
                frac_grads=self.update_frac_grads(frac_grads, u, dudq, existence, a1, neighbors) 
                
                stress = self.update_stress(stress, dudr, d)
            
        self.results['frac_grads']=frac_grads
        return energy, forces, stress

    
    
    def get_potential(self, atom_index, rc, prefactor, exp, d, 
                      neighbors, existence, elements):        
        
        r, crs = self.radial_info(d, rc, atom_index, neighbors)  

        U, dUdx = self.get_x_potential(prefactor, exp, r, crs)
        
        ex_nbs=np.array( [   existence[n] for n in neighbors   ]  )
        
        ex_combined=existence[atom_index]*ex_nbs
        
        dUdq=np.zeros(  (len(neighbors), len(elements))  )   
        
        covrads, min_rad = self.get_covrads(elements)
        
        for e in range(len(elements)):
            
            dUdq[:,e]=ex_nbs*U-ex_combined*dUdx*r/(crs**2)*(covrads[e]-min_rad)
            
    
        dUdr = ex_combined * dUdx * 1/crs
    
        cartesian_dUdr = self.cartesian_conversion(dUdr, d, r)  
    
        potential=U*ex_combined
        
        return U, potential, -cartesian_dUdr , dUdq
    


    def update_frac_grads(self, frac_grads, u, dudq, existence, atom_index, neighbors):
    
        frac_grads[atom_index] += dudq.sum(axis=0)

        for a2, g2, u2 in zip(neighbors, dudq, u):            
            frac_grads[a2] += g2 + u2*(existence[atom_index]-existence[a2])
            
        return frac_grads
        
    
    
class WeightedHighDimRepulsivePotential(WeightedRepulsivePotential, 
                                        HighDimRepulsivePotential):
    
    implemented_properties = ['energy', 'forces', 'frac_grads', 'stress']

    default_parameters = {'prefactor': 1,'rc_factor': 0.9, 
                          'potential_type': 'LJ', 'exponent': 2,
                          'extrapotentials': None,
                          'elements': None, 'min_radius': False}    
    
    nolabel = True   


  
  
class GhostExtraPotential(HighDimRepulsivePotential):
    '''
    a prior on atoms with no existence to make them fllow the other atoms.
    the potential doesnt affect energies as its only moving vacuum around
    also stress is inaffected as vacuum should not influence the unit cell.
    Hence only forces are nonzero
    '''  
    def __init__(self, strength=0.01, onset=1e-5, rc=None, cutoff=3):
        self.strength=strength
        self.onset=onset
        self.rc=rc
        self.cutoff=cutoff
        
    def potential(self, atoms):
        return 0.0
        
    def forces(self, atoms):
        
        forces=np.zeros( (len(atoms), atoms.dims) )
        
        existence=np.sum(atoms.fractions,axis=1)
        
        rc = self.get_rc(atoms)
        
        neighbor_list = NeighborList(self.cutoff*np.array(rc), self_interaction=False, bothways=True)   
        neighbor_list.update(atoms)
        
        for atom_index in range(len(atoms)):
            
            if existence[atom_index]<self.onset:
                neighbors, d = self.get_distance_info(atoms, atom_index, neighbor_list)
                coeff=(self.onset-existence[atom_index])/self.onset
                
                if len(neighbors)>0:     
                                       
                    derivative=self.get_ghost_derivative(atom_index, rc, d, neighbors, coeff)
              
                    forces[atom_index] = derivative.sum(axis=0)

        return forces  # its actually just the derivatives, not the forces
        
        
    def stress(self, atoms):
        return np.zeros(6)
        
        
    def get_ghost_derivative(self, atom_index, rc, d, neighbors, coeff):
        
        r2 = (d**2).sum(1)
        r=np.sqrt(r2)
        crs=rc[atom_index]+np.array( [   rc[n] for n in neighbors   ]  )
        
        r_min= crs /(2**(1/6))
        
        LJ_derivative= coeff*self.strength*(24/r)* ((r_min/r)**6-2*(r_min/r)**12) 
            
        r_block=np.tile(r,(len(d[0,:]),1)).T
        
        polar_to_cart=d/r_block
        LJ_derivative=LJ_derivative[:,np.newaxis]*polar_to_cart
        
        return LJ_derivative
    
    def get_rc(self, atoms):
        if self.rc is None:
            rc = [ covalent_radii[atoms[i].number]  for i in range(len(atoms)) ]
        else:
            
            if type(self.rc)==list:
                rc=self.rc
            else:
                rc=np.ones(len(atoms))*self.rc
        
        return rc
    
    