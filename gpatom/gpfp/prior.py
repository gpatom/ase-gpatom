import numpy as np
from scipy.linalg import cho_solve
from ase.calculators.calculator import Calculator, all_changes
from ase.neighborlist import NeighborList
from ase.data import covalent_radii
from ase.constraints import FixAtoms

from ase.stress import (full_3x3_to_voigt_6_stress,
                        voigt_6_to_full_3x3_stress)

# each prior must implement a method called potential taking a fingerprint, x 
# with an atoms object attached x.atoms

class ConstantPrior:
    '''Constant prior, with energy = constant and zero forces

    Parameters:

    constant: energy value for the constant.
    '''

    def __init__(self, constant, **kwargs):
        self.constant = constant

    def potential(self, x):
        d = len(x.atoms) * 3  # number of forces
        output = np.zeros(d + 1)
        output[0] = self.constant
        return output
    
    def potential_with_stress(self, x):
        d = len(x.atoms) * 3
        output = np.zeros(1 + d + 9)
        output[0]=self.constant
        return output
        
    def get_stress(self, x):
        stress=np.array([0., 0., 0., 0., 0., 0.])
        return stress

    def set_constant(self, constant):
        self.constant = constant

    def get_contribution_vector(self, X):
        self.set_constant(1.)
        return np.hstack([self.potential(x) for x in X])

    def update(self, X, Y, L, use_forces):
        """Update the constant to maximize the marginal likelihood.

        The optimization problem:
        m = argmax [-1/2 (y-m).T K^-1(y-m)]

        can be turned into an algebraic problem
        m = [ u.T K^-1 y]/[u.T K^-1 u]

        where u is the constant prior with energy 1 (eV).

        parameters:
        ------------
        X: training data
        Y: training targets
        L: Cholesky factor of the kernel """


        if use_forces:
            u=self.get_contribution_vector(X)
        else:
            u=np.ones(len(X))
            
        w = cho_solve((L, True), u, check_finite=False)
  
        m = np.dot(w, np.array(Y).flatten()) / np.dot(w, u)   
        
        self.set_constant(m)


       


class CalculatorPrior:

    '''CalculatorPrior object, allows the user to
    use another calculator as prior function instead of the
    default constant.

    The form of prior is

    E_p(x) = m*u + E_c(x)

    where m is constant (energy), u is array with 1's for energy
    and 0 for force components, E_c(x) is the calculator
    potential.

    Parameters:

    calculator: one of ASE's calculators
    **kwargs: arguments passed to ASE parent calculator
    '''

    def __init__(self, calculator, constant=0, **kwargs):

        self.calculator = calculator
        self.constant = constant 

    
    def potential(self, x):

        atoms=self.get_atoms(x)
        output_size=self.get_output_size(atoms)
        output=np.zeros(output_size)
    
        self.calculator.results.clear()
    
        atoms.calc = self.calculator

        output[0] = atoms.get_potential_energy() + self.constant

        output[1:] = -atoms.get_forces().reshape(-1)

        return output
    
    def potential_with_stress(self, x):
        
        atoms=self.get_atoms(x)
        output_size=self.get_output_size(atoms)
        output=np.zeros(output_size+9)
        
        atoms_output=self.potential(x)
    
        cell_output=self.get_stress(x)
        cell_output=voigt_6_to_full_3x3_stress(cell_output)
        
        output[0:-9]=atoms_output
        output[-9:]=cell_output.flatten()
        return output
    
    def get_stress(self, x):
        stress = self.calculator.results['stress']
        return stress
        
    def set_constant(self, constant):
        self.constant = constant    
        
    def get_atoms(self,x):
        atoms=x.atoms.copy()
        return atoms

    def get_output_size(self, atoms):
        return 1 + len(atoms) * 3
   
    def get_contribution_vector(self, X):
        self.set_constant(1.)
        return np.hstack([ConstantPrior.potential(self, x) for x in X])

    def update(self, X, Y, L, use_forces):
        """Update the constant to maximize the marginal likelihood.

        The optimization problem:
        m = argmax [-1/2 (y-m-Ec).T K^-1(y-m-Ec)]

        can be turned into an algebraic problem
        m = [ u.T K^-1 (y - Ec)]/[u.T K^-1 u]

        where u is the constant prior with energy 1 (eV),
        and Ec is the calculator prior.

        parameters:
        ------------
        X: training parameters
        Y: training targets
        L: Cholesky factor of the kernel """

        if use_forces:
            u=self.get_contribution_vector(X)
            pv=np.hstack([self.potential(x) for x in X])
        else:
            u=np.ones(len(X))
            pv=np.hstack([self.potential(x)[0] for x in X])
                    
        E_calcprior = (pv - u).flatten()
        
        # w = K\u
        w = cho_solve((L, True), u, check_finite=False)   

        # Set constant
        m = np.dot(w, (np.array(Y).flatten() - E_calcprior)) / np.dot(w, u)
        
        self.set_constant(m)


   
class RepulsivePotential(Calculator):
    ''' Repulsive potential of the form
    sum_ij (0.7 * (Ri + Rj) / rij)**12

    where Ri and Rj are the covalent radii of atoms
    '''

    implemented_properties = ['energy', 'forces', 'stress']

    default_parameters = {'prefactor': 1,'rc': 0.9, 
                          'potential_type': 'LJ', 'exponent': 2, 
                          'extrapotentials': None}    
    
    nolabel = True

    def calculate(self, atoms=None,
                  properties=['energy', 'forces', 'stress'],
                  system_changes=all_changes):
        
        Calculator.calculate(self, atoms, properties, system_changes)
        
        # get parameters
        rc, prefactor, exp = self.get_parameters(atoms)

        neighbor_list = NeighborList(np.array(rc), skin=0,
                                     self_interaction=False)     
        neighbor_list.update(atoms)

        energy, forces, stress = self.get_energy_and_derivatives(atoms, rc, prefactor, exp, neighbor_list)
        
        # set energy and forces
        self.results['energy'] = energy   
        self.results['forces'] = forces    
        
        assert(atoms.cell.rank==3)
        self.results['stress'] = stress.flat[[0, 4, 8, 5, 2, 1]] / atoms.get_volume()

        if self.parameters.extrapotentials is not None:
            assert isinstance(self.parameters.extrapotentials, list)
            self.get_extra_potential(atoms)

    def get_parameters(self, atoms):
        
        prefactor = self.parameters.prefactor
        
        if type(self.parameters.rc)==list:
            rc=self.parameters.rc
        else:
            covrads = [ covalent_radii[self.atoms[i].number] for i in range(len(self.atoms)) ]
            rc=self.parameters.rc*np.array(covrads)
        
        exp=self.parameters.exponent 
        
        return rc, prefactor, exp



    def get_energy_and_derivatives(self, atoms, rc, prefactor, exp, neighbor_list):

        energy, forces = self.setup_energy_forces(atoms)   
        
        stress = np.zeros((3, 3))

        for a1 in range(len(atoms)):
            
            neighbors, d = self.get_distance_info(atoms, a1, neighbor_list)
            
            if len(neighbors)>0:     
                
                potential, derivative = self.get_potential(a1, rc, prefactor, exp, d, neighbors)   
                
                energy = self.update_energy(energy, potential)
                
                forces = self.update_forces(forces, derivative, a1, neighbors) 
                
                stress=self.update_stress(stress, derivative, d)
                
        return energy, forces, stress
    
    
    
    def setup_energy_forces(self, atoms):
        energy = 0.0
        forces = np.zeros((len(atoms), 3)) 
        return energy, forces



    def get_distance_info(self, atoms, atom_idx, neighbor_list):
        
        positions = atoms.positions
            
        cell = atoms.cell
        
        neighbors, offsets = neighbor_list.get_neighbors(atom_idx)
            
        cells = np.dot(offsets, cell)
            
        d = positions[neighbors] + cells - positions[atom_idx]
        
        return neighbors, d  
        

    def get_potential_function_LJ(self, prefactor, x, exp):
        
        # Potential function with zero value and zero derivative at x=1.
        if x<=1:
            value = prefactor*(1/x**exp-1) - exp*prefactor*(1-x)
            deriv = exp*prefactor*(1-1/x**(exp+1))
        else:
            value = 0
            deriv = 0
        return value , deriv


    def get_potential_function_parabola(self, prefactor, x, exp):
        
        # Potential function with zero value and zero derivative at x=1.
        
        if x<=1:
            value = prefactor*(x-1)**exp
            deriv = exp*prefactor*(x-1)**(exp-1)
        else:
            value = 0
            deriv = 0
        return value , deriv



    def get_x_potential(self, prefactor, exp, r, crs):
        
        x = r/crs
        
        potential = np.zeros(len(x))
        derivative=np.zeros(len(x))
        
        if self.parameters.potential_type=='LJ':
            for i, xx in enumerate(x):
                potential[i], derivative[i] = self.get_potential_function_LJ(prefactor,xx, exp)
        elif self.parameters.potential_type=='parabola':
            for i, xx in enumerate(x):
                potential[i], derivative[i] = self.get_potential_function_parabola(prefactor,xx, exp)
        else:
            raise RuntimeError('potential_type={:s} not known.'
                               .format(self.parameters.potential_type))
            
        return potential, derivative 
        


    def radial_info(self, d, rc, atom_index, neighbors):
        r2 = (d**2).sum(1)
        r=np.sqrt(r2)
        
        crs=rc[atom_index]+np.array( [   rc[n] for n in neighbors   ]  )

        return r, crs


    def cartesian_conversion(self, derivative, d, r):
        r_block=np.tile(r,(len(d[0,:]),1)).T
        polar_to_cart=d/r_block
        derivative_cartesian=derivative[:,np.newaxis]*polar_to_cart
        
        return derivative_cartesian


    def get_potential(self, atom_index, rc, prefactor, exp, d, neighbors):        

        r, crs = self.radial_info(d, rc, atom_index, neighbors)  

        x_potential, x_potential_derivative = self.get_x_potential(prefactor, exp, r, crs)

        derivative = x_potential_derivative * 1/crs

        cartesian_derivative = self.cartesian_conversion(derivative, d, r)  

        return x_potential, -cartesian_derivative

    
    def update_energy(self, energy, potential):
        energy += potential.sum()
        return energy
    
    
    def update_forces(self, forces, derivative, atom_index, neighbors):
    
        forces[atom_index] -= derivative.sum(axis=0)
    
        for a2, f2 in zip(neighbors, derivative):
            forces[a2] += f2
        return forces
    
    def update_stress(self, stress, derivative, d):
        stress -= np.dot(derivative.T, d)
        return stress

    def get_extra_potential(self, atoms):
        for potential in self.parameters.extrapotentials:  
            self.results['energy']  += potential.potential(atoms)
            self.results['forces'] += potential.forces(atoms)
            self.results['stress']  +=  potential.stress(atoms)
    

class TallStructurePunisher:

    '''
    Punish atoms being below zlow or above zhigh
    
    E += strength * (z - zlow)**2 if z < zlow
    E += strength * (z - zhigh)**2 if z > zhigh
    E = 0                          else

    notice, the potential only works if the cell is nonperiodic
    in the z-direction meaning that the zz, xz and yz components
    of the tress tensor is not calculated

    Parameters
    ----------
    zlow : float (Angstroms)
        height uner which the potential starts
    zhigh : float (Angstroms)
        height over which the potential starts
    strength : float
        Strength k of the harmonic potential E = kA**2
        
    '''

    def __init__(self, zlow=0.0, zhigh=5.0, strength=2.0):
        self.zlow = zlow
        self.zhigh = zhigh
        self.strength = strength

    def potential(self, atoms):
        
        result = 0.0
        
        fixed_atoms=self.get_constrained_atoms(atoms)
        
        for i in range(len(atoms)):
            if i in fixed_atoms:
                continue
            
            z = atoms.positions[i, 2]
            if z >= self.zhigh:
                result += self.strength * (z - self.zhigh)**2
            elif z <= self.zlow:
                result += self.strength * (z - self.zlow)**2
        return result
        
    def forces(self, atoms):
        result = np.zeros((len(atoms), 3))
        
        fixed_atoms=self.get_constrained_atoms(atoms)
        
        for i in range(len(atoms)):
            if i in fixed_atoms:
                continue
                    
            z = atoms.positions[i, 2]
            if z >= self.zhigh:
                result[i, 2] += self.strength * 2 * (z - self.zhigh)
            elif z <= self.zlow:
                result[i, 2] += self.strength * 2 * (z - self.zlow)
        return -result

    def stress(self, atoms):
    
        results=np.zeros(6)
        
        return results
    
    def get_constrained_atoms(self, atoms):
        constr_index = []
        for C in atoms.constraints:
            if isinstance(C, FixAtoms):
                constr_index = C.index
        return np.array(constr_index)
        
 
class AreaPunisher:
    
    def __init__(self, A_small=20.0, A_large=200.0, strength=10.0):
        '''
        Punish too small unit cells in directions x and y.
        Potential shape:
        
        E = strength * (A-A_small)**2  if A < A_small
        E = strength * (A-A_large)**2  if A > A_large
        E = 0                          else

        Here A is the area in xy plane.

        notice, the potential only works properly if system 
        is nonperiodic in z-direction and if the cell has the form  
        [ [a,b,0],  [c,d,0],  [0,0,e] ]
        


        Parameters
        ----------
        A_small : float (Angstroms**2)
            Area under which the potential starts
        A_large : float (Angstroms**2)
            Area over which the potential starts
        strength : float
            Strength k of the harmonic potential E = kA**2
            
        '''

        assert(A_small<A_large)

        self.A_small = A_small
        self.A_large = A_large
        self.strength = strength

    def potential(self, atoms):
        
        '''
        Compute potential.
        '''
        
        cell = atoms.cell
        
        A = self.area(cell)
     
        if A<=self.A_small:
            result = self.strength * (A - self.A_small)**2
        elif A>=self.A_large:
            result = self.strength * (A - self.A_large)**2
        else:
            result=0
            
        return result

    @staticmethod
    def area(cell):
        '''
        XXX In future ASE, area will be directly available for ase.Cell
        '''
        # is cell is not flat in the z-direction I think this will
        # mess up the calculation
        return cell.area(2)

    def forces(self, atoms):
        '''
        Potential does not affect forces on atoms.
        '''
        return np.zeros((len(atoms), 3))

    def stress(self, atoms):
        
        results=np.zeros(6)     
        cell = atoms.cell
        A = self.area(cell)
        
        if A<=self.A_small:
            stress= 2*self.strength * (A - self.A_small)*A  / atoms.get_volume()
            results[0]=stress
            results[1]=stress
        elif A>=self.A_large:
            stress= 2*self.strength * (A - self.A_large)*A  / atoms.get_volume()
            results[0]=stress
            results[1]=stress
        
        return results


class VolumePunisher:
    
    def __init__(self, V_small=20.0, V_large=200.0, strength=10.0):
        '''
        Punish too small unit cells in directions x, y, z.
        Potential shape:
        
        E = strength * (V-V_small)**2  if V < V_small
        E = strength * (V-V_large)**2  if V > V_large
        E = 0                          else

        Here V is the area in xy plane.

        Parameters
        ----------
        V_small : float (Angstroms**3)
            volume under which the potential starts
        V_large : float (Angstroms**3)
            colume over which the potential starts
        strength : float
            Strength k of the harmonic potential E = kA**2
        '''
        assert(V_small<V_large)
        
        self.V_small = V_small
        self.V_large = V_large
        self.strength = strength

    def potential(self, atoms):
        
        '''
        Compute potential.
        '''
        
        V=atoms.get_volume()   
     
        if V<=self.V_small:
            result = self.strength * (V - self.V_small)**2
        elif V>=self.V_large: 
            result = self.strength * (V - self.V_large)**2
        else:
            result=0
            
        return result


    def forces(self, atoms):
        '''
        Potential does not affect forces on atoms.
        '''
        return np.zeros((len(atoms), 3))

    def stress(self, atoms):
        
        results=np.zeros(6)     

        V = atoms.get_volume()

        if V<=self.V_small:
            stress= 2*self.strength * (V - self.V_small)*V  / V
            results[0]=stress
            results[1]=stress
            results[2]=stress
        elif V>=self.V_large:
            stress= 2*self.strength * (V - self.V_large)*V  / V
            results[0]=stress
            results[1]=stress
            results[2]=stress                        
        
        return results


class CalculatorExtraPotential:
     
    def __init__(self, calculator):
        self.calculator = calculator
        
    def potential(self, atoms):
        self.calculator.results.clear()
        atoms = atoms.copy()
        atoms.calc = self.calculator
        atoms.get_potential_energy()
        return self.calculator.results['energy'] 
    
    def forces(self, atoms):
        return self.calculator.results['forces']  
    
    def stress(self, atoms):
        return self.calculator.results['stress']