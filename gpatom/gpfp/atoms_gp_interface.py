import numpy as np
from gpatom.gpfp.calculator import copy_image

from gpatom.gpfp.fingerprint import FingerPrint
from gpatom.gpfp.gp import GaussianProcess
from gpatom.gpfp.database import Database

from gpatom.gpfp.kerneltypes import EuclideanDistance

from ase.stress import (full_3x3_to_voigt_6_stress,
                        voigt_6_to_full_3x3_stress)

class Model:
    '''
    GP model parameters
    -------------------
    
    gp :  gaussianprocess object
     
    fp : fingerprint creator object
        Fingerprint class to be used in the model

    params: dict
        Dictionary to include all the hyperparameters for the kernel and
        for the fingerprint

    use_forces: bool
        Whether to train the GP on forces

    '''
   

    def __init__(self, gp=GaussianProcess, fp=FingerPrint):  

        self.data=Database()
        self.gp=gp
        self.fp=fp

    
    def new_fingerprint(self, atoms, **kwargs):    
        '''
        Compute a fingerprint of 'atoms' with given parameters.
        '''
        
        return self.fp.get(atoms=atoms, **kwargs)



    def add_data_points(self, train_images):       
        '''
        Calculate fingerprints and add features and corresponding targets
        to the database of 'data' and retrain.
        '''
        
        if type(train_images)!=list:
            train_images=[train_images]
                
        for im in train_images:
            image = copy_image(im)
            fp = self.new_fingerprint(image)
            self.data.add(fp, image.get_potential_energy(apply_constraint=False))
            
            if self.gp.use_forces:
                self.data.add_forces(image.get_forces(apply_constraint=False))
            
            if self.gp.use_stress:
                self.data.add_stress(image.get_stress(apply_constraint=False))
            
        self.train_model()


    def train_model(self):       
        '''
        Train a Gaussian process with given data.

        Parameters
        ----------
        gp : GaussianProcess instance
        data : Database instance

        Return a trained GaussianProcess instance
        '''

        features = self.data.get_all_fingerprints()

        targets=self.data.get_all_targets(self.gp.use_forces, 
                                          self.gp.use_stress)
        
        self.gp.train(features, targets)

        

    def calculate(self, atoms, with_stress=False, **kwargs):  

        '''
        Calculate energy, forces and uncertainty for a fingerprint
        using the given fingerprint.
        '''        
        
        x=self.new_fingerprint(atoms, **kwargs)
    
        natoms=len(atoms)
    
        f, V = self.gp.predict(x, get_variance=False)
        energy = f[0]
        gradients=f[1:]
        
        if self.gp.use_stress:
            gradients, stress = self.separate_grads(gradients, natoms)
            return energy, gradients, stress
            
        gradients = gradients.reshape(natoms, -1)
        if with_stress:
            stress=self.gp.predict_stress(x)
            return energy, gradients, stress
                    
        return energy, gradients  
    

    def get_energy_and_forces(self, atoms):
        
        fp=self.new_fingerprint(atoms)
        
        energy, forces, uncertainty_squared = self.gp.get_properties(fp)
        
        uncertainty = np.sqrt(uncertainty_squared)
        
        return energy, forces, uncertainty


    def get_predicted_stress(self, atoms):
        
        fp=self.new_fingerprint(atoms)
            
        stress=self.gp.predict_stress(fp)
        return stress


    def calculate_distances(self, atoms):              
        '''
        Calculate the distances between the training set and
        given atoms.
        '''
        fp0 = self.new_fingerprint(atoms)

        distances = np.zeros(len(self.data), dtype=float)

        for i, x in enumerate(self.data.fingerprintlist):
            distances[i] = EuclideanDistance.distance(fp0, x)

        return distances


    def recalculate_fingerprints(self):
        features = self.data.get_all_fingerprints()
        
        image_list=[f.atoms for f in features]
        
        new_fingerprints=[self.new_fingerprint(image) for image in image_list]
        
        self.data.replace_fingerprints(new_fingerprints)
        
        self.train_model() 
        
    def separate_grads(self, gradients, natoms):
        atom_grads = gradients[:-9].reshape(natoms, -1)
        cell_grads = gradients[-9:].reshape(3,3)
        cell_grads = full_3x3_to_voigt_6_stress(cell_grads)
        return atom_grads, cell_grads 
    
    

    
class LCBModel(Model):
    
    def __init__(self, gp=GaussianProcess, fp=FingerPrint, kappa=2):  
        
        super().__init__(gp=gp, fp=fp)
        
        self.kappa=kappa
        
    def calculate(self, atoms, with_stress=False, **kwargs):

        x=self.new_fingerprint(atoms, **kwargs)        
        
        f, V, dkdx = self.gp.predict(x, get_variance=True, return_dkdx=True)
                
        energy = f[0]
        gradients = f[1:]
    
        if self.gp.use_stress:
            unc, acq, dacq_r, dacq_c = self.calculate_acq(x, energy, gradients, V, dkdx)
            return acq, dacq_r, dacq_c
        
        unc, acq, dacq_r = self.calculate_acq(x, energy, gradients, V, dkdx)
                            
        if with_stress:
            dacq_c=self.calculate_stress(x, unc)
            return  acq, dacq_r, dacq_c
        
        return acq, dacq_r
    
    
    
    def calculate_acq(self, x, energy, gradients, V, dkdx):
        
        
        if self.gp.use_stress:
            var=V[0,0]
            unc=np.sqrt(var)
            
            f_var_1 = V[1: , 0]
            f_var_2 = V[0 , 1:]
            
            dvar = f_var_1 + f_var_2

            dunc=1/(2*unc)*dvar   
        
            grads_r, grads_c = self.separate_grads(gradients, len(x.atoms))
        
            dunc_r, dunc_c = self.separate_grads(dunc, len(x.atoms))
            dunc_c/=x.atoms.get_volume()
        
            acq=energy-self.kappa*unc
            dacq_r=grads_r-self.kappa*dunc_r
            dacq_c=grads_c-self.kappa*dunc_c
            
            return unc, acq, dacq_r, dacq_c
        
        if self.gp.use_forces:
            
            var=V[0,0]
            unc=np.sqrt(var)
            
            #quick solution
            f_var_1 = V[1: , 0]
            f_var_2 = V[0 , 1:]
            dvar = f_var_1 + f_var_2
            dvar=dvar.reshape(len(x.atoms),-1)
            
            dunc=1/(2*unc)*dvar
            
            '''
            #long general solution
            natoms=len(x.atoms)
            dk_dr = [np.concatenate((self.gp.kernel.kernel_function_gradient(x, x2).reshape(1,natoms*3),
                                     self.gp.kernel.kernel_function_hessian(x2, x) ),
                                    axis=0) for x2 in self.gp.X] 
            
            dk_dr = np.array(dk_dr).reshape(-1, natoms*3)
                        
            dkdr_Ck = np.einsum('ij,i->j', dk_dr, self.gp.Ck[:,0])
            dvar_r=-2*dkdr_Ck
            dvar_r=dvar_r.reshape(natoms, 3)
            dunc=1/(2*unc)*dvar_r
            '''
            
        else:
            var=V[0]
            unc=np.sqrt(var)
            
            self_grad=self.gp.kernel.kerneltype.kernel_gradient(x, x)
            dkdx_Ck = np.einsum('ijk,i->jk', dkdx, self.gp.Ck)
            dvar_r=2*(self_grad-dkdx_Ck)
            
            dunc=1/(2*unc)*dvar_r
            
            '''
            #long solution. kept here in case its needed at some point
            self_grad=self.gp.kernel.kerneltype.kernel_gradient(x, x)
            k = self.gp.kernel.kernel_vector(x, self.gp.X)
            k_grad=np.array([self.gp.kernel.kerneltype.kernel_gradient(x, x2)
                             for x2 in self.gp.X])

            Ck_grad=cho_solve((self.gp.L, self.gp.lower), k_grad,                  
                              overwrite_b=False, check_finite=True)
            k_gradCk = np.einsum('ijk,i->jk', k_grad, self.gp.Ck)
            kCk_grad =  np.einsum('i,ijk->jk', k, Ck_grad)
            dvar_r=2*self_grad-k_gradCk-kCk_grad
            '''
            
        gradients=gradients.reshape(len(x.atoms),-1)
        acq=energy-self.kappa*unc
        dacq_r=gradients-self.kappa*dunc
    
        return unc, acq, dacq_r
    
    
    def calculate_stress(self, x, unc):
        
        stress, dk_dc = self.gp.predict_stress(x, return_dkdc=True)
        
        if self.gp.use_forces:
            dkdc_Ck = np.einsum('ijk,i->jk', dk_dc, self.gp.Ck[:,0])
        else:
            dkdc_Ck = np.einsum('ijk,i->jk', dk_dc, self.gp.Ck)

        dvar_c=-2*dkdc_Ck
                
        volume=x.atoms.get_volume()
        dvar_c/=volume
        dunc_c=1/(2*unc)*dvar_c.flat[[0, 4, 8, 5, 2, 1]]
            
        dacq_c=stress-self.kappa*dunc_c
        
        return dacq_c
