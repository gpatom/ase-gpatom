import time
import warnings
from scipy.optimize import minimize, OptimizeResult
import numpy as np
from ase.parallel import paropen
from scipy.special import expit
import copy
from scipy.linalg import eigh
from scipy.linalg import solve_triangular, cho_factor, cho_solve
from scipy.spatial import distance_matrix

class HyperparameterFitter:       
    
    @classmethod
    def fit(cls, gp, params_to_fit, fit_weight=True, fit_prior=True, pd=None,
            bounds=None, tol=1e-2, txt='mll.txt'):
                
        '''
        Fit hyperparameters that are allowed to fit
        based on maximum log likelihood of the data in gp.
        '''


        txt = paropen(txt, 'a')
        txt.write('\n{:s}\n'.format(20 * '-'))
        txt.write('{}\n'.format(time.asctime()))
        txt.write('Number of training points: {}\n'.format(len(gp.X)))
                                                                               
        arguments = (gp, params_to_fit, fit_weight, fit_prior, pd, txt)

        params = []

        # In practice, we optimize the logarithms of the parameters:
        for string in params_to_fit:
            params.append(np.log10(gp.hp[string]))

        t0 = time.time()

        result = minimize(cls.neg_log_likelihood,
                          params,
                          args=arguments,
                          method='Nelder-Mead',
                          options={'fatol': tol})
        


        txt.write("Time spent minimizing neg log likelihood: "
                  "{:.02f} sec\n".format(time.time() - t0))

        converged = result.success

        # collect results:
        optimalparams = {}
        powered_results = np.power(10, result.x)
        
        
        
        for p, pstring in zip(powered_results, params_to_fit):
            optimalparams[pstring] = p

        gp.set_hyperparams(optimalparams)
        gp.train(gp.X, gp.Y)

        txt.write('{} success: {}\n'.format(str(gp.hp), converged))
        txt.close()

        return gp, result.fun



    @staticmethod
    def logP(gp):
        y = gp.Y.flatten()
               
        logP = (- 0.5 * np.dot(y - gp.prior_array, gp.model_vector)
                - np.sum(np.log(np.diag(gp.L)))
                - len(y) / 2 * np.log(2 * np.pi))
             
        return logP


    @staticmethod
    def neg_log_likelihood(params, *args, fit_weight=True, fit_prior=True):

        gp, params_to_fit, fit_weight, fit_prior, prior_distr, txt = args

        params_here = np.power(10, params)


        txt1 = ""
        paramdict = {}
        for p, pstring in zip(params_here, params_to_fit):
            paramdict[pstring] = p
            txt1 += "{:18.06f}".format(p)

        gp.set_hyperparams(paramdict)
        gp.train(gp.X, gp.Y)
        
        
        if fit_prior:                                       
            PriorFitter.fit(gp)
            
            
        if fit_weight:
            GPPrefactorFitter.fit(gp)


        # Compute log likelihood
        logP = HyperparameterFitter.logP(gp)
        
        # Prior distribution:
        if prior_distr is not None:
            logP += prior_distr.get(gp.hp['scale'])


        # Don't let ratio fall too small, resulting in numerical
        # difficulties:
        if 'ratio' in params_to_fit:
            ratio = params_here[params_to_fit.index('ratio')]
            if ratio < 1e-6:
                logP -= (1e-6 - ratio) * 1e6

        txt.write('Parameters: {:s}       -logP: {:12.02f}\n'
                  .format(txt1, -logP))
        txt.flush()
              
        return -logP



class PriorFitter:
    
    @staticmethod
    def fit(gp):
        gp.prior.update(gp.X, gp.Y, gp.L, gp.use_forces)
      
        gp.train(gp.X,gp.Y)  
        
        return gp


class GPPrefactorFitter:

    @staticmethod
    def fit(gp):
        oldvalue = gp.hp['weight']
        
        y = np.array(gp.Y).flatten()
              
        factor = np.sqrt(np.dot(y - gp.prior_array, gp.model_vector) / len(y))
        
        newvalue = factor * oldvalue

        gp.set_hyperparams({'weight': newvalue})
        # Rescale accordingly ("re-train"):
        gp.model_vector /= factor**2            
        gp.L *= factor  # lower triangle of Cholesky factor matrix
        gp.K_reg *= factor**2

        return gp



class HpFitInterface:
    
    
    def __init__(self, scale_prior=None, fit_scale_interval=1, fit_weight_interval=1, fit_prior_interval=1):

        self.scale_prior=scale_prior        

        self.fit_scale_interval=fit_scale_interval
        self.fit_weight_interval=fit_weight_interval
        self.fit_prior_interval=fit_prior_interval
        
        
    def get_fit_boolians(self, gp):
        fit_scale=(len(gp.X) % self.fit_scale_interval == 0)
        fit_weight=(len(gp.X) % self.fit_weight_interval == 0)
        fit_prior=(len(gp.X) % self.fit_prior_interval == 0)
        return fit_scale, fit_weight, fit_prior
            
    
    def fit(self, gp):
        
        fit_scale, fit_weight, fit_prior = self.get_fit_boolians(gp)
            
        
        if fit_scale:
            distances, dist_nn_avg = ScaleSetter.calculate_all_distances(gp)     
            ScaleSetter.update_scale_prior_distribution(self.scale_prior, distances)
            ScaleSetter.fix_scale(gp,distances)  
            
            gp , ll = HyperparameterFitter.fit(gp, ['scale'],
                                               fit_weight=fit_weight, 
                                               fit_prior=fit_prior, 
                                               pd=self.scale_prior) 
               
        else: 
                
            if fit_prior:
                gp=PriorFitter.fit(gp)
    
            if fit_weight: 
                gp = GPPrefactorFitter.fit(gp)
                
            ll=HyperparameterFitter.logP(gp)
    
    
        gp.train(gp.X, gp.Y)
        return gp, ll
    
    
    
    
class ScaleSetter():
    
    @staticmethod
    def calculate_all_distances(gp):
        fp_list=[x.vector for x in gp.X]
        
        fp_dist=distance_matrix(fp_list,fp_list)
        
        fp_dist_unique=np.unique(fp_dist)
        
        fp_dist_unique=fp_dist_unique[fp_dist_unique > 0]

        fp_dist_big_diag=fp_dist+np.eye(len(fp_dist))*2*np.max(fp_dist)
        fp_dist_nn_unique=np.unique(np.min(fp_dist_big_diag,0))
        fp_dist_nn_avg=np.mean(fp_dist_nn_unique)
                        
        return fp_dist_unique, fp_dist_nn_avg
    
            
    @staticmethod
    def fix_scale(gp, distances):
        '''
        Set length so that it is at least as high
        as its lower bound for fitting.
        '''

        scale = max(gp.hp['scale'], np.mean(distances))
        gp.set_hyperparams({'scale': scale})
    
    
    @staticmethod
    def update_scale_prior_distribution(scale_prior, distances):
        ''' Update the 'loc' attribute of the prior distribution '''
        
        if scale_prior is None:
            return
        
        scale_prior.update(distances)
        

class PriorDistributionLogNormal():

    def __init__(self, loc=6, width=1, log=True, update_width=True):
        ''' LogNormal "distribution" for
        hyperparameter 'scale'. Parameters correspond
        to mean (location of peak) and standard deviation
        of the Log_Nmormal in logaritmic space. 
        in non-logaritmic space the distribution mode is located at exp(loc-width**2)
        the prior is set up such that the mode is always located at the mean value
        of feature space distances in non-logaritmic space. 
        '''
        self.name='LogNormal'
        self.loc=loc
        self.width=width
        self.log=log
        self.update_width=update_width
                
    def get(self,x):
        
        log_fp_dist_std=self.width
        
        mode=self.loc

        log_fp_dist_mean=mode+log_fp_dist_std**2
        
        A=-0.5*( (np.log(x) - (log_fp_dist_mean-log_fp_dist_std**2) ) /log_fp_dist_std)**2
        
        B=- log_fp_dist_mean + 0.5*log_fp_dist_std**2 - np.log(log_fp_dist_std*np.sqrt( 2*np.pi ))
        
        log_scale_prior= A+B
        
        return log_scale_prior
    
    def update(self, fingerprint_distances):   
       
        mean_dist=np.mean(fingerprint_distances)
       
        maxpoint=max(fingerprint_distances)
        
        self.loc=  np.log( 0.5* (mean_dist+maxpoint))   
    
        
        if self.update_width and len(fingerprint_distances)>1:
            if self.log:
                self.width=0.5*(np.log(maxpoint)-np.log(mean_dist))        
            else:
                self.width=np.log(0.5*(maxpoint-mean_dist))
        