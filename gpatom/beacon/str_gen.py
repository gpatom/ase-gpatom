from gpatom.gpfp.prior import RepulsivePotential
from ase.constraints import FixAtoms
from ase.optimize import BFGS
from ase.io import read

from scipy.spatial import distance_matrix
import numpy as np
import os

from scipy.optimize import minimize, Bounds

from ase.data import covalent_radii

from ase import Atoms
from ase.io import read, write, Trajectory

from ase.filters import UnitCellFilter

from ase.calculators.emt import EMT

from ase.io import write


def get_fixed_index(atoms):
    '''
    Get the list of indices that are constrained with FixAtoms.
    '''
    constr_index = []
    for C in atoms.constraints:
        if isinstance(C, FixAtoms):
            constr_index = C.index
   
    return np.array(constr_index, dtype=int)


class RandomStructure:

    def __init__(self, atoms, rng=np.random):
        self.atoms = atoms   
        self.rng = rng
        self.cindex = get_fixed_index(atoms)
        
    def sph2cart(self, r, theta, phi):
        x = r * np.sin(theta) * np.cos(phi)
        y = r * np.sin(theta) * np.sin(phi)
        z = r * np.cos(theta)
        return x, y, z
    

class AtomsRelaxer:
    def __init__(self, calculator=None, 
                 with_unit_cell=False,
                 fixed_cell_params=None, 
                 fmax=0.05, steps=200):
        
        
        if calculator is None:
            calculator = RepulsivePotential(prefactor=10, rc=0.9, 
                                            potential_type='parabola', 
                                            exponent=2, 
                                            with_stress=with_unit_cell,
                                            extrapotentials=None)
        
        
        if fixed_cell_params is None:
            fixed_cell_params=[False]*6
        
        self.calculator=calculator
        self.with_unit_cell=with_unit_cell
        self.mask=[not elem for elem in fixed_cell_params]
        self.fmax=fmax
        self.steps=steps        
        
    def run(self, atoms):

        atoms.calc = self.calculator   
        
        
        if self.with_unit_cell:
            opt_object=UnitCellFilter(atoms, mask=self.mask)
        else:
            opt_object=atoms
        
        opt = BFGS(opt_object, maxstep=0.1, logfile=os.devnull)
      
        opt.run(fmax=self.fmax, steps=self.steps)
        atoms.calc=None
        return atoms


class AtomsInsideBoxRelaxer:
    
    def __init__(self, box, calculator=None, covrad_inside=[True, True, True],
                 fmax=0.005, steps=200):
        
        
        if calculator is None:
            calculator = RepulsivePotential(prefactor=10, rc=0.9, 
                                            potential_type='parabola', 
                                            exponent=2, 
                                            extrapotentials=None)
        
        
        self.box=box
        self.covrad_inside=covrad_inside
        self.calculator=calculator
        self.fmax=fmax
        self.steps=steps
        self.ndof=3
        
    def run(self, atoms):
        
        lb, ub = self.get_bounds(atoms, self.box, self.covrad_inside)        
                     
        params = atoms.positions.flatten() 
            
        result = minimize(self.prior_vals,   
                          params,
                          args=(atoms, self.calculator),
                          method='L-BFGS-B',
                          bounds=Bounds(lb, ub, keep_feasible=False),
                          jac=True,
                          options={'ftol':0, 'gtol':self.fmax, 'maxiter': self.steps, 'maxls':20})

        opt_array = result['x'].reshape(len(atoms), self.ndof)

        opt_coords = opt_array[:, :self.ndof].reshape(-1, self.ndof)
                
        atoms.positions=opt_coords 

        atoms.calc=None
        return atoms
        

    def get_bounds(self, atoms, box, covrad_inside):
        lb , ub = self.setup_limits(atoms, box, covrad_inside)
        lb , ub = self.constrain_atoms(atoms, lb, ub)
        
        return lb, ub
    
    
    def setup_limits(self, atoms, box, covrad_inside):
        
        covrad_inside=np.array(covrad_inside,dtype=int)
            
        atomic_radii=[  covalent_radii[atoms[i].number]*covrad_inside  for i in range(len(atoms))   ]
        
        
        n_atoms=len(atoms)
        
        lb = [box[i][0] for i in range(self.ndof)]*n_atoms
        ub = [box[i][1] for i in range(self.ndof)]*n_atoms
        
        lb=np.array(lb) + np.array(atomic_radii).flatten()
        ub=np.array(ub) - np.array(atomic_radii).flatten()    
        
        return lb, ub
    
    
    def constrain_atoms(self, atoms, lb, ub):
        
        cindex = get_fixed_index(atoms)
        
        if len(cindex)!=0:
            for atom in atoms:
                if atom.index in cindex:
                    
                    idx=atom.index*self.ndof 
                    for i in range(self.ndof):
                    
                        lb[idx+i]=np.array( atom.position[i] )
                        ub[idx+i]=lb[idx+i]                    

        return lb, ub        
  

    def prior_vals(self, params, *args):
        
        atoms=args[0]
        prior=args[1]
       
        atoms=self.prepare_atoms_for_prior(atoms, params)
        
        prior.calculate(atoms)
        
        energy = prior.results['energy']
        derivatives = prior.results['forces']

        return  (energy, -np.array(derivatives.flatten()) )
                    
    
    def prepare_atoms_for_prior(self, atoms, params):
        
        coords = params.reshape(len(atoms), self.ndof)[:, :self.ndof]
        atoms = atoms.copy()
        atoms.positions = coords
        
        return atoms
    
          

class Remake(RandomStructure):
    
    def __init__(self, atoms, rng=np.random):
        RandomStructure.__init__(self, atoms)
                
    def get(self):
        newatoms=self.atoms.copy()
        return newatoms


class RandomBranch(RandomStructure):

    def __init__(self, atoms, llim=1.6, ulim=2.0, relaxer=None, **kwargs):

        RandomStructure.__init__(self, atoms, **kwargs)

        assert (llim <= ulim), ('Upper limit (ulim) '
                                'should be larger than '
                                'lower limit (llim).')

        self.llim = llim
        self.ulim = ulim
        self.relaxer=relaxer

    def get(self):
        newatoms = self.atoms.copy()
        cell = newatoms.cell.lengths()
    
        newpos=np.zeros((len(newatoms),3))
        
        idx_occupied=[]
        
        i=len(self.cindex)
        
        
        if i==0:
            newpos[0,:]=newatoms.cell.lengths() / 2
            idx_occupied.append(0)
        else:
            for atom in newatoms:
                if atom.index in self.cindex:
                    newpos[atom.index,:]=atom.position  
                    idx_occupied.append(atom.index)

        for atom in newatoms:
            if atom.index not in idx_occupied:
                new_position=self.connect_new_atom(cell, newpos[idx_occupied])
                newpos[atom.index,:] = new_position
                idx_occupied.append(atom.index)
        
        newatoms.positions=newpos
        
        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)
            
        return newatoms
    
        
    def connect_new_atom(self, cell, positions):
        
        while True:
            # Sample position for new atom:
            r, theta, phi = self.rng.random([3])
            r = self.llim + r * (self.ulim - self.llim)
            theta *= np.pi
            phi *= 2 * np.pi
            pos = self.sph2cart(r, theta, phi)

            # Add atom next to another one
            neighborindex = self.rng.randint(len(positions)) 
 
            new_position = positions[neighborindex] + pos
            new_position=new_position.reshape((1,3))
          
            dm = distance_matrix(new_position, positions)    

            distances_ok = dm.min() > self.llim
            
            cell_ok = (0.0 < new_position).all() and (new_position < cell).all()

            if distances_ok and cell_ok:
                return new_position


class RandomCell(RandomStructure):
        
    def __init__(self, atoms, scale=0.3, fixed_cell_params=None, relaxer=None, **kwargs):        
        

        '''
        fixed_cell_params : list of 6 booleans
            Whether to keep the cell parameters fixed, respectively in Voigt form
        '''

        RandomStructure.__init__(self, atoms, **kwargs)
     
        if  fixed_cell_params is None:
            fixed_cell_params = [False]*6 
                
        self.scale=scale
        
        self.fixed_cell_3x3 = self.get_fixed_cell_params(fixed_cell_params)
        
        self.relaxer=relaxer

    def get(self):

        cell = self.get_random_cellparams(self.atoms)

        newatoms = self.atoms.copy()
        newatoms.cell = cell

        newpos=self.get_new_positions(cell)

        newatoms.positions = newpos
    
        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)
          
        newatoms.wrap()
        
        return newatoms
    
 
    def get_new_positions(self, cell):
        newpos = []

        for atom in self.atoms:
            if atom.index in self.cindex:
                newpos.append(self.atoms.positions[atom.index])
                continue

            r0 = cell[0] * self.rng.random()
            r1 = cell[1] * self.rng.random()
            r2 = cell[2] * self.rng.random()

            pos = r0 + r1 + r2

            newpos.append(pos)

        newpos = np.array(newpos)
        
        return newpos

    
    def get_fixed_cell_params(self, params):

        if params is None:
            params = np.ones(6, dtype=bool)

        # convert voigt form to 3 x 3 matrix:
        fixed_3x3 = np.zeros((3, 3), dtype=bool)
        c1, c2 = np.unravel_index([0, 4, 8, 5, 2, 1], (3, 3))  # de-voigt
        for i in range(len(c1)):
            fixed_3x3[c1[i], c2[i]] = params[i]
        fixed_3x3[np.tril_indices(3)] = fixed_3x3.T[np.tril_indices(3)]
        
        return fixed_3x3

    def get_random_cellparams(self, atoms):

        # Approximately preserve volume of the unit cell
        volume = atoms.cell.volume

        # add random contributions:
        limit = self.scale * volume**(1/3.)
        addition = self.rng.uniform(-limit, limit, size=(3, 3))

        if self.fixed_cell_3x3 is not None:
            addition[self.fixed_cell_3x3] = 0.0

        newcell = self.atoms.cell.copy() + addition

        return newcell



class RandomCellVolumeRange(RandomCell):
        
    def __init__(self, atoms, scale=0.5, base_volume=None, 
                 volume_scaling=[1,3], relaxer=None, **kwargs):  
        
        '''
        fixed_cell_params : list of 6 booleans
            Whether to keep the cell parameters fixed, respectively in Voigt form
            If None, all cell parameters are given by random (if randomcell==True)
        '''

        RandomCell.__init__(self, atoms, scale=scale, 
                            fixed_cell_params=None, **kwargs)
                
        if base_volume is None:
            rc=np.array( [ covalent_radii[atoms[i].number] for i in range(len(atoms)) ] )
            base_volume=sum(4/3*np.pi*rc**3)
        
        
        self.volume_range=[base_volume*volume_scaling[0],
                           base_volume*volume_scaling[1]]
        
        self.base_cell = np.array([[1,0,0],
                                   [0,1,0], 
                                   [0,0,1]])
        
        self.relaxer=relaxer
        
    def get_random_cellparams(self, atoms):
    
        cell=self.base_cell.copy()
    
        addition = self.rng.uniform(-self.scale, self.scale, size=(3, 3))
    
        cell = cell + addition
            
        atoms=atoms.copy()
        
        atoms.cell=cell
        
        current_volume=atoms.get_volume()
            
        desired_volume = self.rng.uniform(self.volume_range[0], self.volume_range[1])
    
        g=( desired_volume/current_volume )**(1/3)
            
        atoms.cell*=g
        
        return atoms.cell


class RandomBox(RandomStructure):

    def __init__(self, atoms, box=[(0., 1.), (0., 1.), (0., 1.)],
                 covrad_inside=False, relaxer=None, **kwargs):

        RandomStructure.__init__(self, atoms, **kwargs)

        self.box = box
        self.covrad_inside=covrad_inside
        self.relaxer=relaxer
        
    def get_coord(self, rc, idx):        
        delta_box= (self.box[idx][1]-rc) - (self.box[idx][0]+rc)
        coord=delta_box*self.rng.random() + (self.box[idx][0]+rc)
        return coord


    def get_xyz(self, rc):
        x=self.get_coord(rc, 0)
        y=self.get_coord(rc, 1)
        z=self.get_coord(rc, 2)
        return np.array([x, y, z])   


    def get_positions(self, atoms):
        
        for atom in atoms:
            if atom.index not in self.cindex:
                rc = covalent_radii[atom.number] if self.covrad_inside else 0
                atom.position=self.get_xyz(rc)

        return atoms


    def get(self):
        
        newatoms=self.atoms.copy()
        
        newatoms=self.get_positions(newatoms)

        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)
            
        newatoms.wrap()

        return newatoms



class RandomBoxVolumeRange(RandomStructure):
    
    def __init__(self, atoms, base_volume=None, volume_scaling=[1,3], 
                 cell_length=None, free_space=5, 
                 covrad_inside=False, relaxer=None, **kwargs):

        RandomStructure.__init__(self, atoms, **kwargs)
        
        if base_volume is None:
            rc=np.array( [    covalent_radii[atoms[i].number]  for i in range(len(atoms))   ]  )
            base_volume=sum(4/3*np.pi*rc**3)
            
        volume_range=[base_volume*volume_scaling[0], 
                      base_volume*volume_scaling[1]]
        
        if cell_length is None:
            cell_length=(volume_range[1])**(1/3)+2*free_space
        
        self.volume_range=volume_range
        
        self.cell_size=[cell_length]*3
        
        self.atoms.cell=self.cell_size
        
        self.covrad_inside=covrad_inside
            
        self.relaxer=relaxer
        
    def get_box(self):
        volume = self.rng.uniform(self.volume_range[0], self.volume_range[1])
        
        box_length=volume**(1/3)
        box = BoxConstructor.construct_box(self.cell_size, box_length)
        return box

    def get_xyz(self, box, rc):
        x=self.get_coord(box, rc, 0)
        y=self.get_coord(box, rc, 1)
        z=self.get_coord(box, rc, 2)
        return np.array([x, y, z])   

    def get_positions(self, box, atoms):
        
        for atom in atoms:
            if atom.index not in self.cindex:
                rc = covalent_radii[atom.number] if self.covrad_inside else 0
                atom.position=self.get_xyz(box, rc)

        return atoms

    def get(self):
        
        newatoms=self.atoms.copy()
        
        box=self.get_box()
        
        newatoms=self.get_positions(box, newatoms)

        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)
            
        newatoms.wrap()

        return newatoms

    def get_coord(self, box, rc, idx):            
        delta_box = (box[idx][1]-rc) - (box[idx][0]+rc)
        coord=delta_box*self.rng.random() + (box[idx][0]+rc)
        return coord
    
    

class BoxConstructor:
    
    @staticmethod
    def get_box(atoms, volume_fraction=0.2, free_space=None, cell=None):
        box_length=BoxConstructor.get_box_length(atoms, volume_fraction)
        
        if cell is None:
            cell=BoxConstructor.get_unitcell(box_length, free_space)
        else:
            cell=atoms.cell
    
        box = BoxConstructor.construct_box(cell, box_length)
        
        return box, cell
        
    @staticmethod
    def get_box_length(atoms, volume_fraction):
        rc =np.array( [    covalent_radii[atoms[i].number]  for i in range(len(atoms))   ]  )
        
        V_atoms=sum(  (4/3)*np.pi*rc**3  )
        
        box_length=(V_atoms/volume_fraction)**(1/3)
        
        return box_length
        
    @staticmethod
    def construct_box(cell, box_length): 

        box_start=cell[0]/2-box_length/2
        box_end=cell[0]/2+box_length/2
        box=[(box_start, box_end)]*3
    
        return box

    @staticmethod
    def get_unitcell(box_length, free_space):
            
        if free_space is None:
            cell_L=box_length
        else:
            cell_L=box_length+2*free_space
       
        cell=[cell_L, cell_L, cell_L]
        
        return cell


class Random2D(RandomCell):
    '''
    Generator for a 2D-system with possibility of generating random
    cell parameters. The z-coordinate of the cell is kept fixed. The
    atoms are placed randomly in xy-plane and randomly in z-coordinates
    between 'minz' and 'maxz'.
    '''


    def __init__(self, atoms, minz, maxz, scale=0.3, fixed_cell_params=None,
                 covrad_inside=False, relaxer=None, **kwargs):
    
        self.minz = minz  
        self.maxz = maxz
        self.covrad_inside=covrad_inside
        
        if fixed_cell_params is None:
            fixed_cell_params=[False, False, True, True, True, False]
 
        RandomCell.__init__(self, atoms, scale=scale, 
                            fixed_cell_params=fixed_cell_params, **kwargs)
        
        self.relaxer=relaxer

    
    def get_xyz(self, cell, rc=0):
        x = cell[0] * self.rng.random()
        y = cell[1] * self.rng.random()

        # z-coordinate is a vector along the cell[2] axis
        # with random length between minz and maxz:
        delta_z = (self.maxz-rc) - (self.minz+rc)
        zlength = delta_z * self.rng.random() + (self.minz+rc)
        z = zlength * cell[2] / np.linalg.norm(cell[2])

        return x + y + z
    

    def get(self):
        '''
        Mostly duplication of RandomBox.get()
        '''
        newatoms = self.atoms.copy()
        if not self.fixed_cell_3x3.all():
            cell = self.get_random_cellparams(self.atoms)
        else:
            cell = self.atoms.cell

        newatoms.cell = cell

        newpos = []

        for atom in self.atoms:

            if atom.index in self.cindex:
                pos = atom.position
            else:
                rc = covalent_radii[atom.number] if self.covrad_inside else 0
                pos = self.get_xyz(cell, rc)

            newpos.append(pos)

        newpos = np.array(newpos)

        newatoms.positions = newpos

        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)

        newatoms.wrap()

        return newatoms


class Random2DRanges(RandomStructure):
    """
    Generator for a 2D-system with possibility of generating random
    cell parameters for X,Y as well as their angle.
    This generator will ignore the input unit cell, except for the z-direction
    The z-coordinate of the cell is kept fixed.
    The atoms are placed randomly in the generated xy-plane.. 
    and randomly in z-coordinates between 'minz' and 'maxz'. 
    xy_ratio_rannges are y_to_x_ratio limits, default [1, 1.5]
    anlge_ranges are angle limits in degrees, default [25,90].
    """
    def __init__(self, atoms, minz, maxz, base_area=None, area_scaling=[1,3],  
                 xy_ratio_range=[1,1.5], angle_range=[25, 90], height=20, 
                 covrad_inside=False, relaxer=None, **kwargs):
                         
        RandomStructure.__init__(self, atoms, **kwargs)
    
        self.minz = minz
        self.maxz = maxz
        self.covrad_inside=covrad_inside
        
        if base_area is None:
            rc=np.array( [    covalent_radii[atoms[i].number]  for i in range(len(atoms))   ]  )
            if minz==maxz:
                base_area=sum( np.pi*rc**2 )
            else:
                dz = (maxz-minz)
                atoms_volume=sum( (4/3)*np.pi*rc**3 )
                base_area=atoms_volume/dz
            
        self.area_range=[base_area*area_scaling[0],
                         base_area*area_scaling[1]]
        
        self.xy_ratio_range=xy_ratio_range
        self.angle_range=angle_range
        self.height=height
        
        self.relaxer = relaxer
        

    def get_xyz(self, cell, rc=0):
        x = cell[0] * self.rng.random()
        y = cell[1] * self.rng.random()

        # z-coordinate is a vector along the cell[2] axis
        # with random length between minz and maxz:
        delta_z = (self.maxz-rc) - (self.minz+rc)
        zlength = delta_z * self.rng.random() + (self.minz+rc)
        z = zlength * cell[2] / np.linalg.norm(cell[2])
        
        return x + y + z

    def get(self):
        '''
        Mostly duplication of RandomBox.get()
        '''

        cell=self.get_cell()
        
        newatoms = self.atoms.copy()
        newatoms.set_cell(cell)
        
        cell=newatoms.cell
        newpos = []

        for atom in self.atoms:

            if atom.index in self.cindex:
                pos = atom.position
            else:
                rc = covalent_radii[atom.number] if self.covrad_inside else 0
                pos = self.get_xyz(cell, rc)

            newpos.append(pos)

        newpos = np.array(newpos)

        newatoms.positions = newpos

        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)

        newatoms.wrap()

        return newatoms
    
    
    def get_cell(self):
        area=self.rng.uniform(self.area_range[0],self.area_range[1])
        xy_ratio=self.rng.uniform(self.xy_ratio_range[0],self.xy_ratio_range[1])
        angle=self.rng.uniform(self.angle_range[0],self.angle_range[1])
        
        angle=angle*np.pi/180

        X=np.array([1.0, 0.0, 0.0])
        Y=np.array([np.cos(angle),np.sin(angle),0])*xy_ratio
        Z=[0,0,self.height]
        
        X_norm=np.linalg.norm(X)
        Y_norm=np.linalg.norm(Y)
        
        current_area=X_norm*Y_norm*np.sin(angle)
        
        g=np.sqrt(area/current_area)
        
        X*=g
        Y*=g
        
        cell=np.array([X,Y,Z])
        
        return cell


class RandomGrapheneSubstrate(RandomStructure):
    
    def __init__(self, graphene, n_delete=0, substitution_atoms=None, 
                 adsorbate_atoms=None, delta_z=5, relaxer=None, **kwargs):
        
        
        graphene_atoms=graphene.copy()
        
        for i in range(n_delete):
            graphene_atoms.pop()
        
        atoms=graphene_atoms+adsorbate_atoms

        RandomStructure.__init__(self, atoms, **kwargs)
       
        self.graphene=graphene
                
        self.n_delete=n_delete
        
        if substitution_atoms is None:
            n_substitute=[]
            elem_substitute=[]
        else:
            symbols = substitution_atoms.get_chemical_symbols()
            elem_substitute, n_substitute, = np.unique(symbols, return_counts=True)
       
        self.elem_substitute=elem_substitute 
       
        self.n_substitute=n_substitute
    
        self.adsorbate_atoms=adsorbate_atoms
        
        self.minz=self.graphene[0].position[2] 

        self.maxz=self.minz+delta_z

        self.relaxer=relaxer
    
    
    def get(self):
        
        substrate=self.graphene.copy()
        
        if self.n_delete>0:
            substrate=self.make_holes(substrate)
        
        if len(self.elem_substitute)>0:
            substrate=self.make_substitutions(substrate)
               
        if self.adsorbate_atoms is not None:
            newatoms=self.add_adsorbates(substrate)
        else:
            newatoms=substrate
            
        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)

        newatoms.wrap()

        return newatoms
        
    
    def make_holes(self, graphene):
        
        permutation=self.rng.permutation(len(graphene))
        
        delete_indices=permutation[0:self.n_delete]
        
        delete_indices=sorted(delete_indices, reverse=True)
        
        for index in delete_indices:
            del graphene[index]
            
        return graphene 
    
    
    def make_substitutions(self, graphene):
        
        permutation=self.rng.permutation(len(graphene))
        
        low_index=0
        
        for i in range(len(self.n_substitute)):
            high_index=low_index+self.n_substitute[i]
            
            substitution_indices=permutation[low_index:high_index]

            for index in substitution_indices:
                graphene[index].symbol = self.elem_substitute[i]
            
            low_index+=self.n_substitute[i]
            
        return graphene


    def add_adsorbates(self, graphene):
                
        new_atoms=graphene.copy()
        
        cell=graphene.cell
        
        for atom in self.adsorbate_atoms:
            rc = covalent_radii[atom.number]
            pos = self.get_xyz(cell, rc)
            atom.position=pos
            new_atoms.append(atom)
        
        return new_atoms
    
    
    def get_xyz(self, cell, rc=0):
        x = cell[0] * self.rng.random()
        y = cell[1] * self.rng.random()

        # z-coordinate is a vector along the cell[2] axis
        # with random length between minz and maxz:
        delta_z = (self.maxz-rc) - (self.minz+rc)
        zlength = delta_z * self.rng.random() + (self.minz+rc)
        z = zlength * cell[2] / np.linalg.norm(cell[2])

        return x + y + z
    
    
    def constrain_substrate_atoms(self, graphene):
    
        fixed_atoms_indices=np.arange(len(graphene))
        constraint = FixAtoms(indices=fixed_atoms_indices)
        graphene.set_constraint(constraint)
            
        return graphene
            


class MoleculeOnSubstrate(RandomStructure):
    
    def __init__(self, substrate, adsorbate, minz=2, maxz=4, 
                 cell_displace=0.125, relaxer=None, **kwargs):

        
        atoms=substrate + adsorbate

        RandomStructure.__init__(self, atoms, **kwargs)

        self.substrate=substrate
    
        self.adsorbate=adsorbate
        
        self.minz=minz
        
        self.maxz=maxz  
    
        self.cell_displace=cell_displace
    
        self.relaxer=relaxer
    
    def get(self):
        
        adsorbate=self.adsorbate.copy()
        
        adsorbate.cell=self.substrate.cell.copy()
        adsorbate.center()
            
        delta_z = self.maxz - self.minz
        zlength = delta_z * self.rng.random() + self.minz
        
        adsorbate.positions[:,2]+=zlength
    
        adsorbate.rotate('z', self.rng.random()*360, center='COM')
    
        unitcell_cv = (self.substrate.cell*self.cell_displace)[:2, :2]    
    
        random_displacement=np.zeros(3)
        
        random_displacement[0:2] =  self.rng.random(2) @ unitcell_cv
        
        adsorbate.translate(random_displacement)
        
        newatoms=self.substrate.copy()
        
        newatoms.extend(adsorbate)
        
        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)
        
        newatoms.wrap()

        return newatoms
        
    
    
class ReadFiles():

    def __init__(self, filenames):

        self.frames = [read(fname) for fname in filenames]
        self.count = 0

    def get(self):
        atoms = self.frames[self.count]
        self.count += 1
        return atoms


class Rattler(RandomStructure):                
    
    def __init__(self, atoms, intensity=0.1, **kwargs):

        RandomStructure.__init__(self, atoms, **kwargs)

        self.intensity = intensity

    def get(self):
        atoms = self.atoms.copy()
        atoms.rattle(self.intensity,
                     seed=self.rng.randint(100000))
        atoms.wrap()
        return atoms

