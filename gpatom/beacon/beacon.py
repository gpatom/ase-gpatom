from gpatom.gpfp.calculator import copy_image, GPCalculator

from gpatom.beacon.inout import FileWriter, FileWriter2

from ase.optimize import BFGS
from ase.io import read, write, Trajectory

from ase.parallel import world, broadcast

from ase.data import covalent_radii
from scipy.spatial import distance_matrix

import numpy as np
import warnings
import os

from ase.filters import UnitCellFilter

import signal

from ase.build import niggli_reduce

class TimeoutError(Exception):
    pass

class CustomError(Exception):
    pass

class BEACON():

    '''
    Bayesian Exploration of Atomic Configurations for Optimization
    --------------------------------------------------------------

    Optimizer to search for the global minimum energy.

    Find optimal structure of a set of atoms given the
    number of atoms, size and periodicity of the unit cell
    and the potential. The calculator uses Gaussian process
    to model the global potential energy landscape, by
    representing the atomic structure by a rotationally,
    translationally and permutationally invariant fingerprint,
    and searches for the absolute minimum of the landscape.

    The original idea behind the method is published
    (along with the code GOFEE) as
        Efficient Global Structure Optimization with a
        Machine-Learned Surrogate Model
        M. K. Bisbo, B. Hammer
        Physical Review Letters, vol. 124, 086102 (2020)
        https://doi.org/10.1103/PhysRevLett.124.086102

    BEACON is published as
        Global optimization of atomic structures with
        gradient-enhanced Gaussian process regression
        S. Kaappa, E. G. del R\'io, K. W. Jacobsen
        Physical Review B, vol. 103, 174114 (2021)
        https://doi.org/10.1103/PhysRevB.103.174114

    Usage:
    1. Create a class instance as
         from gpatom.beacon import BEACON
         go = BEACON(calculator=..., model=..., ...)
    2. Find optimal structure:
         go.run()

    Parameters:

    calculator : calculator object
        must be supplied

    model : model object
        must be supplied

    initatomsgen : random structure generator object
        must be supplied

    hp_optimizer : hp_optimzer object
        default None (no fitting of hyper parameters)

    surropt : surrogate optimizer object
        default None (structures wont be relaxed)

    checker : surrogate optimizer object
        default None (no checking of surrogate relaxed structures)

    acq : acquisition function object.
        default None (atoms are evaluated by predicted energy only)

    init_atoms : list of atoms objects
        initial training set made by the user.
        Dafault None  (make random instead with initatmsgen)

    ninit : int
        Number of initial training structures if init_atoms is not
        given.
        Default 2

    ndft : int
        Maximum number of search steps during the search.
        After this number of steps, go.run() is terminated.
        Default 100

    nsur : int
        Number of relaxations in surrogate potential at each step
        Default world.size

    write_surropt : bool
        if files of the individual surrogate relaxations should be made.
        will be saved in surropt.xyz
        Default False

    stopfitsize : int
        Number of DFT calculations after which fitting the
        hyperparameters is stopped. Does not affect prefactor
        and prior constant fitting.
        Default 10000 (=never)

    output : string
        Filename of the output for results and parameters.
        Default info.txt

    foutput : string
        Filename of the output for predicted and evaluated forces.
        Default os.devnull (not writing the file)

    logoutput : string
        Filename of the log book.
        Default log.txt
    '''


    def __init__(self, calculator, model, initatomsgen, hp_optimizer=None,
                 fp_updater=None, surropt=None, checker=None, acq=None, init_atoms=None,
                 logger=None, ninit=2, ndft=100, nsur=None,  write_surropt=False,
                 stopfitsize=10000, stop_fp_update_size=10000, 
                 surropt_timeout=None, make_rand_if_fail=True):

        self.stopfitsize=stopfitsize
        
        self.stop_fp_update_size=stop_fp_update_size
        
        self.write_surropt=write_surropt
        self.make_rand_if_fail=make_rand_if_fail


        self.calculator_interface=calculator
        self.model=model
        self.hp_optimizer=hp_optimizer
        self.fp_updater=fp_updater
        self.initatomsgen=initatomsgen
        self.surropt=surropt
        self.checker=checker
        self.acq=acq

        if logger is None:
            logger=Logger()
        self.logger=logger

        # Atoms reader/writer:
        self.atomsio = BEACONAtomsIO()


        if (len(self.model.data)>0):
            raise RuntimeError("Don't import models with pre-existing databases."
                               "import list of atoms objects instead: atoms=list")


        if init_atoms is None:
            init_atoms = []
            for i in range(ninit):
                init_atoms.append(self.initatomsgen.get_random())

        if type(init_atoms)!=list:
            init_atoms=[init_atoms]

        self.init_atoms = init_atoms
        # These lines makes sure that all processors have the same initial structures.
        #it is absolutely crucial that this comes after generating the random structures.
        # This one deletes atoms constraints   
        for i in range(len(self.init_atoms)):
            self.init_atoms[i] = broadcast(self.init_atoms[i], root=0)

        self.natoms=len(self.init_atoms[0])

        # Run parameters:
        self.ndft = ndft
        self.nsur = nsur if nsur is not None else world.size
        assert self.nsur > 0


        self.set_initial_frames(self.init_atoms)

        self.initatomsgen.setup_lob(self.init_atoms)

        self.model.add_data_points(self.init_atoms)

        self.index = len(self.init_atoms)

        if surropt_timeout is not None:
            surropt_timeout=int(surropt_timeout*60)
            
        self.surropt_timeout=surropt_timeout

    def run(self):

        ''' Run search '''
        if self.checker is None:
            self.logger.log_from_master('Structure checks set to False!!!')

        self.logger.log_init_structure_info(self.init_atoms)
        # RUN GLOBAL SEARCH
        while self.index < self.ndft:

            self.logger.log_stepinit(self.index, len(self.model.data))

            self.logger.log_lob_energies(self.initatomsgen.lob.energies)

            # optimize hyper parameters before running the search
            
            self.update_fp_parameters(self.model)
            
            self.optimize_gp_hyperparameters(self.model)


            (best_atoms, best_e, best_f, 
             best_u, best_subindex) = self.get_candidate()

            if best_atoms is None:  # if no structures were accepted
                self.add_random_structures()
                self.logger.log_random_structure()
                self.index += 1
                continue

            try:
                self.calculator_interface.calculate_properties(best_atoms)
                real_e, real_f = self.calculator_interface.get_output(best_atoms)
                self.logger.log_real_energy(self.index, real_e)
            except Exception as err:
                self.logger.log_from_master('Something went wrong: {} '
                                            .format(str(err)))
                
                if self.make_rand_if_fail:
                    self.add_random_structures()
                    self.logger.log_random_structure()
                    self.index += 1
                else:
                    self.logger.log_from_master('Continuing...')
                    
                continue

            # WRITE RESULT XYZ
            best_atoms.info['Ndft']=self.index
            self.atomsio.write_xyz(best_atoms, strtype='result')

            # WRITE RESULT NUMBERS
            distances = self.model.calculate_distances(best_atoms)

            fmax_real = self.logger.get_max_force(real_f)
            fmax_pred = self.logger.get_max_force(best_f)

            data = dict(index=self.index,
                        pred=best_e,
                        real=real_e,
                        unc=best_u,
                        diff=real_e - best_e,
                        fmax_real=fmax_real,
                        fmax_pred=fmax_pred,
                        mindist=distances.min(),
                        maxdist=distances.max(),
                        ntrain=len(self.model.data),
                        prior=self.model.gp.prior.constant,
                        **self.model.gp.hp,
                        aweight=self.model.fp.fp_args['aweight'])

            self.logger.write_output(data)

            # UPDATE LIST OF BEST
            self.initatomsgen.lob.update(new_atoms=best_atoms,
                                         candidate_index=best_subindex)


            # UPDATE GAUSSIAN PROCESS
            self.model.add_data_points([best_atoms])

            # ITERATE
            self.index += 1


        self.logger.log_from_master('Maximum number of steps '
                                    '({}) reached.'.format(self.ndft))

    def set_initial_frames(self, atoms):
        '''
        Calculate energies and forces for initial training images,
        and save to files.
        '''

        for i in range(len(atoms)):
            try:
                self.calculator_interface.calculate_properties(atoms[i], attatch_calc=False)
            except RuntimeError:
                self.calculator_interface.calculate_properties(atoms[i])
                tmp = copy_image(atoms[i])
                atoms[i] = tmp

            self.atomsio.write_xyz(atoms[i], strtype='init')


    def get_candidate(self):
        it_result=self.get_candidate_pool()

        (best_atoms, best_e, best_f, 
         best_u, best_acq, best_subindex) = it_result.distribute()

        if best_atoms is None:  # if no atoms were accepted
            self.logger.log_novalid()
        else:
            self.logger.log_bestinfo(self.index, best_subindex, 
                                     best_e, best_u, best_acq)

        return best_atoms, best_e, best_f, best_u, best_subindex



    def get_candidate_pool(self):

        it_result = IterationResult(self.natoms)

        for j in range(self.nsur):

            # DISTRIBUTE SURROGATE RELAXATIONS
            if j % world.size != world.rank:
                continue

            # CREATE NEW STRUCTURES
            testatoms = self.initatomsgen.get(j)

            # SURROGATE RELAXATION
            
            if self.surropt is not None:
            
                if (self.surropt_timeout is not None):
                    signal.signal(signal.SIGALRM, self.surropt_timeout_handler)
                    signal.alarm(self.surropt_timeout)
                    
                try: 
                    file_identifier='{:03d}_{:03d}'.format(self.index, j)
                    testatoms, success = self.surropt.relax(testatoms, self.model,
                                                            file_identifier)
                except (TimeoutError, CustomError) as err:
                    structure_ok=False
                    string='Relaxation Failed: {} '.format(str(err))
                    self.logger.log_nonvalid_structures(structure_ok, string,
                                                        index=self.index, subindex=j)                    
                    continue
                    
                finally:
                    if (self.surropt_timeout is not None):
                        signal.alarm(0)
                    
            else:
                success=False


            energy, forces, unc = self.model.get_energy_and_forces(testatoms)


            if self.write_surropt:
                self.write_surropt_results(testatoms, energy, forces, unc, success)

            self.logger.log_relaxedinfo(index=self.index, energy=energy,
                                        success=success, unc=unc, subindex=j)


            if self.checker is not None:
                distances = self.model.calculate_distances(testatoms)
                structure_ok, string = self.checker.check(atoms=testatoms,
                                                  distances=distances)

                self.logger.log_nonvalid_structures(structure_ok, string,
                                                    index=self.index, subindex=j)

            else:
                structure_ok = True

            if structure_ok:

                if self.acq is not None:
                    my_acq = self.acq.get(energy=energy, uncertainty=unc)
                else:
                    my_acq= self.model.calculate(testatoms)[0]

                better_acq = it_result.is_better(my_acq)
                if better_acq:
                    it_result.update(dict(energy=energy,
                                          forces=forces.flatten().copy(),
                                          unc=unc,
                                          atoms=testatoms,
                                          acq=my_acq,
                                          subindex=j))
        return it_result


    def surropt_timeout_handler(self, signum, frame):
        raise TimeoutError("Relaxation timed out")


    def write_surropt_results(self, atoms, energy, forces, unc, success):
        atoms.info['index']=self.index
        atoms.info['gp_energy']=energy
        atoms.info['gp_uncertainty']=unc
        atoms.info['success']=success
        self.atomsio.write_xyz(atoms, strtype='surropt', parallel=False)
        

    def read_pool(self, index=None):
        '''
        method to read a pool made by get_candidate_pool
        index. lets you decide if you want pool from a specific cycle
        '''

        pool_all=read('surropt.xyz',':')

        if index is None:
            pool=pool_all
        else:
            pool=[atoms for atoms in pool_all if atoms.info['index']==index]

        energies=[atoms.info['gp_energy'] for atoms in pool]
        uncertainty=[atoms.info['gp_uncertainty'] for atoms in pool]
        success=[atoms.info['success'] for atoms in pool]

        return pool, energies, uncertainty, success



    def add_random_structures(self):
        '''
        Add a structure (by sgen) to the training set.
        '''
        random_structure_successfull=False
        count=0
        while not random_structure_successfull:
            try: 
                atoms=self.generate_valid_random_structure()
                #broadcast atomsobject from rank 0 to all processors
                atoms = broadcast(atoms, root=0)
    
                # Evaluate:
                self.calculator_interface.calculate_properties(atoms)
                random_structure_successfull=True
            except Exception as err:
                count+=1
                self.logger.log_from_master('Random structure failed: {} '
                                            'Retrying...'.format(str(err)))
                
                if count>3:
                    raise Exception

        # candidate_index=np.inf just means not part of list of best.
        self.initatomsgen.lob.update(new_atoms=atoms,
                                     candidate_index=np.inf)



        new_structure = [copy_image(atoms)]
        self.model.add_data_points(new_structure)

        # Write extra_NNN.xyz file:
        if world.rank == 0:
            self.atomsio.write_xyz(atoms, strtype='extra', parallel=False)

    
    def generate_valid_random_structure(self):
        valid_structure_found=False
        while not valid_structure_found:
            atoms=self.initatomsgen.get_random()
 
            if self.checker is None:
                valid_structure_found=True
            else:                
                distances=self.model.calculate_distances(atoms)
                if self.checker.check_fingerprint_distances(distances):
                    valid_structure_found=True
            
        return atoms


    def optimize_gp_hyperparameters(self, model):
        
        if self.hp_optimizer is None:
            return

        if len(self.model.data) > self.stopfitsize:
            return

        self.hp_optimizer.fit(model.gp)
        
    def update_fp_parameters(self, model):
        
        if self.fp_updater is None:
            return
        
        if len(self.model.data) > self.stop_fp_update_size:
            return
        
        fingerprints = self.model.data.get_all_fingerprints()
        
        self.fp_updater.update(fingerprints, self.model.fp)
        
        self.model.recalculate_fingerprints()
               

        
class Logger:
    
    '''    
    Logger class to log progress of BEACON global optimizer
    
    usage
    1 instantiate class 
        logger=Logger(output=.., logoutput=..)
    2 write dictionary to info file 
        logger.write_output(dict)      
    3 write to text string log file 
        logger.log_from_master(txt)  
        

    parameters:
        
    output: string
        name for output of beacon inout.py FileWriter2.  
        default: info.txt
    
    
    logoutput: string
        name for output of beacon inout.py FileWriter.  
        default: log.txt
    '''
    
    
    
    def __init__(self, output='info.txt',
                 logoutput='log.txt'):
        if world.rank == 0:
            self.infofile = FileWriter2(output, printout=False)

        # All processors
        self.logfile = FileWriter(logoutput, printout=False,
                                  write_time=True)
        
        
    def write_output(self, dictionary):
        if world.rank == 0:
            self.infofile.write(dictionary)

    def log_from_master(self, txt):
        if world.rank == 0:
            self.logfile.write(txt)

    def log_real_energy(self, index, real_e):

        txt = 'Index: {:03d} Real energy: {:06f}'
        txt = txt.format(index, real_e)
        self.log_from_master(txt)

    def get_max_force(self, forces):
        '''
        Get maximum absolute force of the given force array.
        '''
        forces = forces.reshape(-1, 3)
        fmax=np.max(np.linalg.norm(forces, axis=1))

        return fmax

    def log_init_structure_info(self, atoms):
        world.barrier()
        for i in range(len(atoms)):
            txt = ('Init structure {:03d} '
                   'Energy: {:.02f} eV '
                   'Max force: {:.02f} eV/Ang'
                   .format(i,
                           atoms[i].get_potential_energy(),
                           self.get_max_force(atoms[i].get_forces())))

            self.log_from_master(txt)

    def log_stepinit(self, index, data_size):
        txt = ('Index: {:03d} Training data size: {:5d}'
               .format(index, data_size ))
        self.log_from_master(txt)

    def log_relaxedinfo(self, index, energy, success, unc, subindex):
        ''' Log from all processors '''

        notconverged = ''
        if not success:
            notconverged = '(Not converged)'
        self.logfile.write('Index: {:03d} Subindex: {:03d} '
                           'Energy: {:.04f} Uncertainty: {:.04f} {}'
                           .format(index, subindex,
                                   energy, unc,
                                   notconverged))

    def log_novalid(self):
        txt = 'No valid structure found.'
        self.log_from_master(txt)

    def log_random_structure(self):
        txt = 'Random structure made.'
        self.log_from_master(txt)

    def log_bestinfo(self, index, best_subindex, best_e, best_u, best_acq):
        txt = 'Index: {:03d} '
        txt += 'Best structure: '
        txt += 'Subindex: {:03d} '
        txt += 'Energy: {:04f} '
        txt += 'Uncertainty: {:04f} '
        txt += 'Acquisition: {:04f} '
        txt = txt.format(index, best_subindex, best_e,
                         best_u, best_acq)
        self.log_from_master(txt)


    def log_lob_energies(self, listofbest_e):
        '''
        Write energies from list-of-best-structures
        to log file.
        '''

        if len(listofbest_e) == 0:
            txt = 'List of best energies is empty.'
            self.log_from_master(txt)
        else:
            txt = 'List of best energies:'
            self.log_from_master(txt)
            for i, e in enumerate(listofbest_e):
                txt = ('Index: {:03d} Energy: {:04f}'
                       .format(i, e))
                self.log_from_master(txt)

    def log_nonvalid_structures(self, structure_ok, string, index, subindex):
        if not structure_ok:
            txt=('Index: {:03d} Subindex: {:03d} '
                 .format(index, subindex) + string)
            self.logfile.write(txt)



class LowerBound:
    '''
    Class to calculate acquisition function by the lower confidence bound
    
    usage:
        1. initiate class 
            acq=LowerBound(kappa=...)
        2. get acquisition value
            acq.get(energy=.., uncertainty=..)
    
    parameters:    
    
    kappa: float
        parameter describing emphasis on uncertainty in acquisition function
        default 2
    '''
    def __init__(self, kappa=2):
        self.kappa = kappa

    def get(self, energy, uncertainty):
        return energy - self.kappa * uncertainty




class CalculatorInterface:
    '''
    class to help handling of dft calculations inside of beacon
    
    Usage:
        1. instantiate class
            calculator=CalculatorInterface(calc=.., calcparams=..)
        2. calculate properties
            calculator.calculate_properties(atoms)
    
    Parameters:
        
    calc: calculator object
        e.g. EMT  or GPAW
    calcparams: dict
        parameters for calculator of choice
    prepare_atoms_for_dft: function
        a function taking atoms as input and outputs a modified atomsobject
        with properties important for DFT calculation, e.f. initial charges
        or magnetic moments
    '''


    def __init__(self, calc, calcparams=None, prepare_atoms_for_dft=None,
                 calculator_extension=None):


        self.calc=calc
        if calcparams is None:
            calcparams={}

        self.calcparams=calcparams
        self.prepare_atoms_for_dft = prepare_atoms_for_dft
        self.calculator_extension = calculator_extension
        
        
    def new_calc(self):
        ''' Return new Calculator object '''

        calculator=self.calc(**self.calcparams)
        
        if self.calculator_extension is not None: # such that one can use DFTD3
            calculator=self.calculator_extension(dft=calculator)
            
        return calculator


    def calculate_properties(self, atoms, attatch_calc=True):
        '''
        Calculate energy and forces of atoms. Return potential energy.
        '''
        
        if self.prepare_atoms_for_dft is not None:
            atoms = self.prepare_atoms_for_dft(atoms)

        if attatch_calc:
            if len(atoms.constraints)==0:
                niggli_reduce(atoms)
                atoms.center()
                atoms.wrap()
                
            atoms.calc=self.new_calc()


        atoms.get_potential_energy(force_consistent=None, 
                                   apply_constraint=False)
        atoms.get_forces(apply_constraint=False)
            
        return

    def get_output(self, atoms):
        '''
        Get correct energy of the best found structure.
        '''
        real_e = atoms.calc.results['energy'] 
        real_f = atoms.calc.results['forces'].flatten()
        return real_e, real_f



class StructureList:

    def __init__(self, images, n, flim):
        '''
        This class handles the list of best structures in
        BEACON.


        usage: 
            1. instantiate class:
                lob=StructureList(images=..., n=..., flim=...)
            2. update list of best:
                lob.update(atoms=..., candidate_index=...)

        Parameters:

        atoms: list
            list of ase.Atoms objects
            must be supplied
        n: int
            Number of structures to store in the list
            must be supplied
        flim: float
            Force threshold. If the maximum force of an atomic
            structure is below this number, it is excluded
            from the list.
            must be supplied
        '''

        self.n = n  # number of structures
        self.structurelist = []
        self.update_list_by_many(images)
        self.flim = flim


    def update_list_by_many(self, images):
        self.structurelist += images
        self.sort_structurelist()

    def update_list_by_one(self, atoms, overwrite_index=None):
        if overwrite_index is None:
            self.structurelist += [atoms]
        else:
            self.structurelist[overwrite_index] = copy_image(atoms)
        self.sort_structurelist()

    def sort_structurelist(self):
        '''
        Sort images by their potential energies and create a list
        of the sorted structures with maximum length of self.n
        '''

        argsort = np.argsort(self.energies)
        self.structurelist = [copy_image(self.structurelist[i])
                              for i in argsort[:self.n]]

    @property
    def energies(self):
        '''
        Return all potential energies for the structures in the list.
        '''
        return [atoms.get_potential_energy()
                for atoms in self.structurelist]




    def update(self, new_atoms, candidate_index):
        '''
        Update the list of best structures.
        Returns True if structure is converged. False if not.
        '''

        e = new_atoms.get_potential_energy()
        f = new_atoms.get_forces()


        # Check if all the forces are below the threshold:
        real_local_min = (np.max(np.linalg.norm(f.reshape(-1, 3),
                                                axis=1)) < self.flim)

        # Check if the structure was obtained
        # by relaxing a structure on the previous list-of-best:
        one_of_listofbest = candidate_index < len(self)

        # If it is a real local minimum, return True
        if real_local_min:
            if one_of_listofbest:
                # delete image from list of best
                self.structurelist.pop(candidate_index)
            return

        # Don't add but update if it the structure
        # was obtained by relaxing something already on the list:
        overwrite_index = None
        if one_of_listofbest and (e < self.energies[candidate_index]):
            overwrite_index = candidate_index

        # Sort list by energy and make sure it's size
        # is self.nbest (or smaller):
        self.update_list_by_one(new_atoms, overwrite_index=overwrite_index)

        return

    def __len__(self):
        return len(self.structurelist)

    def __getitem__(self, i):
        return self.structurelist[i]


class InitatomsGenerator:
    '''
    Generator to give structures for surrogate relaxations in BEACON.
    The relaxations start from some of the already visited low-energy
    structures, the same structures rattled, or random structures.
    
    
    usage:
        1. Instantiate
            initatomsgen=InitatomsGenerator(sgen=..., rgen=...)
        2. attatch list of best:
            initatomsgen.setup_lob(atoms_list)
            (will be done automatically in BEACON)
        3. get structure from sgen with candidate index i:
            initatomsgen.get(i)
        4. get structure from rgen:
            initatomsgen.get_random()
            
            
    parameters
        sgen: structure generator object
            must be supplied
        
        rgen:structure generatir object
            default: same as sgen
        
        nrattle: int
            how many rattled structues should be generated
            default: 0
        
        nbest: int
            how many structures should be made from list of best
            default: 0
        
        realfmax: float
            realfmax for import to StructureList
            default: 0.1
        
        rattlestrength: float
            how hard the rattled structures are rattled    
            default: 0.5
        
        rng: seed 
            seed for ranum number generation
            default: np.random
            
        rattle_seed_int: int
            seed for atom rattling
            refault: 100000
            
        lob: StructureList object
            default None.
    '''

    def __init__(self, sgen, rgen=None, nrattle=0, nbest=0, realfmax=0.1,
                 rattlestrength=0.5, rng=np.random, rattle_seed_int=100000,
                 lob=None):

        self.nrattle = nrattle
        self.nbest=nbest
        self.realfmax=realfmax
        self.rattlestrength = rattlestrength
        self.rng = rng
        self.sgen = sgen
        self.lob=lob
        self.rattle_seed_int=rattle_seed_int

        if rgen is None:
            self.rgen=sgen
        else:
            self.rgen=rgen


    def setup_lob(self, atoms):

        if self.lob is None:
            self.lob = StructureList(atoms,
                                     n=self.nbest,
                                     flim=self.realfmax)


    def get(self, i=None):
        '''
        Parameters:

        i: int
            Index that indicates what type of structure is
            returned. (one of best, rattled one of best, or
            random). If None, structure from sgen is given.
        '''

        n_best = self.get_nbest()

        if i is None:
            i = np.inf

        atoms = self.get_atoms(i, n_best)

        return atoms

    def get_nbest(self):
        '''
        Get number of best structures in the current state of
        list-of-best.
        '''

        if self.lob is None:
            return 0

        return len(self.lob)

    def get_atoms(self, i, n_best):
        '''
        Get atoms of different types based on the index 'i'
        and the number of best structures 'n_best'.
        '''
        # One of best:
        if i < n_best:
            atoms = self.get_one_of_best_atoms(i)

        # Rattled one of best:
        elif self.get_rattled_criterion(i, n_best):

            atoms = self.get_rattled_one_of_best(i, n_best)

        # One given by structure generator:
        else:
            atoms = self.sgen.get()

        return atoms

    def get_one_of_best_atoms(self, i):

        '''
        Get i'th structure from the list of best.
        '''
        atoms=self.lob[i].copy()
        return atoms

    def get_rattled_one_of_best(self, i, n_best):
        atoms = self.lob[i % n_best].copy()

        atoms.rattle(self.rattlestrength,
                     seed=self.rng.randint(self.rattle_seed_int))
        atoms.wrap()
        return atoms

    def get_rattled_criterion(self, i, n_best):
        return (n_best > 0) and (i < n_best + self.nrattle )



    def get_random(self):
        ''' specific structure generator for making random structures
        if no surrogate candidates were accepted'''
        atoms=self.rgen.get()
        return atoms




class SurrogateOptimizer:
    
    '''
    Class to handle local optimizations in BEACON.
    
    usage:
        1. Instantiate optimizer
            surropt=SurrogateOptimizerUnitCell(fmax=..., relax_steps=...)
        2. relax structure
            surropt.relax(atoms=.., model=..)
            
    
    parameters:
        
        with_unit_cell: if the unit cell should be optimized
            default: False
        
        fixed_cell_params: list of 6 bools
            bool giving what voight components of the cell is not optimized
            xx, yy, zz, yz, xz, xy
            default: [False, False, False, False, False, False]
        
        fmax: float
            convergence criteria. size of largest force component
            befor the surrogate relaxer stops 
            default: 0.05
        
        relax_steps: int
            number of steps taken by relaxer
            default: 100
        
        write_surropt_trajs: bool
            if output trajectory files should be written
            default: False
    '''

    def __init__(self, fmax=0.05, relax_steps=100,
                 with_unit_cell=False, 
                 fixed_cell_params=None, 
                 write_surropt_trajs=False,
                 error_method=None):

        self.fmax = fmax
        self.relax_steps = relax_steps
        self.write_surropt_trajs = write_surropt_trajs
        self.error_method=error_method
        
        self.with_unit_cell=with_unit_cell
        
        if fixed_cell_params is None:
            fixed_cell_params = [False, False, False, False, False, False]
            
        self.mask = [not elem for elem in fixed_cell_params]


    def relax(self, atoms, model, file_identifier=''):
        '''
        Relax atoms in the surrogate potential from model.
        eventually write to file with name including file_identifier
        '''
                
        atoms.calc=GPCalculator(model, calculate_uncertainty=True,
                                calculate_stress=self.with_unit_cell,
                                error_method=self.error_method)

        if self.with_unit_cell:
            opt_object = UnitCellFilter(atoms, mask=self.mask)
        else:
            opt_object=atoms

        opt = BFGS(atoms=opt_object,
                   maxstep=0.2,
                   logfile=os.devnull,
                   master=True)
        
        if self.write_surropt_trajs:
            traj = Trajectory('opt_'+file_identifier+'.traj',
                              'w', atoms, master=True)
            opt.attach(traj)
        
        success = opt.run(fmax=self.fmax, steps=self.relax_steps)

        return atoms, success

    
    
class Checker:
    '''
    Class to check candidates structures from surrogate optimizations
        will output if the structure is accepted or not and a string
        specifying why if/why the structure was rejected
    
    Usage
    
    1. Instantiate class:
           checker=Checker(dist_limit=..., rimit=...)
    2. check structure:
        checker.check(atoms)
        
        
    parameters:
    
    dist_limit: float
        float desvribing the minimal akkowed distance in fingerprint
        space between a new candidate and all structures in the database
        Default: None, i.e. dont check
        
    rlimit: float
        float describing the minimum distance between any two atom centers
        in an atoms object as a multiplier of the atomic covalent radii.
        rlimit=1 means the minimum distance is covalent radius of atom1 +
        the covalent radius of atom 2. 
        Default: None, i.e. dont check
        
    '''

    def __init__(self, dist_limit=None, rlimit=None, 
                 disconnect_limit=None, angle_limit=None, 
                 volume_limits=None):
        
        self.dist_limit = dist_limit
        self.rlimit = rlimit        
        self.volume_limits=volume_limits
        
        self.disconnect_limit=disconnect_limit
        
        if angle_limit is not None:
            angle_limit=90-angle_limit
        
        self.angle_limit=angle_limit


    def check_atomic_distances(self, atoms):
        '''
        Check if atoms are too close to each other or
        too close to unit cell boundary.

        Parameters:
        -----------
        atoms : ase.Atoms
        object to check

        rlimit : float
        Limit for check if atoms are too close to each other
        
        disconnect_limit: float
        limit for check if atoms are too far apart
        
        angle_limit : float 
        Minimum of acceptible angles
        
        volume_limits : list of two floats
        scaling constants for the acceptable cell volumes. 
        first entry, lower limit, second entry, upper limit.

        '''
        atoms = atoms.copy()
        atoms.wrap()
        atoms = atoms.repeat([1 + int(c) for c in atoms.pbc])

        coord = atoms.positions

        # Atoms too close to each other:
        dm = distance_matrix(coord, coord)
        dm += 10.0 * np.eye(len(dm))

        cov_radii = self.rlimit * self.cov_radii_table(atoms)
        if (dm < cov_radii).any():
            return False

        return True
    
    
    def check_disconnections(self, atoms):
        
        atoms = atoms.copy()
        atoms.wrap()
        atoms = atoms.repeat([1 + int(c) for c in atoms.pbc])
        coord = atoms.positions

        # Atoms too close to each other:
        dm = distance_matrix(coord, coord) 
        
        cov_radii =  self.cov_radii_table(atoms)
        
        dm += (np.max(cov_radii) +1) * np.eye(len(dm))
        
        disconnected_atoms = np.all(dm > self.disconnect_limit * cov_radii, axis=0)

        any_disconnected_atoms = np.any(disconnected_atoms)
        if any_disconnected_atoms:
            return False
        
        return True
        
        

    def check_fingerprint_distances(self, distances):
        '''
        Return True if distances are ok,
        False if not.
        '''
        return np.min(distances) > self.dist_limit
    
    
    def check_cell_volume(self, atoms):

        cell_volume=atoms.get_volume() 
        
        cell_too_small = cell_volume<self.volume_limits[0]
        
        cell_too_large = cell_volume>self.volume_limits[1]
        
        return cell_too_small, cell_too_large
        
    
    def check_cell_angles(self, atoms):

        angles = atoms.cell.angles()
        
        angles_too_small = np.any(angles < (90 - self.angle_limit) )
        
        angles_too_large = np.any(angles > (90 + self.angle_limit) )
        
        if angles_too_small or angles_too_large:
            return False
    
        return True
    

    def check(self, atoms, distances):
        '''
        Check the atomic distances and the fingerprint distances of
        'atoms'.
        '''

        if self.dist_limit is not None:
            fp_distances_ok = self.check_fingerprint_distances(distances)
            if not fp_distances_ok:
                output_string='structure too close to existing structure'
                return False, output_string

        if self.rlimit is not None:
            atomic_distances_ok = self.check_atomic_distances(atoms)
            if not atomic_distances_ok:
                output_string='Atomic distances too short'
                return False, output_string

        if self.disconnect_limit is not None:
            all_connected = self.check_disconnections(atoms)
            if not all_connected:
                output_string='one or more disconnected atoms'
                return False, output_string

        if self.angle_limit is not None:
            cell_angles_ok = self.check_cell_angles(atoms)
            if not cell_angles_ok:
                output_string='one or more cell angles too small'
                return False, output_string

        if self.volume_limits is not None:
            cell_too_small, cell_too_large = self.check_cell_volume(atoms)
            if  cell_too_small:
                output_string='cell volume too small'
                return False, output_string
            elif cell_too_large:
                output_string='cell volume too large'
                return False, output_string


        output_string='structure accepted'
        return True, output_string

    def cov_radii_table(self, atoms):
        '''
        Return all-to-all table of the sums of covalent radii for
        atom pairs in 'atoms'.
        '''
        table = [[(covalent_radii[i] + covalent_radii[j])
                  for i in atoms.symbols.numbers]
                 for j in atoms.symbols.numbers]
        table = np.array(table)
        return table


class BEACONAtomsIO:
    '''
    Handle write and read of Atoms objects within BEACON.
    
    Usage:
        1. instantiate class:
            beaconio=BEACONAtomIO()
        2. initialize output files:
            beaconio.initialize_xyzfiles()
        3. write to file identified by string strtype
            beaconio.write_xyz(atoms, strtype)
            
    strtype can be 
    'init',      init_structures.xyz
    'result',    structures_dft.xy
    'extra'      extras.xyz
    'surropt'    surropt.xyz
    '''

    def __init__(self):

        # filenames for writing xyz
        self.initstrfile = 'init_structures.xyz'
        self.structurefile = 'structures_dft.xyz'
        self.extrafile = 'extras.xyz'
        self.surroptfile = 'surropt.xyz'


        self.names_by_type = {'init': self.initstrfile,
                              'result': self.structurefile,
                              'extra': self.extrafile,
                              'surropt': self.surroptfile}

        self.initialize_xyzfiles()

        return

    def initialize_xyzfiles(self):
        '''
        Create empty files.
        '''
        if world.rank == 0:
            for key in self.names_by_type:
                f = open(self.names_by_type[key], 'w')
                f.close()


    def write_xyz(self, atoms, strtype,
                  append=True, **kwargs):
        '''
        Write atoms to the file specified by strtype.
        '''

        filename = self.names_by_type.get(strtype)

        with warnings.catch_warnings():

            # with EMT, a warning is triggered while writing the
            # results in ase/io/extxyz.py. Lets filter that out:
            warnings.filterwarnings('ignore', category=UserWarning)

            write(filename, atoms, append=append, **kwargs)

class IterationResult:
    '''
    Class to store, update, and distribute results from the
    surrogate relaxations.
    
    Usage:
        1. Instantiate_class 
            it_result=IterationResult(natoms=...)
        2. test if new acquisition value is smaller than current smallest value
            is_better(value)
        3. Update the results for this rank
            update(atoms)
        4. Share data between processors, and return the data for the
            structure that has the best acquisition function over all
            ranks.
            distribute()
    
    Parameters:
        natoms: the number of atoms in the structures made        
        
    '''

    def __init__(self, natoms):
        '''
        natoms: an amount of atoms
            an amount of atoms that is only used to read the
            size of the force array for initialization here.
        '''

        self.best_acq = np.inf
        self.my_best = dict(acq=np.inf,
                            energy=0.0,
                            forces=np.empty(natoms * 3),
                            unc=0.0,
                            subindex=0,
                            atoms=None)


    @property
    def my_best_acq(self):
        '''
        Best acquisition function at this rank.
        '''
        return self.my_best['acq']

    def update(self, dct):
        '''
        Update the results for this rank.
        '''
        self.my_best.update(dct)

    def is_better(self, value):
        '''
        Test if value is smaller than my_best_acq.
        '''
        if value < self.my_best_acq:
            return True

        return False

    def distribute(self):
        '''
        Share data between processors, and return the data for the
        structure that has the best acquisition function over all
        ranks.
        '''

        minrank = self.get_min_rank()

        # Share data among processors:
        best_e = np.atleast_1d(self.my_best['energy'])
        best_f = self.my_best['forces']


        best_u = np.atleast_1d(self.my_best['unc'])
        best_acq = np.atleast_1d(self.my_best['acq'])
        my_best_subindex = np.atleast_1d(self.my_best['subindex'])

        best_atoms = self.my_best['atoms']
        world.broadcast(best_e, minrank)
        world.broadcast(best_f, minrank)
        world.broadcast(best_u, minrank)
        world.broadcast(best_acq, minrank)
        world.broadcast(my_best_subindex, minrank)
        
       # broadcast atoms from rank with best structure to all procesors without calculator
        if best_atoms is not None:
            best_atoms.calc=None
        best_atoms=broadcast(best_atoms, minrank)


        # Set results to attributes:
        best_e = best_e[0]
        best_u = best_u[0]
        best_acq = best_acq[0]

        best_subindex = my_best_subindex[0]
        return (best_atoms, best_e, best_f,
                best_u, best_acq, best_subindex)

    def get_min_rank(self):
        '''
        Get rank with the minimal acquisition function.
        '''
        my_best_acq = np.atleast_1d(self.my_best_acq)

        # List all acquisition functions to an array with the
        # indices corresponding to rank indices:
        all_acqs = np.empty([world.size, 1])
        for rank in range(world.size):
            tmp = my_best_acq.copy()
            world.broadcast(tmp, rank)
            all_acqs[rank] = tmp.copy()

        # Get rank with the best acquisition:
        return np.argmin(all_acqs)

