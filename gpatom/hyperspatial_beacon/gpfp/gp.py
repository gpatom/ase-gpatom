from gpatom.gpfp.gp import GaussianProcess
from gpatom.hyperspatial_beacon.gpfp.kernel import HighDimKernel, HighDimStressKernel
from gpatom.gpfp.kernel import FPKernelNoforces

class HighDimGaussianProcess(GaussianProcess):
    '''
    Gaussian process that is specifically used in Space-BEACON.
    '''

    def __init__(self, **kwargs):
        GaussianProcess.__init__(self, **kwargs)

        kernelparams = {'weight': self.hp['weight'],
                        'scale': self.hp['scale']}

        if self.use_stress:
            self.kernel = HighDimStressKernel(kerneltype='sqexp', params=kernelparams)
        elif self.use_forces:
            self.kernel = HighDimKernel(kerneltype='sqexp', params=kernelparams)
        else:
            self.kernel = FPKernelNoforces(kerneltype='sqexp', params=kernelparams)


    def get_properties(self, x, dims=3):
        
        f, V = self.predict(x, get_variance=True)
        
        energy, forces = self.translate_predictions(f, dims)
        
        
        if self.use_forces:
            uncertainty_squared = V[0, 0]
        else:
            uncertainty_squared = V[0]
    
        return energy, forces, uncertainty_squared
    
    def translate_predictions(self, predict_array, dims=3):
        energy=predict_array[0]
        forces=predict_array[1:].reshape(-1, max(dims,3))
         
        return energy, forces
