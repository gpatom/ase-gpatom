#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 29 13:27:25 2023

@author: casper
"""

from gpatom.gpfp.kernel import FPKernel, FPStressKernel

class HighDimKernel(FPKernel):
        
    def get_size(self, x):
        '''
        Return the correct size of a kernel matrix when gradients are
        trained.
        '''
        return len(x.atoms) * x.dims + 1
        
    
class HighDimStressKernel(FPStressKernel):
    
    def get_size(self, x):
        '''
        Return the correct size of a kernel matrix when gradients are
        trained.
        '''
        return len(x.atoms) * x.dims + 1
