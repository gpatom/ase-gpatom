import numpy as np
import itertools
from ase.neighborlist import NeighborList
from gpatom.gpfp.fingerprint import (RadialFP, RadialFPCalculator,
                                     RadialFPGradientCalculator,
                                     AngularFPGradientCalculator,
                                     RadialAngularFP, AngularFPCalculator,
                                     AtomPairs, AtomTriples, FPFactors)

from gpatom.hyperspatial_beacon.hyperspacebeacon import HSParamsHandler


class HighDimFingerPrint:
    
    def __init__(self, fp_args={}, angular=True, 
                 calc_gradients=True, calc_strain=False):

        self.fp_args=fp_args
        self.calc_gradients=calc_gradients
        self.calc_strain=calc_strain
        
        if angular:
            self.fp_class=HighDimRadialAngularFP
        else:
            self.fp_class=HighDimRadialFP
        
    def get(self, atoms, extra_coords=None):
        
            return self.fp_class(atoms, extra_coords,
                                 calc_gradients=self.calc_gradients,
                                 calc_strain=self.calc_strain,
                                 **self.fp_args)
        
        
class HighDimRadialFP(RadialFP):

    def __init__(self, atoms, extra_coords=None, calc_gradients=True, 
                 calc_strain=False, **kwargs):
        ''' Parameters:

        r_cutoff: float
            Threshold for radial fingerprint (Angstroms)

        r_delta: float
            Width of Gaussian broadening in radial fingerprint
            (Angstroms)

        r_nbins: int
            Number of bins in radial fingerprint

        calc_gradients: bool
            Whether gradients are calculated
        '''
        
        self.check_cell(atoms)
        self.atoms = atoms.copy()
        self.atoms.wrap()
        

        default_parameters = {'r_cutoff': 8.0,
                              'r_delta': 0.4,
                              'r_nbins': 200}
        
        self.params = default_parameters.copy()        
        self.params.update(kwargs)
        
        fpparams = dict(cutoff=self.params['r_cutoff'],
                        width=self.params['r_delta'],
                        nbins=self.params['r_nbins'])


        self.dims = HSParamsHandler.get_dimension_from_exra_coords(extra_coords)
        self.extra_coords=extra_coords

        self.pairs = HighDimAtomPairs(self.atoms,
                                      extra_coords,
                                      self.params['r_cutoff'])
        
        self.elements=sorted(atoms.symbols.species())
        self.element_vectors = FPFactors.get_element_vectors(self.atoms, 
                                                             self.elements)
        groups = FPFactors.get_factors_for_pairs(self.pairs,
                                                 self.element_vectors)

        (gaussians,
         fingerprint_ij)=RadialFPCalculator.get_gaussians(self.pairs, 
                                                          **fpparams)
        
        grads_ij=RadialFPGradientCalculator.get_grad_terms(gaussians,
                                                           self.pairs,
                                                           **fpparams)        
                
        self.rho_R = RadialFPCalculator.get_fp(fingerprint_ij, 
                                               groups, self.pairs,
                                               self.params['r_nbins'])
        
        self.vector = self.rho_R.flatten()

        self.gradients = (RadialFPGradientCalculator.
                          get_gradients(grads_ij, groups,  self.natoms, 
                                       self.pairs, self.params['r_nbins'],
                                       dimensions=self.dims)
                          if calc_gradients else None)

        self.strain = (RadialFPGradientCalculator.
                       get_strain(grads_ij, groups, self.pairs, 
                                  self.params['r_nbins'])[:,:,:3,:3]
                       if calc_strain else None)

        
    def reduce_coord_gradients(self):
        '''
        Reshape gradients by flattening the element-to-element
        contributions.
        '''
        return self.gradients.reshape(self.natoms, -1, self.dims) 
    
    
    
    
    
    
class HighDimRadialAngularFP(HighDimRadialFP, RadialAngularFP):
    def __init__(self, atoms, extra_coords=None, calc_gradients=True, 
                 calc_strain=False, **kwargs):
        ''' Parameters:

        a_cutoff: float
                Threshold for angular fingerprint (Angstroms)

        a_delta: float
                Width of Gaussian broadening in angular fingerprint
               (Radians)

        a_nbins: int
            Number of bins in angular fingerprint

        aweight: float
            Scaling factor for the angular fingerprint; the angular
            fingerprint is multiplied by this number

        '''
        
        HighDimRadialFP.__init__(self, atoms, extra_coords=extra_coords, 
                                 calc_gradients=calc_gradients, 
                                 calc_strain=calc_strain, **kwargs)

        default_parameters = {'r_cutoff': 8.0,
                              'r_delta': 0.4,
                              'r_nbins': 200,
                              'a_cutoff': 4.0,
                              'a_delta': 0.4,
                              'a_nbins': 100,
                              'gamma': 0.5,
                              'aweight': 1.0}

        self.params = default_parameters.copy()
        self.params.update(kwargs)

        fpparams=dict(width=self.params['a_delta'],
                      nbins=self.params['a_nbins'],
                      aweight=self.params['aweight'],
                      cutoff=self.params['a_cutoff'],
                      gamma=self.params['gamma'])

        assert self.params['r_cutoff'] >= self.params['a_cutoff']

        self.extra_coords=extra_coords

        self.triples = HighDimAtomTriples(self.atoms,          
                                          extra_coords,
                                          cutoff=self.params['a_cutoff'])


        groups = FPFactors.get_factors_for_triples(self.triples,
                                                   self.element_vectors)
                                               
        (angle_gaussians,
         fingerprint_ijk)=AngularFPCalculator.get_angle_gaussians(self.triples,
                                                                  **fpparams)
                
        (grads_ij, 
         grads_ik, 
         grads_jk) = AngularFPGradientCalculator.get_grad_terms(angle_gaussians,
                                                                self.triples,
                                                                **fpparams)

        self.rho_a = AngularFPCalculator.get_fp(fingerprint_ijk, 
                                                groups, self.triples, 
                                                self.params['a_nbins'])
        
        self.vector = np.concatenate((self.rho_R.flatten(),
                                      self.rho_a.flatten()), axis=None)


        self.anglegradients = (AngularFPGradientCalculator.
                               get_gradients(grads_ij, grads_ik, grads_jk, 
                                             groups, self.natoms, self.triples,
                                             self.params['a_nbins'],
                                             dimensions=self.dims)
                               if calc_gradients else None)

        
        self.anglestrain = (AngularFPGradientCalculator.
                            get_strain(grads_ij, grads_ik, grads_jk, 
                                       groups, self.triples, 
                                       self.params['a_nbins'])[:,:,:3,:3]
                            if calc_strain else None)


        
    def reduce_coord_gradients(self):
        '''
        Reshape gradients by flattening the element-to-element
        contributions and all angles, and concatenate those arrays.
        '''
        
        return np.concatenate((self.gradients.reshape(self.natoms, -1, self.dims),
                               self.anglegradients.reshape(self.natoms, -1, self.dims)),
                              axis=1)
    


class HighDimAtomPairs(AtomPairs):

  
    def __init__(self, atoms, extra_coords, cutoff):
                
        neighbor_list = NeighborList([cutoff/2]*len(atoms), 
                                     skin=0,
                                     self_interaction=False,
                                     bothways=False)
        neighbor_list.update(atoms)
        
        pairs=[]
        vectors=[]
        for atom_index in range(len(atoms)):
            neighbors, d = AtomPairs.get_distance_info(atoms, atom_index, neighbor_list)
            
            if extra_coords is not None:
                d_ext=extra_coords[neighbors]-extra_coords[atom_index]
                d=np.hstack((d,d_ext))
                dists=np.linalg.norm(d, axis=1)
                mask=(dists<cutoff)   
                neighbors=neighbors[mask]
                d=d[mask]
            
            for neighbor_index, vector in zip(neighbors, d):
                pairs.append([atom_index, neighbor_index])
                vectors.append(vector)

        self.get_pair_info(atoms, pairs, vectors)
    
    

class HighDimAtomTriples(AtomTriples):

    def __init__(self, atoms, extra_coords, cutoff):

        neighbor_list = NeighborList([cutoff/2]*len(atoms), 
                                     skin=0,
                                     self_interaction=False,
                                     bothways=True)   
        neighbor_list.update(atoms)
        
        triplets=[]
        vectors_ij=[]
        vectors_ik=[]
    
        for atom_index in range(len(atoms)):
            neighbors, d = AtomPairs.get_distance_info(atoms, atom_index, neighbor_list)
            
            if extra_coords is not None:
                d_ext=extra_coords[neighbors]-extra_coords[atom_index]
                d=np.hstack((d,d_ext))
                dists=np.linalg.norm(d, axis=1)
                mask=(dists<cutoff)   
                neighbors=neighbors[mask]
                d=d[mask]
                        
            neighbor_distance_pairs = list(zip(neighbors, d))
            for (neighbor_1, d1), (neighbor_2, d2) in itertools.combinations(neighbor_distance_pairs, 2):

                triplets.append([atom_index, neighbor_1, neighbor_2])   
                vectors_ij.append(d1)
                vectors_ik.append(d2)
                                
        self.get_triple_info(atoms, triplets, vectors_ij, vectors_ik)